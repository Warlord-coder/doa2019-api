import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pickBy, toLower} from "lodash";
import {PlantRegister} from "../../models/plant-register/PlantRegister";
import {WithDrawPlant} from "../../models/withdraw-plant/WithDrawPlant";
import {WareHouse} from "../../models/warehouse/WareHouse";
import {find} from "lodash";
import {Utility} from "../../utils/constants";
import {LabService} from "../lab/LabService";
import {Lab} from "../../models/lab/Lab";
import {LabAlertForm} from "../../models/LabAlertForm/LabAlertForm";

@Service()
export class WareHouseService extends BaseService<PlantRegister> {

    constructor(@Inject(WareHouse) private wareHouseModal: MongooseModel<WareHouse> | any,
                @Inject(Lab) private labModal: MongooseModel<Lab> | any,
                @Inject(PlantRegister) private plantModal: MongooseModel<PlantRegister> | any,
                @Inject(WithDrawPlant) private withDrawPlantModal: MongooseModel<WithDrawPlant> | any,
                @Inject(LabAlertForm) private alertFormModal: MongooseModel<LabAlertForm> | any) {
        super();
    }

    public async createWH(plantInfo: any): Promise<any> {
        plantInfo.wh_status = "draft";
        plantInfo.register_plant = plantInfo._id;

        delete plantInfo._id;
        delete plantInfo.createdAt;
        delete plantInfo.updatedAt;
        delete plantInfo.createdBy;
        delete plantInfo.updatedBy;

        return this.create(plantInfo, false);
    }

    public async createWHAlert(plantInfo: any): Promise<any> {
        let plant: any = plantInfo;
        plant['warehouse'] = undefined;
        plant['_id'] = undefined;
        delete plant['warehouse'];
        delete plant['_id'];
        plant.wh_status = "draft";
        //plant.register_alert_form = plantInfo._id;
        // const model = this.wareHouseModal;
        // let modelObj = new model(JSON.parse(JSON.stringify(plant)));
        // modelObj = await modelObj.save();
        return this.create(JSON.parse(JSON.stringify(plant)), false);
        //return modelObj.display();
    }

    public async listLabs(): Promise<any> {
        const processPlants = [];
        let labsListing: any = await this.labModal.find({ 'is_alert': null } && { 'isNewLab': false});
        const plantList: any = await this.plantModal.find({m8_lab_id: {$ne: null}})
            .populate("m8_lab_id");
        const whList: any = await this.findAll({register_alert_form: null}, "Lab Listing", "m8_lab_id");
        for await (const wh of whList) {
            processPlants.push(String(wh.register_plant));
            const {m8_lab_id = {}, wh_status} = wh;
            labsListing = labsListing.map((lab) => {
                lab = lab.display ? lab.display() : lab;
                const {lab_no = "", total = 0, complete = 0} = lab;
                if (m8_lab_id && lab_no === m8_lab_id.lab_no) {
                    lab.total = total + 1;
                    lab.complete = wh_status === "complete" ? complete + 1 : complete;
                }

                return lab;
            });
        }
        for await (const wh of plantList) {
            if (processPlants.includes(String(wh._id))) {
                continue;
            }
            const {m8_lab_id = {}} = wh;
            labsListing = labsListing.map((lab) => {
                lab = lab.display ? lab.display() : lab;
                const {lab_no = "", total = 0, complete = 0} = lab;
                if (m8_lab_id && lab_no === m8_lab_id.lab_no) {
                    lab.total = total + 1;
                }
                return lab;
            });
        }

        return labsListing;
    }

    public async listAlertLabs(): Promise<any> {	
        let labsListing: any = await this.labModal.find({is_alert: true});	
        // const plantList: any = await this.alertFormModal.find({labId: {$ne: null}})	
        //     .populate("labId");	
        const whList: any = await this.wareHouseModal.find({register_alert_form: {$ne: null}})	
            // .populate({	
            //     path: "register_alert_form",	
            //     populate: [	
            //         {path: "labId"}	
            //     ]	
            // })	
            //.populate("labId")	
            .populate("volume");	
            for await (let wh of whList) {	
                //const {register_alert_form: {labId = {}} = {}, wh_status} = wh;	
                labsListing = labsListing.map((lab) => {	
                    lab = lab.display ? lab.display() : lab;	
                    const {_id = "", total = 0, complete = 0} = lab;	
                    if (_id.toString() == wh.labId.toString()) {	
                        lab.total = total + 1;	
                        lab.complete = wh.wh_status === "complete" || wh.alertFormStatus === "complete" ? complete + 1 : complete;	
                    }	
                    return lab;	
                });	
            }	
        return labsListing;	
    }

    public async getSpecificLabPlants(id, status = "draft", submitTo = null) {
        const where: any = {deletedAt: null, wh_status: status, register_alert_form: null};
        if (id !== "all") {
            where.m8_lab_id = id;
        }
        if (submitTo) {
            where.submitTo = submitTo;
        }
        const modelObj = await this.wareHouseModal.find(where)
            .populate("m8_lab_id")
            .populate("volume");

        return modelObj.map(obj => obj.display());
    }

    public async getSpecificAllLabPlants(id, submitTo = null) {
        const where: any = {deletedAt: null,register_alert_form: null};
        if (id !== "all") {
            where.m8_lab_id = id;
        }
        if (submitTo) {
            where.submitTo = submitTo;
        }
        const modelObj = await this.wareHouseModal.find(where)
            .populate("m8_lab_id")
            .populate("volume");

        return modelObj.map(obj => obj.display());
    }
    public async getAllWH() {
        let modelObj = await this.wareHouseModal.find({})
                    .populate("reg_plant_sci_name")
                    .populate({	
                        path: 'plant_gene_name',	
                        populate: [{	
                            path: 'plantCategory'	
                        },{	
                            path: 'plantSpecie'	
                        }]	
                    })
                    .populate("reg_gene_type")
                    .populate("reg_gene_category")
                    .populate("updatedBy")
                    .populate("createdBy")
                    .populate("m8_lab_id")
                    .sort({"updatedAt": "desc"});
        return modelObj;
    }

    public async getSpecificAlertLabPlants(id, status = "draft", submitTo = null) {
        const where: any = {deletedAt: null, wh_status: status, register_alert_form: {$ne: null}};
        const labWhere: any = {};

        if (id !== "all") {
            labWhere.labId = id;
        }
        if (submitTo) {
            where.submitTo = submitTo;
        }
        let modelObj = await this.wareHouseModal.find(where)
            .populate({
                path: "register_alert_form", match: labWhere,
                populate: [{
                    path: "warehouse"
                }]
            });
        modelObj = modelObj.filter(obj => obj.register_alert_form);
        modelObj = modelObj.map(obj => {
            const {register_alert_form = {}, ...other} = obj.display();
            const {warehouse = {}} = register_alert_form;
            delete register_alert_form.warehouse;

            return {...warehouse, ...register_alert_form, ...other};
        });

        return modelObj;
    }

    public async getSpecificAllAlertLabPlants(id, submitTo = null) {
        const where: any = {deletedAt: null, register_alert_form: {$ne: null}};
        const labWhere: any = {};

        if (id !== "all") {
            labWhere.labId = id;
        }
        if (submitTo) {
            where.submitTo = submitTo;
        }
        let modelObj = await this.wareHouseModal.find(where)
            .populate({
                path: "register_alert_form", match: labWhere,
                populate: [{
                    path: "warehouse"
                }]
            });

        modelObj = modelObj.filter(obj => obj.register_alert_form);
        modelObj = modelObj.map(obj => {
            const {register_alert_form = {}, ...other} = obj.display();
            const {warehouse = {}} = register_alert_form;
            delete register_alert_form.warehouse;

            return {...warehouse, ...register_alert_form, ...other};
        });

        return modelObj;
    }

    public async updatePlant(updatePlantInfo: any, id, isRemoveEmptyVal = false): Promise<any> {
        const plant = await this.findById(id, "Plant", false);
        if(updatePlantInfo.attribute != undefined){
            updatePlantInfo.attribute = JSON.parse(updatePlantInfo.attribute);
        }
        if(plant['wh_status'] == 'draft'){
            if(plant['submitTo']=='m4'){
                updatePlantInfo.m4recovery_plant_updatedAt = Date.now();
                updatePlantInfo.m4recovery_plant_updatedBy = updatePlantInfo.updatedBy;
            }
            else{
                updatePlantInfo.m9warehouse_plant_updatedAt = Date.now();
                updatePlantInfo.m9warehouse_plant_updatedBy = updatePlantInfo.updatedBy;
            }
        }
        else{
            if(plant['submitTo']=='m2'){
                updatePlantInfo.m2doa_plant_updatedAt = Date.now();
                updatePlantInfo.m2doa_plant_updatedBy = updatePlantInfo.updatedBy;
            }
            // if(plant['submitTo']=='m4'){
            //     updatePlantInfo.m4recovery_plant_updatedAt = Date.now();
            //     updatePlantInfo.m4recovery_plant_updatedBy = updatePlantInfo.updatedBy;
            // }
            
        }
        return this.update(plant, {...updatePlantInfo, updatedAt: Date.now()}, false);
    }
    public async listM4(submitTo): Promise<any> {
        let where: any = {deletedAt: null,submitTo:submitTo,recovery_status:'draft',recovery_id:null};
        let modelObj = await this.wareHouseModal.find({...where, register_alert_form: null})
            .populate("passport")
            .populate("volume")
            .populate("reg_plant_sci_name")
            .populate({	
                path: 'plant_gene_name',	
                populate: [{	
                    path: 'plantCategory'	
                },{	
                    path: 'plantSpecie'	
                }]	
            })
            .populate("reg_gene_type")
            .populate("reg_gene_category")
            .populate("source_country")
            .populate("source_country_1")
            .populate("source_province")
            .populate("source_district")
            .populate("updatedBy")
            .populate("createdBy")
            .sort({"createdAt": "desc"});

        modelObj = modelObj.map(obj => obj.display());


        return modelObj;
    }

    public async list(submitTo = null, others = null): Promise<any> {
        let where: any = {deletedAt: null};
        if (submitTo) {
            where.submitTo = submitTo;
            where.recovery_id = null;
        }
        if (others) {
            where = {...where, ...others};
        }

        let modelObj = await this.wareHouseModal.find({...where, register_alert_form: null})
            .populate("passport")
            .populate("volume")
            .populate("reg_plant_sci_name")
            .populate({	
                path: 'plant_gene_name',	
                populate: [{	
                    path: 'plantCategory'	
                },{	
                    path: 'plantSpecie'	
                }]	
            })
            .populate("reg_gene_type")
            .populate("reg_gene_category")
            .populate("source_country")
            .populate("source_country_1")
            .populate("source_province")
            .populate("source_district")
            .populate("updatedBy")
            .populate("createdBy")
            .populate("m8_lab_id")
            .sort({"updatedAt": "desc"});

        modelObj = modelObj.map(obj => obj.display());


        return modelObj;
    }

    public async getAll(submitTo = null, others = null): Promise<any> { 
        let where: any = {deletedAt: null};
        if (submitTo) {
            where.submitTo = submitTo;
            where.recovery_id = null;
        }
        if (others) {
            where = {...where, ...others};
        }

        let modelObj = await this.wareHouseModal.find({...where, register_alert_form: null})
            .sort({"updatedAt": "desc"});

        modelObj = modelObj.map(obj => obj.display());


        return modelObj;
    }
    public async filterList(submitTo = null, others = null, filter = null): Promise<any> {
        let where: any = {deletedAt: null};
        if (submitTo) {
            where.submitTo = submitTo;
            where.recovery_id = null;
        }
        if (others) {
            where = {...where, ...others};
        }
        
        let {start, length, keyword} = filter;
        let orWhere;
        if(keyword != '') {
            orWhere = [];
            orWhere.push({gs_no: {$regex: '.*' + keyword + '.*', $options: 'i'}});
            orWhere.push({reg_gene_id: {$regex: '.*' + keyword + '.*', $options: 'i'}});
        } else {
            orWhere = [{}];
        }
        let modelObj = await this.wareHouseModal.find({$and: [{$or: orWhere}, {...where, register_alert_form: null}]}).skip(parseInt(start)).limit(parseInt(length))
            .populate("passport")
            .populate("volume")
            .populate("reg_plant_sci_name")
            .populate("plant_gene_name")
            .populate("reg_gene_type")
            .populate("reg_gene_category")
            .populate("source_country")
            .populate("source_country_1")
            .populate("source_province")
            .populate("source_district")
            .populate("updatedBy")
            .populate("createdBy");

        modelObj = modelObj.map(obj => obj.display());
        
        let totalRows = await this.wareHouseModal.count({$and: [{$or: orWhere}, {...where, register_alert_form: null}]});
        
        return {data: modelObj, total_rows: totalRows};
    }

    public async alertFilterList(submitTo = null, others = null, filter = null): Promise<any> {
        // const plantList: any = await this.plantModal.find({m8_lab_id: {$ne: null}})
        //     .populate("m8_lab_id");

        let {start, length, keyword, alert} = filter;

        let result = [];
        let labs: any = [];
        if(alert == false) {
            labs = await this.wareHouseModal.find({m8_lab_id: {$ne: null}}).distinct('m8_lab_id');
            for(let i = 0; i < labs.length; i++) {
                let lab_id = labs[i];
                if(lab_id == undefined) break;
                let warehouse = await this.wareHouseModal.find({m8_lab_id: lab_id}).populate('m8_lab_id');
                let temp = [];
                warehouse.forEach(element => {       
                    if(element.m8_lab_id == null) return;
                    if(element.m8_lab_id != null){
                        let item;
                        if(keyword) {
                           if(toLower( element.m8_lab_id.lab_no).includes(toLower(keyword)) ) {
                                item = {lab_id: element.m8_lab_id._id, lab_no: element.m8_lab_id.lab_no, createdAt: element.m8_lab_id.createdAt, updatedAt: element.m8_lab_id.updatedAt, status: element.m8_lab_status, wh_status: element.wh_status};
                                temp.push(item);
                            }
                        } else {
                            item = {lab_id: element.m8_lab_id._id, lab_no: element.m8_lab_id.lab_no, createdAt: element.m8_lab_id.createdAt, updatedAt: element.m8_lab_id.updatedAt, status: element.m8_lab_status, wh_status: element.wh_status};
                            temp.push(item);
                        }
                        
                        
                    }
                });
                //
                if(temp.length > 0) result.push(temp);
            }
            result.sort((a, b) => {
                if(a[0].lab_no > b[0].lab_no) return -1;
                return 1;
            })
        } else {
            labs = await this.wareHouseModal.find({register_alert_form: {$ne: null}}).distinct('labId');
            for(let i = 0; i < labs.length; i++) {
                let lab_id = labs[i];
                
                let warehouse = await this.wareHouseModal.find({labId: lab_id,alertFormStatus:"complete"}).populate('labId');
                let temp = [];
                warehouse.forEach(element => {
                    if(element.labId.is_alert == true) {
                        if(element.labId == null) return;
                        let item;
                        if(keyword) {
                            if((toLower(element.labId.lab_no).includes(toLower(keyword)) )) {
                                item = {lab_id: element.labId._id, lab_no: element.labId.lab_no, createdAt: element.labId.createdAt, updatedAt: element.labId.updatedAt, status: element.wh_status};
                                temp.push(item);
                            }
                        } else {
                            item = {lab_id: element.labId._id, lab_no: element.labId.lab_no, createdAt: element.labId.createdAt, updatedAt: element.labId.updatedAt, status: element.wh_status};
                            temp.push(item);
                        }
                    }
                });
                if(temp.length > 0) result.push(temp);
            }
            result.sort((a, b) => {
                if(a[0].lab_no > b[0].lab_no) return -1;
                return 1;
            })
        }
        
        //let modelObj = whList.map(obj => obj.display());
        return {data: result.splice(start, length), total_rows: labs.length};
    }

    public async alertFilterNotDraftNumber(submitTo = null, others = null, filter = null): Promise<any> {
        // const plantList: any = await this.plantModal.find({m8_lab_id: {$ne: null}})
        //     .populate("m8_lab_id");

        let {start, length, keyword, alert} = filter;

        let result = [];
        let labs: any = [];
        labs = await this.wareHouseModal.find({m8_lab_id: {$ne: null}}).distinct('m8_lab_id');
        for(let i = 0; i < labs.length; i++) {
            let lab_id = labs[i];
            if(lab_id == undefined) break;
            let warehouse = await this.wareHouseModal.find({m8_lab_id: lab_id}).populate('m8_lab_id');
            let temp = [];
            warehouse.forEach(element => {       
                if(element.m8_lab_id == null) return;
                if(element.m8_lab_id != null){
                    let item;
                    item = {lab_id: element.m8_lab_id._id, lab_no: element.m8_lab_id.lab_no, createdAt: element.m8_lab_id.createdAt, updatedAt: element.m8_lab_id.updatedAt, status: element.m8_lab_status, wh_status: element.wh_status};
                    temp.push(item);
                }
            });
            //
            if(temp.length > 0) result.push(temp);
        }
        
        return {data: result};
    }
    public async alertFilterDraftNumber(submitTo = null, others = null, filter = null): Promise<any> {
        // const plantList: any = await this.plantModal.find({m8_lab_id: {$ne: null}})
        //     .populate("m8_lab_id");

        let result = [];
        let labs: any = [];
        labs = await this.wareHouseModal.find({register_alert_form: {$ne: null}}).distinct('labId');
        for(let i = 0; i < labs.length; i++) {
            let lab_id = labs[i];
            
            let warehouse = await this.wareHouseModal.find({labId: lab_id,alertFormStatus:"complete"}).populate('labId');
            let temp = [];
            warehouse.forEach(element => {
                if(element.labId.is_alert == true) {
                    if(element.labId == null) return;
                    let item;
                    item = {lab_id: element.labId._id, lab_no: element.labId.lab_no, createdAt: element.labId.createdAt, updatedAt: element.labId.updatedAt, status: element.wh_status};
                    temp.push(item);
                }
            });
            if(temp.length > 0) result.push(temp);
        }
        
        return {data: result};
    }
    public async getStoreDate(labId){
        let modelObj = await this.alertFormModal.find({labId:labId})
            .populate("warehouse");
        return modelObj;
    }

    public async m2FilterList(submitTo = null, others = null, filter = null): Promise<any> {
        let where: any = {deletedAt: null};
        if (submitTo) {
            where.submitTo = submitTo;
            where.recovery_id = null;
        }
        if (others) {
            where = {...where, ...others};
        }
        let orWhere = [];
        let {start, length, keyword} = filter;
        if(keyword != '') {
            orWhere = [];
            orWhere.push({reg_gene_id: {$regex: '.*' + keyword + '.*', $options: 'i'}});
            orWhere.push({gs_no: {$regex: '.*' + keyword + '.*', $options: 'i'}});
            //orWhere.push({species: {$regex: '.*' + keyword + '.*', $options: 'i'}});
        } else {
            orWhere = [{}];
        }

        let modelObj = await this.wareHouseModal.find({$and: [{$or: orWhere}, {...where, register_alert_form: null}]} ).skip(parseInt(start)).limit(parseInt(length))
            .populate("passport")
            .populate("volume")
            .populate("reg_plant_sci_name")
            .populate("plant_gene_name")
            .populate("reg_gene_type")
            .populate("reg_gene_category")
            .populate("source_country")
            .populate("source_country_1")
            .populate("source_province")
            .populate("source_district")
            .populate("updatedBy")
            .populate("createdBy");

        modelObj = modelObj.map(obj => obj.display());
        
        let totalRows = await this.wareHouseModal.count({$and: [{$or: orWhere}, {...where, register_alert_form: null}]});
        
        return {data: modelObj, total_rows: totalRows};
    }
    
    
    public async listAlert(submitTo = null, others = null): Promise<any> {
        let where: any = {deletedAt: null,$or:[{submitTo:submitTo,recovery_id:null},{m4_status:true}],$and:[{recovery_status:{$ne:"pending"}},{recovery_status:{$ne:"approve"}},{recovery_status:{$ne:"complete"}}]};		
        if (others) {	
            where = {...where, ...others};	
        }

        let modelObj = await this.wareHouseModal.find({...where, register_alert_form: {$ne: null}})
            .populate("passport")
            .populate("volume")
            .populate("reg_plant_sci_name")
            .populate({	
                path: 'plant_gene_name',	
                populate: [{	
                    path: 'plantCategory'	
                },{	
                    path: 'plantSpecie'	
                }]	
            })
            .populate("reg_gene_type")
            .populate("reg_gene_category")
            .populate("source_country")
            .populate("source_country_1")
            .populate("source_province")
            .populate("source_district")
            .populate("updatedBy")
            .populate("createdBy")
            .populate("m4recovery_plant_creatdBy")
            .populate("m4recovery_plant_updatedBy")
            .populate("m4recovery_plant_submittedBy")
            .populate({
                path: "register_alert_form",
                populate: [{
                    path: "warehouse",
                    populate: [{path: "passport"},
                        {path: "reg_plant_sci_name"},
                        {path: "volume"},
                        {path: "reg_gene_category"},
                        {path: "plant_gene_name"},
                        {path: "reg_gene_type"},
                        {path: "source_country"},
                        {path: "source_country_1"},
                        {path: "source_province"},
                        {path: "source_district"},
                        {path: "createdBy"},
                        {path: "updatedBy"}
                    ]
                }]
            })
            .sort({"updatedAt": "desc"});
        modelObj = modelObj.filter(obj => obj.register_alert_form);
        modelObj = modelObj.map(obj => {
            const {register_alert_form = {}, ...other} = obj.display();
            const {warehouse = {}} = register_alert_form;
            delete register_alert_form.warehouse;
            return {...warehouse, ...register_alert_form, ...other};
        });
        return modelObj;
    }

    public async getSpecificWHPlant(_id: string): Promise<any> {
        const modelObj = await this.wareHouseModal.findOne({_id, deletedAt: null})
            .populate("m8_lab_id");

        if (modelObj) {
            return modelObj.display();
        }

        throw new Error("Not Found");
    }

    public async getSpecificWHPlantWithdraw(_id: string): Promise<any> {
        const modelObj = await this.wareHouseModal.findOne({_id, deletedAt: null})
            .populate("m8_lab_id")
            .populate("volume")
            .populate("passport");

        if (modelObj) {
            return modelObj.display();
        }

        throw new Error("Not Found");
    }

    public async getSpecificAlertWHPlant(_id: string): Promise<any> {
        const modelObj = await this.wareHouseModal.findOne({_id, deletedAt: null})
            .populate({
                path: "register_alert_form",
                populate: [{
                    path: "warehouse",
                    populate: [{
                        path: "m8_lab_id"
                    }]
                }, {
                    path: "labId"
                }]
            });
        if (modelObj) {
            const {register_alert_form = {}, ...other} = modelObj.display();
            const {warehouse = {}} = register_alert_form;
            delete register_alert_form.warehouse;
            return {...warehouse, ...register_alert_form, ...other};
        }
        throw new Error("Not Found");
    }

    public async getCount(where = {}) {
        const countArr = await this.wareHouseModal.distinct("gs_no");

        return countArr.length;
    }

    getModel() {
        return this.wareHouseModal;
    }

    public async getWHPlant(): Promise<any> {
        const modelObj = await this.wareHouseModal.find({ wh_status_alert: { $ne: false }, deletedAt: null, $or: [{ room10: { $exists: true, $not: { $size: 0 } } }, { room5: { $exists: true, $not: { $size: 0 } } }],submitTo : "m2"})
            .populate("m8_lab_id")
            .populate("reg_gene_type")
            .populate("volume")
            .populate("passport")
            .populate("reg_plant_sci_name")
            .populate({
                path: 'plant_gene_name',
                populate: {
                    path: 'plantCategory'
                }
            })
        //.populate("reg_plant_sci_name");	
        if (modelObj) {
            return modelObj;
        }
        throw new Error("Not Found");
    }
    public async getWHM4(): Promise<any> {
        const modelObj = await this.wareHouseModal.find({ deletedAt: null, $or: [{ submitTo: "m4" }, { m4_status: true }] })
            .populate("m8_lab_id")
            .populate("reg_gene_type")
            .populate("volume")
            .populate("updatedBy")
            .populate("passport")
            .populate("reg_plant_sci_name")
            .populate({
                path: 'plant_gene_name',
                populate: {
                    path: 'plantCategory'
                }
            })
        //.populate("reg_plant_sci_name");	
        if (modelObj) {
            return modelObj;
        }
        throw new Error("Not Found");
    }

    public async getRecoveryByStatus(status): Promise<any> {
        const modelObj = await this.wareHouseModal.find({ deletedAt: null,recovery_status:status})
            .populate("reg_gene_type")
            .populate("volume")
            .populate("passport")
            .populate("reg_plant_sci_name")
            .populate({
                path: 'recovery_id',
                populate: {
                    path: 'createdBy'
                }
            })
            .populate({
                path: 'plant_gene_name',
                populate: [{
                    path: 'plantCategory'
                },{
                    path: 'plantSpecie'
                }]
            })
            .sort({"createdAt": "desc"});
        //.populate("reg_plant_sci_name");	
        if (modelObj) {
            return modelObj;
        }
        throw new Error("Not Found");
    }
    public async getListWHM4(): Promise<any> {
        const modelObj = await this.wareHouseModal.find({ deletedAt: null, $or: [{ recovery_status: "pending" },{ recovery_status: "approve" },{ recovery_status: "complete" }, { m4_status: true }] })
            .populate("reg_gene_type")
            .populate("volume")
            .populate("passport")
            .populate("reg_plant_sci_name")
            .populate({
                path: 'recovery_id',
                populate: {
                    path: 'createdBy'
                }
            })
            .populate({
                path: 'plant_gene_name',
                populate: {
                    path: 'plantCategory'
                }
            }).sort({"recovery_id": "desc",recovery_status:"desc"});
        //.populate("reg_plant_sci_name");	
        if (modelObj) {
            return modelObj;
        }
        throw new Error("Not Found");
    }
    public async getArrayList(id: any): Promise<any> {	
        const modelObj = await this.wareHouseModal.find({_id:{$in:id}, deletedAt: null})	
        if (modelObj) {	
            const {register_alert_form = {}, ...other} = modelObj;	
            const {warehouse = {}} = register_alert_form;	
            delete register_alert_form.warehouse;	
            return modelObj;	
        }	
        throw new Error("Not Found");	
    }
    public async updateStatusAlert(id: any,updateInfo: any): Promise<any> {	
        return this.wareHouseModal.updateOne({_id:id}, {$set:{wh_status_alert:false, updatedAt: Date.now()}});	
    }
    public async updateStockAlert(id: any,amonth: any,key:any): Promise<any> {	
        const data = await this.findById(id, "WareHouse", false);	
        let datas = data[key];	
        datas[0].wh_batch_weight =  datas[0].wh_batch_weight- amonth.admin_amount;	
        let setData;	
        if(key == 'room5'){	
            setData = {'room5':datas, updatedAt: Date.now()}	
        }	
        if(key == 'room10'){	
            setData = {'room10':datas, updatedAt: Date.now()}	
        }	
        return this.wareHouseModal.updateOne({_id:id}, {$set:{key:datas, updatedAt: Date.now()}});	
    }
    public async updateStockAlertM4(id: any,amonth: any,key:any): Promise<any> {	
        const data = await this.findById(id, "WareHouse", false);	
        let datas = data[key];	
        datas[0].wh_batch_weight =  datas[0].wh_batch_weight- amonth.admin_amount;	
        let setData;	
        if(key == 'room5'){	
            setData = {'room5':datas, updatedAt: Date.now(),recovery_status:'approve',recovery_seed_amount:amonth.admin_amount}	
        }	
        if(key == 'room10'){	
            setData = {'room10':datas, updatedAt: Date.now(),recovery_status:'approve',recovery_seed_amount:amonth.admin_amount}	
        }	
        return this.wareHouseModal.updateOne({_id:id}, {$set:setData});	
    }
    public async getAlertWHPlant(): Promise<any> {

        const modelObj = await this.wareHouseModal.find({ wh_status_alert: { $ne: false }, deletedAt: null, $or: [{ room10: { $exists: true, $not: { $size: 0 } } }, { room5: { $exists: true, $not: { $size: 0 } } }],submitTo : "m2"})
            .populate({
                path: "alert-list",
                populate: [{
                    path: "warehouse",
                    populate: [{
                        path: "m8_lab_id"
                    }]
                }, {
                    path: "labId"
                }]
            })
            .populate({
                path: 'plant_gene_name',
                populate: {
                    path: 'plantCategory'
                }
            })
            .populate("volume")
            .populate("passport")
            .populate("reg_gene_type")
            .populate("plant_gene_name")
            .sort({ "updatedAt": "desc", "recovery_status": "desc" });
        if (modelObj) {
            const { register_alert_form = {}, ...other } = modelObj;
            const { warehouse = {} } = register_alert_form;
            delete register_alert_form.warehouse;
            return { ...warehouse, ...register_alert_form, ...other };
        }
        throw new Error("Not Found");
    }
    public async getByDepartment(submitTo = null, filter = {}) {
        let startDt = filter['startDt'];
        let endDt = filter['endDt'];
        const plants = await this.wareHouseModal.find({
            parentPlant: null,
            createdAt: { $gte: startDt, $lte: endDt }
        }).populate("volume")
            .populate({
                path: "m9warehouse_plant_refer_id",
                // path: "warehouse_id",
                populate: [{
                    path: "reg_plant_sci_name",
                    populate: [{ path: "plant_category" }]
                }]
            })
            .populate({
                path: "reg_plant_sci_name",
                populate: [{ path: "plant_category" }]
            });
        return;
    }

    public async gsNoSearch(keyword) {
        const warehouses = await this.wareHouseModal.find({gs_no: {$regex: '.*' + keyword + '.*', $options: 'i'}}).limit(20);

        return warehouses;
    }

    public async regNoSearch(keyword) {
        const warehouses = await this.wareHouseModal.find({reg_gene_id: {$regex: '.*' + keyword + '.*', $options: 'i'}}).limit(20);

        return warehouses;
    }
    
    public async exportDoa(query) {
        const {
            keyword, plantCategory, alterPlant, plantType, gs_no, reg_gene_id, plantSpecies,
            plantCommonName, plantGroup, plantGroupType, country, province, plantCharacter, lab_test_growth_percent,
            wh_sample_ref_molecular = null, isSpecieSelected = false
        } = query;

        const whereClause = {
            submitTo: "m2",
        };

        const warehouseList = await this.wareHouseModal.find(whereClause)
            .populate({
                path: "plant_gene_name",
                populate: [{
                    path: 'plantCategory'
                }]
            })
            .populate({
                path: "reg_plant_sci_name",
                populate: [{
                    path: "plant_category"
                }]
            })
            .populate("source_province")
            .populate("source_country")
            .populate("source_district")
            .sort({"updatedAt": "desc"});

        return warehouseList.filter(data => {
            if (plantSpecies) {
                const queryObj = data["reg_plant_sci_name"];
                if (!queryObj || !toLower(queryObj.species).includes(toLower(plantSpecies))) {
                    return false;
                }
            }

            const {room5 = [], room10 = [], reg_character_brief = "", reg_plant_alter_name = []} = data;
            if (lab_test_growth_percent) {
                const growthPercent = [];
                for (const roomInfo of room5) {
                    growthPercent.push(toLower(roomInfo.lab_test_growth_percent));
                }
                for (const roomInfo of room10) {
                    growthPercent.push(toLower(roomInfo.lab_test_growth_percent));
                }

                if (!growthPercent.includes(toLower(lab_test_growth_percent))) {
                    return false;
                }
            }

            if (!isSpecieSelected && plantCategory) {
                const {fields = [], key, innerKey} = this.getSearchField("Plant Category");
                
                let queryObj = data[key];
                if(data[key] == undefined) return false;
                if (innerKey && queryObj) {
                    queryObj = queryObj[innerKey];
                }
                if(!queryObj) return false;
                if (!toLower(queryObj[fields[0]]).includes(toLower(plantCategory)) && !toLower(queryObj[fields[1]]).includes(toLower(plantCategory))) {
                    return false;
                }
            }

            if (!isSpecieSelected && plantType) {
                const {fields = [], key, innerKey} = this.getSearchField("Plant type");
                let queryObj = data[key];
                if (innerKey && queryObj) {
                    queryObj = queryObj[innerKey];
                }
                if (!toLower(queryObj[fields[0]]).includes(toLower(plantType)) && !toLower(queryObj[fields[1]]).includes(toLower(plantType))) {
                    return false;
                }
            }

            if(!isSpecieSelected && gs_no) {
                if (!toLower(data['gs_no']).includes(toLower(gs_no))) {
                    return false;
                }
            }
            if(!isSpecieSelected && reg_gene_id) {
                if (!toLower(data['reg_gene_id']).includes(toLower(reg_gene_id))) {
                    return false;
                }
            }
            if (!isSpecieSelected && plantCommonName) {
                const {fields = [], key} = this.getSearchField("Plant common name");
                const queryObj = data[key];
                if(data[key] == undefined) return false;
                if (!toLower(queryObj[fields[0]]).includes(toLower(plantCommonName)) && !toLower(queryObj[fields[1]]).includes(toLower(plantCommonName))) {
                    return false;
                }
            }

            if (plantGroup) {
                const {fields = [], key} = this.getSearchField("Plant type");
                const queryObj = data[key];
                if(data[key] == undefined) return false;
                if (!toLower(queryObj[fields[0]]).includes(toLower(plantGroup)) && !toLower(queryObj[fields[1]]).includes(toLower(plantGroup))) {
                    return false;
                }
            }

            if (plantGroupType) {
                const {fields = [], key, innerKey} = this.getSearchField("Plant Category");
                let queryObj = data[key];
                if(data[key] == undefined) return false;
                if (innerKey && queryObj) {
                    queryObj = queryObj[innerKey];
                }
                if(!queryObj) return false;
                if (!toLower(queryObj[fields[0]]).includes(toLower(plantGroupType)) && !toLower(queryObj[fields[1]]).includes(toLower(plantGroupType))) {
                    return false;
                }
            }

            if (country) {
                const {fields = [], key} = this.getSearchField("Source Country");
                const queryObj = data[key];
                if(data[key] == undefined) return false;
                if (!toLower(queryObj[fields[0]]).includes(toLower(country)) && !toLower(queryObj[fields[1]]).includes(toLower(country))) {
                    return false;
                }
            }

            if (province) {
                const {fields = [], key} = this.getSearchField("Source Province");
                const queryObj = data[key];
                if(data[key] == undefined) return false;
                if (!toLower(queryObj[fields[0]]).includes(toLower(province)) && !toLower(queryObj[fields[1]]).includes(toLower(province))) {
                    return false;
                }
            }

            // if (plantCharacter) {
            //     if (!toLower(reg_character_brief).includes(toLower(plantCharacter))) {
            //         return false;
            //     }
            // }

            // if (!isSpecieSelected && alterPlant) {
            //     const namesInfo = reg_plant_alter_name.map(({display}) => toLower(display));
            //     if (!namesInfo.includes(toLower(alterPlant))) {
            //         return false;
            //     }
            // }

            if (wh_sample_ref_molecular !== "" && wh_sample_ref_molecular !== null && String(wh_sample_ref_molecular) !== String(data.wh_sample_ref_molecular || false)) {
                return false;
            }


            return true;
        });
    }

    getSearchField(field) {
        switch (field) {
            case "Plant Category":
                return {
                    fields: ["categoryName_en", "categoryName_th"],
                    key: "reg_plant_sci_name",
                    innerKey: "plant_category"
                };
            case "Plant type":
                return {fields: ["plantTypeName_en", "plantTypeName_th"], key: "plant_gene_name"};
            case "Plant Species":
                return {fields: ["species", "genus"], key: "reg_plant_sci_name"};
            case "Plant common name":
                return {fields: ["reg_plant_common_name"], key: "reg_plant_sci_name"};
            case "Source Country":
                return {fields: ["countryName_en", "countryName_th"], key: "source_country"};
            case "Source Province":
                return {fields: ["provinceName_en", "provinceName_th"], key: "source_province"};
        }

        return null;
    }
}
