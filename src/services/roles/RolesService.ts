import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pickBy, toLower} from "lodash";
import {Role} from "../../models/roles/Role";
import {UserService} from "../users/UsersService";

@Service()
export class RoleService extends BaseService<Role> {

    constructor(@Inject(Role) private roleModal: MongooseModel<Role> | any, private userService: UserService) {
        super();
    }

    public async createRole(roleInfo: any): Promise<any> {
        return this.create(roleInfo, false);
    }

    public async findTotal(rolesInfo: any): Promise<any> {
        return Promise.all(rolesInfo.map(async role => {
            const {_id} = role;
            const total = await this.userService.totalUsersInRole(_id);

            return {...role, total};
        }));
    }

    public async updateRole(updateRoleInfo: any, id): Promise<any> {
        const role = await this.findById(id, "Role", false);

        return this.update(role, updateRoleInfo, false);
    }

    public async list(): Promise<any> {
        const roles = await this.findAll({}, "Roles");

        return roles;
    }

    public async getSpecificRole(id: string): Promise<any> {
        const role = await this.findById(id, "Role", false);

        return role;
    }

    getModel() {
        return this.roleModal;
    }
}
