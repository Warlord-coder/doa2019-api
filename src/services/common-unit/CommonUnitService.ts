import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pickBy, toLower} from "lodash";
import {CommonUnit} from "../../models/common-unit/CommonUnit";

@Service()
export class CommonUnitService extends BaseService<CommonUnit> {

    constructor(@Inject(CommonUnit) private commonUnitModal: MongooseModel<CommonUnit> | any) {
        super();
    }

    public async createProvince(commonUnitInfo: any): Promise<any> {
        return this.create(commonUnitInfo, false);
    }

    public async updateProvince(updateProvinceInfo: any, id): Promise<any> {
        const commonUnit = await this.findById(id, "Common Unit", false);

        return this.update(commonUnit, updateProvinceInfo, false);
    }

    public async list(where = {}): Promise<any> {
        const commonUnits = await this.findAll(where, "Common Unit");

        return commonUnits;
    }

    public async getSpecificProvince(id: string): Promise<any> {
        const commonUnit = await this.findById(id, "Common Unit", false);

        return commonUnit;
    }

    getModel() {
        return this.commonUnitModal;
    }
}
