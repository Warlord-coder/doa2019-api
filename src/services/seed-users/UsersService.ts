import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pickBy, toLower} from "lodash";
import * as UUIDV4 from "uuid/v4";
import * as moment from "moment";
import AppConstants from "../../utils/constants";
import {SeedUsers} from "../../models/seed-users/SeedUsers";

@Service()
export class SeedUsersService extends BaseService<SeedUsers> {

    constructor(@Inject(SeedUsers) private userModel: MongooseModel<SeedUsers> | any) {
        super();
    }

    public async createUser(user: any): Promise<SeedUsers | any> {
        user["password"] = await SeedUsers.encrypt(user.password);

        return this.create(user, false);
    }

    public updateUser(user: SeedUsers, updatedVal = {}, removeEmptyValues = true): Promise<SeedUsers | any> {
        return this.update(user, updatedVal, removeEmptyValues);
    }

    public async resetPassword(user: SeedUsers, updatedVal = {}, removeEmptyValues = true): Promise<SeedUsers | any> {
        user['password'] = await SeedUsers.encrypt(user.password);

        let email = user['email'];
        let temp_user = await this.findOne({email}, "SeedUsers", true, false);
        return this.update(temp_user, user, true);
    }
    
    public findUser(id: string): Promise<SeedUsers | any> {
        return this.findById(id, "SeedUsers", false);
    }

    public totalUsersInRole(id: string): Promise<SeedUsers | any> {
        return this.userModel.count({role: id, deletedAt: null});
    }

    public async findAndUpdate(id: string, updatedValues: object | any): Promise<any> {
        const user = await this.findById(id, "SeedUsers", false);
        if (updatedValues.password) {
            updatedValues["password"] = await SeedUsers.encrypt(updatedValues.password);
        }

        return this.update(user, updatedValues, true);
    }

    public findAllUsers(): Promise<SeedUsers | any> {
        return this.findAll({}, "SeedUsers", "role");
    }

    public async setForgotPasswordLink(email: string): Promise<any> {
        const resetPasswordToken = UUIDV4();
        const resetPasswordExpiry = moment().add(1, "days").unix();
        let user = await this.findOne({email}, "SeedUsers", true, false);
        user = await this.update(user, {updatedAt: Date.now(), resetPasswordToken, resetPasswordExpiry});

        return user;
    }

    public async verifyForgotPasswordToken(token: string): Promise<any> {
        const user: SeedUsers | any = await this.findOne({resetPasswordToken: token}, "SeedUsers", false, false);

        if (!user) {
            throw new Error(AppConstants.ERROR_MESSAGES.USER_NOT_EXIST_RESET_LINK);
        }

        const {resetPasswordExpiry} = user;
        if (resetPasswordExpiry < moment().unix()) {
            throw ({message: AppConstants.ERROR_MESSAGES.RESET_TOKEN_EXPIRED, data: {expired: true}});
        }

        return user;
    }

    getModel() {
        return this.userModel;
    }
}
