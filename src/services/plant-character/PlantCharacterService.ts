import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pickBy, toLower} from "lodash";
import {PlantCharacter} from "../../models/plant-character/PlantCharacter";
import {UserService} from "../users/UsersService";

@Service()
export class PlantCharacterService extends BaseService<PlantCharacter> {

    constructor(@Inject(PlantCharacter) private characterModal: MongooseModel<PlantCharacter> | any, private userService: UserService) {
        super();
    }

    public async createCharacter(characterInfo: any): Promise<any> {
        return this.create(characterInfo, false);
    }

    public async updateCharacter(updateCharacterInfo: any, id): Promise<any> {
        const character = await this.findById(id, "Character", false);

        return this.update(character, updateCharacterInfo, true);
    }

    public async updateTags(tags, id): Promise<any> {
        const character: any = await this.findById(id, "Character", false, false);
        if (!character || !tags) {
            return null;
        }
        const {reg_plant_alter_name = []} = character;

        tags = tags.filter(({value}) => {
            return !reg_plant_alter_name.includes(value);
        });

        character.reg_plant_alter_name = [...reg_plant_alter_name, ...tags.map(({value}) => value)];
        character.save();
    }

    public async list(where, populate = ""): Promise<any> {
        const characters = await this.findAll(where, "Characters", populate);

        return characters;
    }

    public async getSpecificCharacter(id: string): Promise<any> {
        const character = await this.findById(id, "Character", false);

        return character;
    }

    public async getFilterList(filter): Promise<any> {
        let orWhere: any = [];
        let {start, length, keyword} = filter;
        if(keyword != '') {
            orWhere = [];
            orWhere.push({genus: {$regex: '.*' + keyword + '.*', $options: 'i'}});
            orWhere.push({species: {$regex: '.*' + keyword + '.*', $options: 'i'}});
        } else {
            orWhere = [{}];
        }
        let modelObj = await this.characterModal.find({$or: orWhere}).skip(parseInt(start)).limit(parseInt(length))
            
        modelObj = modelObj.map(obj => obj.display());

        let totalRows = await this.characterModal.find({$or: orWhere}).count();
        
        return {data: modelObj, total_rows: totalRows};
    }

    getModel() {
        return this.characterModal;
    }
}
