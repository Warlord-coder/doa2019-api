import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import * as moment from "moment";
import {BaseService} from "../BaseService";
import {Accessement} from "../../models/Accessement/Accessement";

@Service()
export class AccessmentService extends BaseService<Accessement> {

    constructor(@Inject(Accessement) private accessementModal: MongooseModel<Accessement> | any) {
        super();
    }

    public async createAccessement(accessementInfo: any): Promise<any> {
        return this.create(accessementInfo, false);
    }

    public async updateAccessement(updateAccessementInfo: any, id): Promise<any> {
        const accessement = await this.findById(id, "Accessement", false);

        return this.update(accessement, updateAccessementInfo, false);
    }

    public async list(where = {}): Promise<any> {
        const accessments = await this.findAll(where, "accessments");

        return accessments;
    }

    public async getSpecificAccessement(id: string): Promise<any> {
        const accessments = await this.findById(id, "Accessement", false);

        return accessments;
    }

    public getFilterItems(items, rating, toDate, fromDate) {
        return items.filter((item) => {
            if (rating && item.rating !== rating) {
                return false;
            }

            if (toDate && !moment(toDate).isSameOrBefore(item.createdAt)) {
                return false;
            }

            if (fromDate && !moment(fromDate).isSameOrBefore(item.createdAt)) {
                return false;
            }

            return true;
        });
    }

    getModel() {
        return this.accessementModal;
    }
}
