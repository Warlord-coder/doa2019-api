import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {EventNews} from "../../models/event-news/EventNews";
import {Banner} from "../../models/frontend-banner/Banner";

@Service()
export class BannerService extends BaseService<Banner> {

    constructor(@Inject(Banner) private bannerModel: MongooseModel<Banner> | any) {
        super();
    }

    public async createEventNews(eventNewsInfo: any): Promise<any> {
        return this.create(eventNewsInfo, false);
    }

    public async updateEventNews(updateEventNewsInfo: any, id): Promise<any> {
        const eventNews = await this.findById(id, "EventNews", false);

        return this.update(eventNews, updateEventNewsInfo, false);
    }

    public async list(where = {}, paginated = {}): Promise<any> {
        const eventNews = await this.findAll(where, "EventNews", "", {...paginated, order: "createdAt"});

        return eventNews;
    }

    public async getSpecificEventNews(id: string): Promise<any> {
        const eventNews = await this.findById(id, "EventNews", false);

        return eventNews;
    }

    getModel() {
        return this.bannerModel;
    }
}
