import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {CommonProvince} from "../../models/common-province/CommonProvince";
import {EventNews} from "../../models/event-news/EventNews";
import {Setting} from "../../models/frontend-setting/Setting";

@Service()
export class SettingsService extends BaseService<CommonProvince> {

    constructor(@Inject(Setting) private settingInfo: MongooseModel<Setting> | any) {
        super();
    }

    public async createEventNews(eventNewsInfo: any): Promise<any> {
        return this.create(eventNewsInfo, false);
    }

    public async updateEventNews(updateEventNewsInfo: any, id): Promise<any> {
        const eventNews = await this.findById(id, "EventNews", false);

        return this.update(eventNews, updateEventNewsInfo, false);
    }

    public async list(where = {}): Promise<any> {
        const eventNews = await this.findAll(where, "EventNews");

        return eventNews;
    }

    public async getSpecificEventNews(where): Promise<any> {
        const eventNews = await this.findOne(where, "EventNews", false);

        return eventNews;
    }

    getModel() {
        return this.settingInfo;
    }
}
