import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pickBy, toLower} from "lodash";
import {CommonAumper} from "../../models/common-aumper/CommonAumper";

@Service()
export class CommonAumperService extends BaseService<CommonAumper> {

    constructor(@Inject(CommonAumper) private aumperModal: MongooseModel<CommonAumper> | any) {
        super();
    }

    public async createAumper(aumperInfo: any): Promise<any> {
        return this.create(aumperInfo, false);
    }

    public async updateAumper(updateAumperInfo: any, id): Promise<any> {
        const aumper = await this.findById(id, "Aumper", false);

        return this.update(aumper, updateAumperInfo, false);
    }

    public async list(where): Promise<any> {
        const aumpers = await this.findAll(where, "Aumper");

        return aumpers;
    }

    public async getSpecificAumper(id: string): Promise<any> {
        const aumper = await this.findById(id, "Aumper", false);

        return aumper;
    }

    getModel() {
        return this.aumperModal;
    }
}
