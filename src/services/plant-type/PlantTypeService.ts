import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pickBy, toLower} from "lodash";
import {PlantType} from "../../models/plant-type/PlantType";

@Service()
export class PlantTypeService extends BaseService<PlantType> {

    constructor(@Inject(PlantType) private plantTypeModal: MongooseModel<PlantType> | any) {
        super();
    }

    public async createPlantType(plantTypeInfo: any): Promise<any> {
        return this.create(plantTypeInfo, false);
    }

    public async updatePlantType(updatePlantTypeInfo: any, id): Promise<any> {
        const plantType = await this.findById(id, "PlantType", false);

        return this.update(plantType, updatePlantTypeInfo, false);
    }

    public async list(where = {}): Promise<any> {
        const plantTypes = await this.findAll(where, "Countries");

        return plantTypes;
    }

    public async getSpecificPlantType(id: string): Promise<any> {
        const plantTypes = await this.findById(id, "PlantType", false);

        return plantTypes;
    }

    public async getFilterList(filter): Promise<any> {
        let where: any = {plantCategory: {$ne: null}};
        let {start, length, keyword} = filter;
        let orWhere = [];
        if(keyword != '') {
            orWhere = [];
            orWhere.push({plantTypeName_en: {$regex: '.*' + keyword + '.*', $options: 'i'}});
            //orWhere.push({species: {$regex: '.*' + keyword + '.*', $options: 'i'}});
        } else {
            orWhere = [{}];
        }
        let modelObj = await this.plantTypeModal.find({$and: [{$or: orWhere}, where]}).populate("plantCategory").skip(parseInt(start)).limit(parseInt(length))

        modelObj = modelObj.map(obj => obj.display());

        let totalRows = await this.plantTypeModal.find({$and: [{$or: orWhere}, where]}).populate("plantCategory").count();
        
        return {data: modelObj, total_rows: totalRows};
    }

    public async search(keyword, category_id) {
        const types = await this.plantTypeModal.find({$and: [{$or: [{plantTypeName_en: {$regex: '.*' + keyword + '.*', $options: 'i'}}, {plantTypeName_th: {$regex: '.*' + keyword + '.*', $options: 'i'}}]}, {plantCategory: category_id}]}).limit(20);

        return types;
    }
    getModel() {
        return this.plantTypeModal;
    }
}
