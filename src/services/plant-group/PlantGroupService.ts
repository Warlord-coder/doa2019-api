import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pickBy, toLower} from "lodash";
import {PlantGroup} from "../../models/plant-group/PlantGroup";
import {UserService} from "../users/UsersService";

@Service()
export class PlantGroupService extends BaseService<PlantGroup> {

    constructor(@Inject(PlantGroup) private categoryModal: MongooseModel<PlantGroup> | any, private userService: UserService) {
        super();
    }

    public async createGroup(categoryInfo: any): Promise<any> {
        return this.create(categoryInfo, false);
    }

    public async updateGroup(updateGroupInfo: any, id): Promise<any> {
        const category = await this.findById(id, "Group", false);

        return this.update(category, updateGroupInfo, true);
    }

    public async list(where = {}): Promise<any> {
        const categorys = await this.findAll(where, "Groups");

        return categorys;
    }

    public async getSpecificGroup(id: string): Promise<any> {
        const category = await this.findById(id, "Group", false);

        return category;
    }

    getModel() {
        return this.categoryModal;
    }
}
