import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pickBy, toLower} from "lodash";
import {Lab} from "../../models/lab/Lab";
import {VolumeService} from "../volume/VolumeService";
import {Utility} from "../../utils/constants";
import {PlantRegisterService} from "../plant-register/PlantRegisterService";
import {WareHouseService} from "../ware-house/WareHouseService";
import {LabAlertForm} from "../../models/LabAlertForm/LabAlertForm";
import {WareHouse} from "../../models/warehouse/WareHouse";

@Service()
export class LabAlertFormService extends BaseService<Lab> {

    constructor(@Inject(LabAlertForm) private labAlertForm: MongooseModel<LabAlertForm> | any,
                @Inject(WareHouse) private wareHouseModal: MongooseModel<WareHouse> | any,                
                private wareHouseService: WareHouseService,
                private plantRegisterService: PlantRegisterService,
                private volumeService: VolumeService) {
        super();
    }

    public async createLabAlertForm(labInfo: any): Promise<any> {
        delete labInfo._id;

        return this.create({
            ...labInfo,
            createdAt: Date.now(),
            updatedAt: Date.now()
        }, false);
    }

    public async updateAlertForm(plantInfo, id): Promise<any> {
        const plant = await this.findById(id, "Plant", false);

        return this.update(plant, plantInfo, true);
    }

    public async listAll(): Promise<any> {
        const labAlerts: any = await this.findAll({}, "LabAlertForm");
        return labAlerts;
    }
    public async list(labId: any, status = "draft"): Promise<any> {
        const alertForm: any = await this.labAlertForm.find({
            labId,
            alertFormStatus: status
        }).populate({
            path: "warehouse",
            populate: [
                {path: "passport"},
                {path: "volume"},
                {path: "reg_plant_sci_name"},
                {path: "plant_gene_name"},
                {path: "reg_gene_category"},
                {path: "reg_gene_type"},
                {path: "source_country"},
                {path: "source_country_1"},
                {path: "source_province"},
                {path: "source_district"},
                {path: "createdBy"},
                {path: "updatedBy"}
            ]
        })
            .sort({"updatedAt": "desc"});

        return alertForm.map(alert => {
            let {warehouse, ...other} = alert.display();

            const KEYS = ["lab_test_plant_amount_input", "lab_test_plant_amount_dropdown",
                "lab_test_plant_amount", "lab_test_moisture_date", "lab_test_moisture_method",
                "lab_test_moisture_amount", "lab_test_moisture_person", "lab_test_growth_percent",
                "lab_test_growth_date", "lab_test_growth_method", "lab_test_growth_amount", "lab_test_growth_person",
                "lab_test_strong_percent", "lab_test_strong_date", "lab_test_strong_method", "lab_test_strong_amount",
                "lab_test_strong_person", "lab_test_alive_percent", "lab_test_alive_date", "lab_test_alive_method",
                "lab_test_alive_amount", "lab_test_alive_person", "lab_seed_rest", "lab_seed_rest_fix_method",
                "lab_seed_rest_fix_date", "lab_seed_amount", "lab_10seed_weight", "lab_seed_est_amount", "lab_plaint_character",
                "lab_admin_name", "lab_remark"];

            warehouse = pickBy(warehouse, (data, key) => {
                return !KEYS.includes(key);
            });

            return {...warehouse, ...other};
        });
    }
    public async specificGrowth(id: any): Promise<any> {
        const alertForm: any = await this.labAlertForm.findById(id)
            .populate({
                path: "labId"
            })
            .populate({
                path: "warehouse"
            })
            .sort({"updatedAt": "desc"});
        return alertForm;
    }
    public async specific(id: any): Promise<any> {
        const alertForm: any = await this.labAlertForm.findById(id)
            .populate({
                path: "labId"
            })
            .populate({
                path: "warehouse",
                populate: [{path: "register_plant"}]
            })
            .sort({"updatedAt": "desc"});

        if (!alertForm) {
            throw new Error("Plant not found");
        }

        let {warehouse, ...other} = alertForm.display();
        let {register_plant = {}} = warehouse || {};
        delete warehouse.register_plant;

        const KEYS = ["lab_test_plant_amount_input", "lab_test_plant_amount_dropdown",
            "lab_test_plant_amount", "lab_test_moisture_date", "lab_test_moisture_method",
            "lab_test_moisture_amount", "lab_test_moisture_person", "lab_test_growth_percent",
            "lab_test_growth_date", "lab_test_growth_method", "lab_test_growth_amount", "lab_test_growth_person",
            "lab_test_strong_percent", "lab_test_strong_date", "lab_test_strong_method", "lab_test_strong_amount",
            "lab_test_strong_person", "lab_test_alive_percent", "lab_test_alive_date", "lab_test_alive_method",
            "lab_test_alive_amount", "lab_test_alive_person", "lab_seed_rest", "lab_seed_rest_fix_method",
            "lab_seed_rest_fix_date", "lab_seed_amount", "lab_10seed_weight", "lab_seed_est_amount", "lab_plaint_character",
            "lab_admin_name", "lab_remark"];

        warehouse = pickBy(warehouse, (data, key) => {
            return !KEYS.includes(key);
        });

        register_plant = pickBy(register_plant, (data, key) => {
            return !KEYS.includes(key);
        });


        return {...register_plant, ...warehouse, ...other};
    }

    public async submitIntoM9(plantIds = []): Promise<any> {
        return Promise.all(plantIds.map(async plantId => {
            const plant = await this.findById(plantId, "Plant", false);
            const updatedInfo: any = await this.update(plant, {
                //alertFormStatus: "complete"
                alertFormStatus: "pending"
            }, true);

            return this.wareHouseService.createWHAlert(updatedInfo.toJSON());
        }));
    }

    getModel() {
        return this.labAlertForm;
    }
    public async updateSpecificAlert(id, info): Promise<any> {	
        return this.labAlertForm.updateOne(	
            {_id:id}, 	
            {$set:{alert_test_amount:info.admin_amount,alertFormStatus:'approve', updatedAt: Date.now()}	
        });	
    }	
    public async listAlertM9(status = "pending"): Promise<any> {	
        const alertForm: any = await this.labAlertForm.find({	
            alertFormStatus: status	
        }).populate({	
            path: "warehouse",	
            populate: [	
                {path: "passport"},	
                {path: "volume"},	
                {path: "reg_plant_sci_name"},	
                {
                    path: "plant_gene_name",
                    populate: [{	
                        path: 'plantCategory'	
                    }]
                },	
                {path: "reg_gene_category"},	
                {path: "reg_gene_type"},	
                {path: "source_country"},	
                {path: "source_country_1"},	
                {path: "source_province"},	
                {path: "source_district"},	 
                {path: "createdBy"},	
                {path: "updatedBy"},	
                {path: "m8_lab_id",	
                populate: [{path: "createdBy"}]}	
            ]	
        }).populate({	
            path: "labId"	
        })	
        .sort({"updatedAt": "desc"});	
        return alertForm.map(alert => {	
            let {warehouse, ...other} = alert.display();	
            const KEYS = ["lab_test_plant_amount_input", "lab_test_plant_amount_dropdown",	
                "lab_test_plant_amount", "lab_test_moisture_date", "lab_test_moisture_method",	
                "lab_test_moisture_amount", "lab_test_moisture_person", "lab_test_growth_percent",	
                "lab_test_growth_date", "lab_test_growth_method", "lab_test_growth_amount", "lab_test_growth_person",	
                "lab_test_strong_percent", "lab_test_strong_date", "lab_test_strong_method", "lab_test_strong_amount",	
                "lab_test_strong_person", "lab_test_alive_percent", "lab_test_alive_date", "lab_test_alive_method",	
                "lab_test_alive_amount", "lab_test_alive_person", "lab_seed_rest", "lab_seed_rest_fix_method",	
                "lab_seed_rest_fix_date", "lab_seed_amount", "lab_10seed_weight", "lab_seed_est_amount", "lab_plaint_character",	
                "lab_admin_name", "lab_remark"];	
            warehouse = pickBy(warehouse, (data, key) => {	
                return !KEYS.includes(key);	
            });	
            return {...warehouse, ...other};	
        });	
    }
    public async listAlert(status = "pending"): Promise<any> {	
        const alertForm: any = await this.labAlertForm.find({	
            alertFormStatus: status	
        }).populate({	
            path: "warehouse",	
            populate: [	
                {path: "passport"},	
                {path: "volume"},	
                {path: "reg_plant_sci_name"},	
                {path: "plant_gene_name"},	
                {path: "reg_gene_category"},	
                {path: "reg_gene_type"},	
                {path: "source_country"},	
                {path: "source_country_1"},	
                {path: "source_province"},	
                {path: "source_district"},	
                {path: "createdBy"},	
                {path: "updatedBy"},	
                {path: "m8_lab_id",	
                populate: [{path: "createdBy"}]}	
            ]	
        }).populate({	
            path: "labId"	
        })	
            .sort({"updatedAt": "desc"});	
        return alertForm.map(alert => {	
            let {warehouse, ...other} = alert.display();	
            const KEYS = ["lab_test_plant_amount_input", "lab_test_plant_amount_dropdown",	
                "lab_test_plant_amount", "lab_test_moisture_date", "lab_test_moisture_method",	
                "lab_test_moisture_amount", "lab_test_moisture_person", "lab_test_growth_percent",	
                "lab_test_growth_date", "lab_test_growth_method", "lab_test_growth_amount", "lab_test_growth_person",	
                "lab_test_strong_percent", "lab_test_strong_date", "lab_test_strong_method", "lab_test_strong_amount",	
                "lab_test_strong_person", "lab_test_alive_percent", "lab_test_alive_date", "lab_test_alive_method",	
                "lab_test_alive_amount", "lab_test_alive_person", "lab_seed_rest", "lab_seed_rest_fix_method",	
                "lab_seed_rest_fix_date", "lab_seed_amount", "lab_10seed_weight", "lab_seed_est_amount", "lab_plaint_character",	
                "lab_admin_name", "lab_remark"];	
            warehouse = pickBy(warehouse, (data, key) => {	
                return !KEYS.includes(key);	
            });	
            return {...warehouse, ...other};	
        });	
    }
    public async listM9(): Promise<any> {	
        const where = {};	
        const wareHouseArr = [];	
        const warehouses: any = await this.findAll({}, "LabAlertForm")	
        for await (const warehouse of warehouses) {	
            const {_id} = warehouse;	
            let info = await this.labAlertForm.findOne({_id: _id})	
            .populate({	
                path: "labId",	
                populate: [	
                    {path: "createdBy"}	
                ]	
            })	
            const [draft, pending, approve, total] = await Promise.all([this.labAlertForm.count({	
                labId: _id,	
                alertFormStatus: "draft"	
            }), this.labAlertForm.count({	
                labId: _id,	
                alertFormStatus: "pending",	
                abId: _id	
            }), this.labAlertForm.count({	
                alertFormStatus: "pending",	
                abId: _id	
            }), this.labAlertForm.count({m5_assigned: _id})]);	
            wareHouseArr.push({...warehouse, pending, total, draft, approve,info});	
        }	
        return wareHouseArr;	
        // const alertForm: any = await this.labAlertForm.find({})	
        // .populate({	
        //     path: "warehouse",	
        //     populate: [	
        //         {path: "passport"},	
        //         {path: "volume"},	
        //         {path: "reg_plant_sci_name"},	
        //         {path: "plant_gene_name"},	
        //         {path: "reg_gene_category"},	
        //         {path: "reg_gene_type"},	
        //         {path: "source_country"},	
        //         {path: "source_country_1"},	
        //         {path: "source_province"},	
        //         {path: "source_district"},	
        //         {path: "createdBy"},	
        //         {path: "updatedBy"},	
        //         {path: "m8_lab_id",	
        //         populate: [{path: "createdBy"}]}	
        //     ]	
        // }).populate({	
        //     path: "labId"	
        // })	
        //     .sort({"updatedAt": "desc","alertFormStatus":"desc"});	
        // //return alertForm;	
        // return alertForm.map(alert => {	
        //     let {warehouse, ...other} = alert.display();	
        //     const KEYS = ["lab_test_plant_amount_input", "lab_test_plant_amount_dropdown",	
        //         "lab_test_plant_amount", "lab_test_moisture_date", "lab_test_moisture_method",	
        //         "lab_test_moisture_amount", "lab_test_moisture_person", "lab_test_growth_percent",	
        //         "lab_test_growth_date", "lab_test_growth_method", "lab_test_growth_amount", "lab_test_growth_person",	
        //         "lab_test_strong_percent", "lab_test_strong_date", "lab_test_strong_method", "lab_test_strong_amount",	
        //         "lab_test_strong_person", "lab_test_alive_percent", "lab_test_alive_date", "lab_test_alive_method",	
        //         "lab_test_alive_amount", "lab_test_alive_person", "lab_seed_rest", "lab_seed_rest_fix_method",	
        //         "lab_seed_rest_fix_date", "lab_seed_amount", "lab_10seed_weight", "lab_seed_est_amount", "lab_plaint_character",	
        //         "lab_admin_name", "lab_remark"];	
        //     warehouse = pickBy(warehouse, (data, key) => {	
        //         return !KEYS.includes(key);	
        //     });	
        //     return {...warehouse, ...other};	
        // });	
    }
    public async updateSpecificPlant(_id, status) {	
        // return this.labAlertForm.updateOne(	
        //     {_id:_id}, 	
        //     {$set:{alert_test_amount:info.admin_amount,alertFormStatus:status, updatedAt: Date.now()}	
        // });	
        return true;	
    }
    public async specificLabid(labId: any): Promise<any> {	
        const alertForm: any = await this.labAlertForm.find({	
            labId	
        }).populate({	
            path: "warehouse",	
            populate: [	
                {path: "passport"},	
                {path: "volume"},	
                {path: "reg_plant_sci_name"},	
                {path: "plant_gene_name"},	
                {path: "reg_gene_category"},	
                {path: "reg_gene_type"},	
                {path: "source_country"},	
                {path: "source_country_1"},	
                {path: "source_province"},	
                {path: "source_district"},	
                {path: "createdBy"},	
                {path: "updatedBy"}	
            ]	
        }).populate({	
            path: "labId"	
        })	
            .sort({"updatedAt": "desc"});	
            	
        return alertForm.map(alert => {	
            let {warehouse, ...other} = alert.display();	
            warehouse['wh_id'] = warehouse._id	
            const KEYS = ["lab_test_plant_amount_input", "lab_test_plant_amount_dropdown",	
                "lab_test_plant_amount", "lab_test_moisture_date", "lab_test_moisture_method",	
                "lab_test_moisture_amount", "lab_test_moisture_person", "lab_test_growth_percent",	
                "lab_test_growth_date", "lab_test_growth_method", "lab_test_growth_amount", "lab_test_growth_person",	
                "lab_test_strong_percent", "lab_test_strong_date", "lab_test_strong_method", "lab_test_strong_amount",	
                "lab_test_strong_person", "lab_test_alive_percent", "lab_test_alive_date", "lab_test_alive_method",	
                "lab_test_alive_amount", "lab_test_alive_person", "lab_seed_rest", "lab_seed_rest_fix_method",	
                "lab_seed_rest_fix_date", "lab_seed_amount", "lab_10seed_weight", "lab_seed_est_amount", "lab_plaint_character",	
                "lab_admin_name", "lab_remark"];	
            warehouse = pickBy(warehouse, (data, key) => {	
                return !KEYS.includes(key);	
            });	
            return {...warehouse, ...other};	
        });	
    }	
    public async submitIntoM9Complete(plantIds = []): Promise<any> {	
        return Promise.all(plantIds.map(async plantId => {	
            let plant = {};	
             plant = await this.findById(plantId, "LabAlertForm", false);
            let updatedInfo: {} = await this.update(plant, {	
                alertFormStatus: "complete",	
                register_alert_form:plantId,	
                wh_status:"complete"	
            }, true);	
            // updatedInfo['wh_status'] = "draft";	
            // updatedInfo['register_alert_form'] = plantId;	
            return this.wareHouseService.createWHAlert(updatedInfo);	
        }));	
    }

    public async specificId(id: any): Promise<any> {	
        const alertForm: any = await this.labAlertForm.findById(id)	
            .sort({"updatedAt": "desc"});	
        if (!alertForm) {	
            throw new Error("Plant not found");	
        }	
        return alertForm;	
    }

}
