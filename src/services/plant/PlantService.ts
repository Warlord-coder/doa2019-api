import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pickBy, toLower} from "lodash";
import {Plant} from "../../models/plant/Plant";

@Service()
export class PlantService extends BaseService<Plant> {

    constructor(@Inject(Plant) private plantModal: MongooseModel<Plant> | any) {
        super();
    }

    public async createPlant(plantInfo: any): Promise<any> {
        return this.create(plantInfo, false);
    }

    public async updatePlant(updatePlantInfo: any, id): Promise<any> {
        const plant = await this.findById(id, "Plant", false);

        return this.update(plant, updatePlantInfo, true);
    }

    public async list(where = {}): Promise<any> {
        const plants = await this.findAll(where, "Plant");

        return plants;
    }

    public async getSpecificPlant(id: string): Promise<any> {
        const plant = await this.findById(id, "Plant", false);

        return plant;
    }

    getModel() {
        return this.plantModal;
    }
}
