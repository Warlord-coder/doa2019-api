import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pickBy, toLower} from "lodash";
import {CommonCondition} from "../../models/common-condition/CommonCondition";

@Service()
export class CommonConditionService extends BaseService<CommonCondition> {

    constructor(@Inject(CommonCondition) private conditionModal: MongooseModel<CommonCondition> | any) {
        super();
    }

    public async createCondition(conditionInfo: any): Promise<any> {
        return this.create(conditionInfo, false);
    }

    public async updateCondition(updateConditionInfo: any, id): Promise<any> {
        const condition = await this.findById(id, "Condition", false);

        return this.update(condition, updateConditionInfo, false);
    }

    public async list(where = {}): Promise<any> {
        const countries = await this.findAll(where, "Countries");

        return countries;
    }

    public async getSpecificCondition(id: string): Promise<any> {
        const countries = await this.findById(id, "Condition", false);

        return countries;
    }

    getModel() {
        return this.conditionModal;
    }
}
