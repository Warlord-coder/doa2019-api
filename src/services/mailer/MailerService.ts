import * as nodemailer from "nodemailer";
import Config from "../../config";
import {readFileSync} from "fs";
import {$log} from "ts-log-debug";
import * as moment from "moment";
import {template} from "lodash";
import {Utility} from "../../utils/constants";
import * as fs from "fs-extra";

export default class MailerService {
    private static _mailerServiceInstance;

    public static getInstance(): MailerService {
        if (this._mailerServiceInstance) {
            return this._mailerServiceInstance;
        }
        this._mailerServiceInstance = new MailerService();

        return this._mailerServiceInstance;
    }

    /**
     *
     * @param mailOptions
     * @private
     */
    private _sendMail = (mailOptions): Promise<any> => {
        const {subject = ""} = mailOptions;
        mailOptions.subject = `${process.env.EMAIL_PREFIX || ""}${subject}`;
        const transporter = nodemailer.createTransport(Config.EMAIL_CONFIG.smtpConfig);

        return transporter.sendMail(mailOptions);
    };

    /**
     *
     * @param to
     * @param emailType
     * @private
     */
    private async _getMailOptions(to: String, emailType: string): Promise<any> {
        const {subject = "", bodyPath = ""} = Config.EMAIL_TEMPLATES[emailType] || {};
        const html = await readFileSync(bodyPath, "utf8");

        return {
            from: Config.EMAIL_CONFIG.from,
            to, subject, html
        };
    }

    /**
     * Reset Password Link Email
     * @param to
     * @param params
     */
    public resetPassword = (to: String, params: any): void => {
        const {fullName = "", resetPasswordToken = ""} = params || {};
        this._getMailOptions(to, "resetPassword")
            .then((options = {}) => {
                options.html = template(options.html)({fullName, to, resetPasswordToken});

                return this._sendMail(options);
            })
            .then(() => $log.info("Reset Password Link Email to user Sent"))
            .catch(err => $log.error(err));
    };

    public verifyAccount = (to: String, inviteToken: String) => {
        this._getMailOptions(to, "verifyAccount")
            .then((options = {}) => {
                options.html = template(options.html)({to, inviteToken});

                return this._sendMail(options);
            })
            .then(() => $log.info("Verify Account Email to user Sent"))
            .catch(err => $log.error(err));
    };

    /**
     * Event created Email
     * @param to
     * @param params
     */
    public eventCreated = (to: String, params: any): void => {
        const {
            fullName = "", eventTimeInterval = "", inviteeName = "", inviteeMail = "",
            startDateTime = "", endDateTime = "", timeZone = ""
        } = params || {};
        this._getMailOptions(to, "eventCreated")
            .then((options = {}) => {
                options.subject = template(options.subject)({
                    eventTimeInterval, inviteeName, eventTime: moment(startDateTime).utc().format("Do MMMM YYYY")
                });
                options.html = template(options.html)({
                    fullName,
                    to,
                    eventTimeInterval,
                    inviteeName,
                    inviteeMail,
                    eventTime: `${Utility.getFormattedDateTime(startDateTime, "hh:mm a", timeZone)} -
                                ${Utility.getFormattedDateTime(endDateTime, "hh:mm a", timeZone)} -
                                ${Utility.getFormattedDateTime(startDateTime, "Do MMMM YYYY", timeZone)} (${timeZone})`,
                    timeZone
                });

                return this._sendMail(options);
            })
            .then(() => $log.info("New Event creation email sent to the user"))
            .catch(err => $log.error(err));
    };

    /**
     * Event Updated Email
     * @param to
     * @param params
     */
    public eventUpdated = (to: String, params: any): void => {
        const {
            fullName = "", eventTimeInterval = "", inviteeName = "", inviteeMail = "",
            startDateTime = "", endDateTime = "", timeZone = "", formarEventTime = "", reason = ""
        } = params || {};
        this._getMailOptions(to, "eventUpdated")
            .then((options = {}) => {
                options.subject = template(options.subject)({
                    eventTimeInterval, inviteeName, eventTime: moment(startDateTime).format("Do MMMM YYYY")
                });
                options.html = template(options.html)({
                    fullName,
                    to,
                    eventTimeInterval,
                    inviteeName,
                    inviteeMail,
                    eventTime: `${Utility.getFormattedDateTime(startDateTime, "hh:mm a", timeZone)} -
                                ${Utility.getFormattedDateTime(endDateTime, "hh:mm a", timeZone)} -
                                ${Utility.getFormattedDateTime(startDateTime, "Do MMMM YYYY", timeZone)} (${timeZone})`,
                    timeZone,
                    formarEventTime: `${Utility.getFormattedDateTime(formarEventTime, "hh:mm a", timeZone)} -
                                ${Utility.getFormattedDateTime(formarEventTime, "Do MMMM YYYY", timeZone)} (${timeZone})`,
                    reason
                });

                return this._sendMail(options);
            })
            .then(() => $log.info("Event updated email sent to the user"))
            .catch(err => $log.error(err));
    };

    /**
     * Event Updated Email
     * @param to
     * @param params
     */
    public eventCanceled = (to: String, params: any): void => {
        const {
            fullName = "", eventTimeInterval = "", inviteeName = "", inviteeMail = "", organizerName = "",
            organizerEmail = "", startDateTime = "", endDateTime = "", timeZone = "", reason = ""
        } = params || {};
        this._getMailOptions(to, "eventCanceled")
            .then((options = {}) => {
                options.subject = template(options.subject)({
                    eventTimeInterval, inviteeName, eventTime: moment(startDateTime).utc().format("Do MMMM YYYY")
                });
                options.html = template(options.html)({
                    fullName,
                    to,
                    eventTimeInterval,
                    organizerName,
                    organizerEmail,
                    inviteeName,
                    inviteeMail,
                    eventTime: `${Utility.getFormattedDateTime(startDateTime, "hh:mm a", timeZone)} -
                                ${Utility.getFormattedDateTime(endDateTime, "hh:mm a", timeZone)}
                                ${Utility.getFormattedDateTime(startDateTime, "Do MMMM YYYY", timeZone)} (${timeZone})`,
                    timeZone,
                    reason
                });

                return this._sendMail(options);
            })
            .then(() => $log.info("Event Canceled email sent to the user"))
            .catch(err => $log.error(err));
    };

    /**
     * Post call summary
     * @param to
     * @param params
     */
    public postCallSummary = (to: String, params: any, callId: string): void => {
        const {
            fullName = "", userToCall = "", callDateTime = "", callDuration = "", sentimentalAnalysis = "", shareLink = "", link = "",
            transcription = "", linkToRecordedCall = "", isRecordingEnabled = false, chatMsgs = [], keywords = []
        } = params || {};
        const path = `${Config.UPLOAD_URL}/${Date.now()}.png`;
        this._getMailOptions(to, "postCallSummary")
            .then(async (options = {}) => {
                options.subject = template(options.subject)({
                    userToCall
                });
                options.html = template(options.html)({
                    fullName,
                    to,
                    userToCall,
                    callDateTime,
                    callDuration,
                    sentimentalAnalysis,
                    transcription,
                    linkToRecordedCall,
                    isRecordingEnabled,
                    chatMsgs,
                    keywords,
                    shareLink, link
                });
                await Utility.saveFileAsPdf(callId, Config.SECRET_TOKEN, path);
                options.attachments = [{filename: "call-report.pdf", path, contentType: "application/pdf"}];
                $log.info(options);

                return this._sendMail(options);
            })
            .then(() => {
                fs.remove(path);
                $log.info("Post Call Summary Email sent to the User");
            })
            .catch(err => {
                fs.remove(path);
                $log.error(err);
            });
    };

    /**
     * Post call summary
     * @param to
     * @param params
     */
    public postCallAlerts = (to: String, params: any): void => {
        const {
            fullName = "", userToCall = "", callDateTime = "", callDuration = "", linkToRecordedCall = "",
            alertCount = 0, shareableLink = null
        } = params || {};
        const alertCountSubject = alertCount > 1 ? `${alertCount} Alerts` : `${alertCount} Alert`;
        this._getMailOptions(to, "postCallAlert")
            .then((options = {}) => {
                options.subject = template(options.subject)({
                    userToCall, alertCountSubject
                });
                options.html = template(options.html)({
                    fullName,
                    to,
                    userToCall,
                    callDateTime,
                    callDuration,
                    shareableLink,
                    linkToRecordedCall
                });

                return this._sendMail(options);
            })
            .then(() => $log.info("Post Call Alerts Email sent to the User"))
            .catch(err => $log.error(err));
    };

    /**
     * Subscription Suspended
     * @param to
     * @param params
     */
    public subscriptionSuspended = (to: String, params: any): void => {
        const { name = "", cardErrorMessage = "", nextPaymentAttempt = "" } = params || {};
        this._getMailOptions(to, "subscriptionSuspended")
            .then((options = {}) => {
                options.html = template(options.html)({
                    name,
                    to,
                    cardErrorMessage,
                    nextPaymentAttempt
                });

                return this._sendMail(options);
            })
            .then(() => $log.info("Subscription Plan Suspend Email Sent"))
            .catch(err => $log.error(err));
    }

    /**
     * Subscription Suspended
     * @param to
     * @param params
     */
    public subscriptionErrorMail = (to: String, params: any): void => {
        const { message = "", body = ""} = params || {};
        this._getMailOptions(to, "subscriptionErrorMail")
            .then((options = {}) => {
                options.html = template(options.html)({
                    to,
                    message,
                    body
                });

                return this._sendMail(options);
            })
            .then(() => $log.info("Subscription Error Email Sent"))
            .catch(err => $log.error(err));
    }

    /**
     * Process Subscription
     * @param to
     * @param params
     */
    public processSubscription = (to: String, params: any): void => {
        const { name = "", amount = "", dateTime = "", invoiceUrl = "" } = params || {};
        this._getMailOptions(to, "processSubscription")
            .then((options = {}) => {
                options.html = template(options.html)({
                    to,
                    name,
                    amount,
                    dateTime,
                    invoiceUrl
                });

                return this._sendMail(options);
            })
            .then(() => $log.info("Subscription Plan Processed Email Sent"))
            .catch(err => $log.error(err));
    }
}
