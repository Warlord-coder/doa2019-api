import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pickBy, toLower} from "lodash";
import {Volume} from "../../models/volume/Volume";
import {PlantRegister} from "../../models/plant-register/PlantRegister";

@Service()
export class VolumeService extends BaseService<Volume> {

    constructor(@Inject(Volume) private volumeModal: MongooseModel<Volume> | any,
                @Inject(PlantRegister) private plantRegisterModal: MongooseModel<PlantRegister> | any) {
        super();
    }

    public async createVolume(volumeInfo: any): Promise<any> {
        return this.create(volumeInfo, false);
    }

    public async updateVolume(updateVolumeInfo: any, id): Promise<any> {
        const volume = await this.findById(id, "Volume", false);

        return this.update(volume, updateVolumeInfo, false);
    }

    public async list(passport_no, filter = null): Promise<any> {
        let where = {};
        if (passport_no !== "all") {
            where = {passport_no};
        }
        let volumes: any = await this.findAll(where, "Volume", "passport_no");

        volumes = await Promise.all(volumes.map(async volume => {
            const {_id} = volume;
            const [pendingPlant, completedPlantsInM3, totals] = await Promise.all([this.plantRegisterModal.count({
                volume: _id,
                m3_status: "draft",
                parentPlant: null
            }), this.plantRegisterModal.count({
                volume: _id,
                m3_status: "complete",
                parentPlant: null,
                m8_lab_id: null,
            }), this.plantRegisterModal.count({
                volume: _id,
                parentPlant: null
            })]);


            return {...volume, pendingPlant, totals, completedPlantsInM3};
        }));

        if (filter === "completed_plant_m3") {
            return volumes.filter(({completedPlantsInM3}) => completedPlantsInM3);
        }

        return volumes;
    }

    public async getSpecificVolume(id: string): Promise<any> {
        const Volumes = await this.findById(id, "Volume", true);

        const totals = await this.plantRegisterModal.count({
            volume: id,
            parentPlant: null
        });

        return {...Volumes, totals};
    }

    public async findByVolumeNumber(volume_no): Promise<any> {
        const Volume = await this.findOne({volume_no}, "Volume", true);

        return Volume;
    }

    public countVolume(passport_no = null): Promise<any> {
        const where: any = {};
        if (passport_no) {
            where.passport_no = passport_no;
        }

        return this.volumeModal.count(where);
    }

    getModel() {
        return this.volumeModal;
    }
}
