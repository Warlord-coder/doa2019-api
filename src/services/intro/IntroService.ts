import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {Intro} from "../../models/frontend-intro/Intro";

@Service()
export class IntroService extends BaseService<Intro> {

    constructor(@Inject(Intro) private introModel: MongooseModel<Intro> | any) {
        super();
    }

    public async createEventNews(eventNewsInfo: any): Promise<any> {
        return this.create(eventNewsInfo, false);
    }

    public async updateEventNews(updateEventNewsInfo: any, id): Promise<any> {
        const eventNews = await this.findById(id, "EventNews", false);

        return this.update(eventNews, updateEventNewsInfo, false);
    }

    public async list(where = {}): Promise<any> {
        const eventNews = await this.findAll(where, "EventNews");

        return eventNews;
    }

    public async getSpecificEventNews(id: string): Promise<any> {
        const eventNews = await this.findById(id, "EventNews", false);

        return eventNews;
    }

    getModel() {
        return this.introModel;
    }
}
