import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pickBy, toLower} from "lodash";
import {AttributeSet} from "../../models/attribute-set/AttributeSet";

@Service()
export class AttributeSetService extends BaseService<AttributeSet> {

    constructor(@Inject(AttributeSet) private attributeModal: MongooseModel<AttributeSet> | any) {
        super();
    }

    public async createAttribute(attributeInfo: any): Promise<any> {
        return this.create(attributeInfo, false);
    }

    public async updateAttribute(updateAttributeInfo: any, id): Promise<any> {
        const attribute = await this.findById(id, "Attribute", false);

        return this.update(attribute, updateAttributeInfo, false);
    }

    public async list(): Promise<any> {
        const countries = await this.findAll({}, "Countries");

        return countries;
    }

    public async getSpecificAttribute(id: string): Promise<any> {
        const countries = await this.findById(id, "Attribute", false);

        return countries;
    }

    public async getFilterList(filter): Promise<any> {
        let orWhere = [];
        let {start, length, keyword} = filter;
        if(keyword != '') {
            orWhere = [];
            orWhere.push({name_en: {$regex: '.*' + keyword + '.*', $options: 'i'}});
            orWhere.push({code: {$regex: '.*' + keyword + '.*', $options: 'i'}});
            orWhere.push({fieldInput: {$regex: '.*' + keyword + '.*', $options: 'i'}});
        } else {
            orWhere = [{}];
        }
        let modelObj = await this.attributeModal.find({$or: orWhere}).populate('createdBy').skip(parseInt(start)).limit(parseInt(length))
            
        modelObj = modelObj.map(obj => obj.display());

        let totalRows = await this.attributeModal.find({$or: orWhere}).count();
        
        return {data: modelObj, total_rows: totalRows};
    }

    getModel() {
        return this.attributeModal;
    }
}
