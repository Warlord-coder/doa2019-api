import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pickBy, toLower, pick} from "lodash";
import {Utility} from "../../utils/constants";
import {Recovery} from "../../models/recovery/Recovery";
import {WareHouseService} from "../ware-house/WareHouseService";
import {PlantRegisterService} from "../plant-register/PlantRegisterService";
import {WareHouse} from "../../models/warehouse/WareHouse";

@Service()
export class RecoverService extends BaseService<Recovery> {

    constructor(@Inject(Recovery) private recoverModal: MongooseModel<Recovery> | any,
                private warehouseService: WareHouseService,
                @Inject(WareHouse) private wareHouseModal: MongooseModel<WareHouse> | any,
                private registerService: PlantRegisterService) {
        super();
    }

    public async createRecovery(recoverInfo: any): Promise<any> {
        const totalRecords = await this.recoverModal.count();

        return this.create({...recoverInfo, m_no: "M" + String(Utility.getFormattedNumber(totalRecords + 1))}, false);
    }

    public async updateRecovery(updateRecoveryInfo: any, id): Promise<any> {
        const recover = await this.findById(id, "Recovery", false);

        return this.update(recover, updateRecoveryInfo, true);
    }

    public async submitToRecovery(plantIds: any, id, status = "draft", _id): Promise<any> {
        if (status == null) status = "draft"
        const recover = await this.findById(id, "Recovery", false);
        if(_id != undefined && _id != null)
        {
            for await (const plantId of plantIds) {
                await this.warehouseService.updatePlant({
                    recovery_id: id,
                    recovery_status: status || "draft",
                    m4recovery_plant_submittedAt : Date.now(),
                    m4recovery_plant_submittedBy : _id,
                }, plantId);
            }
        }
        else{

            for await (const plantId of plantIds) {
                await this.warehouseService.updatePlant({
                    recovery_id: id,
                    recovery_status: status || "draft"
                }, plantId);
            }
        }
        return recover;

    }

    public async submitToRegisterModule(plantIds: any, id, createdBy = null): Promise<any> {
        for await (const plantId of plantIds) {
            await this.warehouseService.updatePlant({
                m3_recovery_assign: id
            }, plantId);
            await this.registerService.createPlant({
                m9warehouse_plant_refer_id: plantId,
                // warehouse_id: plantId,
                volume: id,
                m3_status: "draft",
                createdBy
            }, false);
        }

        return null;
    }

    public async list(where = {}): Promise<any> {
        const recoveries: any = await this.findAll(where, "Recovery");
        const recoveriesList = [];
        for await (const recovery of recoveries) {
            const {_id: recovery_id} = recovery.display ? recovery.display() : recovery;
            const [totals, drafts] = await Promise.all([this.wareHouseModal.count({recovery_id}), this.wareHouseModal.count({
                recovery_id,
                //recovery_status: "draft"
                $or:[{recovery_status: "draft"},{recovery_status: "pending"},{recovery_status: "approve"}]
            })]);
            recoveriesList.push({...recovery, totals, drafts});
        }

        return recoveriesList;
    }

    public async getSpecificRecovery(id: string): Promise<any> {
        const [recovery, totalPlants] = await Promise.all([this.findById(id, "Recovery", true), this.wareHouseModal.count({recovery_id: id})]);

        return {...recovery, totalPlants};
    }


    public async getSpecificRecoveryPlants(id: string, status): Promise<any> {
        const [recovery, alertRecovery] = await Promise.all([this.warehouseService.list(null, {
            recovery_status: status,
            recovery_id: id
        }), this.warehouseService.listAlert(null, {
            recovery_status: status,
            recovery_id: id
        })]);

        return [...recovery, ...alertRecovery];
    }

    getModel() {
        return this.recoverModal;
    }

    public async updateSpecificPlant(_id, updatedInfo) {	
        // const modelObj = await this.withDrawPlantModal.findOne({_id, deletedAt: null});	
        // if (!modelObj) {	
        //     return null;	
        // }	
        // Object.keys(updatedInfo).forEach(key => {	
        //     modelObj[key] = updatedInfo[key];	
        // });	
        // return modelObj.save();	
        return null	
    }
    public async getRecoverycIdStatus(id: string,status): Promise<any> {	
        const alertForm: any = await this.wareHouseModal.find({	
            recovery_id:id,	
            recovery_status:status	
        }).populate("passport")	
        .populate("volume")	
        .populate("reg_plant_sci_name")	
        .populate("plant_gene_name")	
        .populate("reg_gene_type")	
        .populate("reg_gene_category")	
        .populate("source_country")	
        .populate("source_country_1")	
        .populate("source_province")	
        .populate("source_district")	
        .populate("updatedBy")	
        .populate("createdBy")	
        .populate("recovery_id")	
        .sort({"updatedAt": "desc"});	
        return alertForm;	
    }	
    public async getSpecificId(id: string): Promise<any> {	
        const alertForm: any = await this.wareHouseModal.find({	
            recovery_id:id	
        }).populate("passport")	
        .populate("volume")	
        .populate("reg_plant_sci_name")	
        .populate("plant_gene_name")	
        .populate("reg_gene_type")	
        .populate("reg_gene_category")	
        .populate("source_country")	
        .populate("source_country_1")	
        .populate("source_province")	
        .populate("source_district")	
        .populate("updatedBy")	
        .populate("createdBy")	
        .populate("recovery_id")	
        .sort({"updatedAt": "desc"});	
        return alertForm;	
    }

    public async getByPlace(filter): Promise<any> {
        let where: any = {};
        where.recovery_id = {$ne: null};
        // where.recovery
        const recoveryList: any = await this.wareHouseModal.find(where)
            .populate("recovery_id")
            .sort({"updatedAt": "desc"});	
        return recoveryList;
    }

    public async getByCategory(filter): Promise<any> {
        let where: any = {};
        where.recovery_id = {$ne: null};
        // where.recovery
        const recoveryList: any = await this.wareHouseModal.find(where)
            .populate("recovery_id")
            .populate({
                path: "reg_plant_sci_name",
                populate: [{path: "plant_category"}]
            })
            .sort({"updatedAt": "desc"});	
        return recoveryList;
    }

}
