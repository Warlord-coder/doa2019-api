import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pickBy, toLower} from "lodash";
import {Passport} from "../../models/passport/Passport";
import {VolumeService} from "../volume/VolumeService";
import {Utility} from "../../utils/constants";
import {PlantRegister} from "../../models/plant-register/PlantRegister";
import {PlantCategory} from "../../models/plant-category/PlantCategory";
import { start } from "repl";
@Service()
export class PassportRegisterService extends BaseService<Passport> {

    constructor(@Inject(Passport) private passportRegisterModal: MongooseModel<Passport> | any,
                @Inject(PlantRegister) private plantRegisterModal: MongooseModel<PlantRegister> | any,
                @Inject(PlantCategory) private plantCategoryModal: MongooseModel<PlantCategory> | any,
                private volumeService: VolumeService) {
        super();
    }

    public async createPassport(passportRegisterInfo: any): Promise<any> {
        const totalRecords = await this.passportRegisterModal.count();
        
        passportRegisterInfo.isNewPassport = true;
        return this.create({
            ...passportRegisterInfo,
            passport_no: String(Utility.getFormattedNumber(totalRecords + 1))
        }, false);
    }

    public async updatePassport(updatePassportInfo: any, id): Promise<any> {
        const passportRegister = await this.findById(id, "Passport", false);
        if(updatePassportInfo.isNewPassport == false)
            passportRegister.isNewPassport =false
        return this.update(passportRegister, updatePassportInfo, true);
    }

    public async list(status): Promise<any> {
        const passports: any = await this.findAll({}, "Passport");
        const filterPassports = [];
        for await (const passport of passports) {
            const {_id} = passport;
            const [totalVols, pendingPlant ,completedPlantsInM3] = await Promise.all([
                this.volumeService.countVolume(_id),
               // this.volumeService.countVolume(),
                this.plantRegisterModal.count({passport: _id, m3_status: "draft", parentPlant: null}),
                this.plantRegisterModal.count({
                    passport: _id,
                    m3_status: "complete",
                    parentPlant: null
                })
            ]);
            if ((pendingPlant && status === "draft") || (!pendingPlant && status === "draft" && passport.isNewPassport) || (!pendingPlant && status === "completed" && !passport.isNewPassport)) {

                filterPassports.push({...passport, totalVols, pendingPlant, completedPlantsInM3});
            }
        }

        return Promise.all(filterPassports);
    }

    public async exportExcel(startDt, endDt): Promise<any> {

        let where = {reg_plant_sci_name: {$ne: null}};
        let plantCategories : any = await this.plantCategoryModal.find({});
        const plantRegisters: any = await this.plantRegisterModal.find(where).populate('volume').populate('reg_plant_sci_name');
        
        return {categories: plantCategories, registers: plantRegisters};
    }

    public async getSpecificPassport(id: string): Promise<any> {
        const passports = await this.findById(id, "Passport", false);

        return passports;
    }

    getModel() {
        return this.passportRegisterModal;
    }
}
