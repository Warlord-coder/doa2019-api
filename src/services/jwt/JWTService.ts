import * as jwt from "jsonwebtoken";
import * as Promise from "bluebird";
import Config from "../../config";
import * as expressJwt from "express-jwt";


const jwtSign = Promise.promisify(jwt.sign);
const validateJwt = expressJwt({
    secret: Config.JWT_SECRET
});

export default class JWTService {
    public static sign(id: String, role: String, options: Object, method: any = jwtSign): Promise<any> {
        return method({id, role}, Config.JWT_SECRET, options);
    }

    public static verify(req, res, next): Promise<any> {
        return validateJwt(req, res, next);
    }
}
