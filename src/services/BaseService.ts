import {BaseModel} from "../models/Base";
import {pickBy, identity} from "lodash";


export abstract class BaseService<T extends BaseModel> {
    public abstract getModel();

    protected async create(data: object, isDisplay = true): Promise<T> {
        const model = this.getModel();
        let modelObj = new model(data);
        modelObj = await modelObj.save();

        return isDisplay ? modelObj.display() : modelObj;
    }

    protected async findById(id: string, modelName: string, isDisplay = true, isError = true): Promise<T> {
        const modelObj = await this.getModel().findById(id);
        if (modelObj && !modelObj.deletedAt) {
            return isDisplay ? modelObj.display() : modelObj;
        }

        if (isError) {
            throw new Error(`${modelName} not found by this id`);
        }

        return null;
    }

    protected async findOne(where = {}, modelName: string, isError = true, isDisplay = true): Promise<T> {
        const modelObj = await this.getModel().findOne({...where, deletedAt: null})
            .populate("updatedBy")
            .populate("createdBy");
        if (modelObj && !modelObj.deletedAt) {
            return isDisplay ? modelObj.display() : modelObj;
        }

        if (isError) {
            throw new Error(`${modelName} not found`);
        }

        return modelObj;
    }

    protected async findAll(where = {}, modelName: string, populate = "", paginated = {}, isDisplay = true): Promise<T> {
        const {pageNo = null, pageSize = 15, order = "updatedAt", orderBy = "desc"}: any = paginated;
        let modelQuery = this.getModel().find({...where, deletedAt: null})
            .populate(populate)
            .populate("updatedBy")
            .populate("createdBy")
            .sort({[order]: orderBy});
        if (pageNo !== null) {
            modelQuery = this.getModel().find({
                ...where,
                deletedAt: null
            }).populate(populate)
                .populate("updatedBy")
                .populate("createdBy")
                .skip(pageNo * pageSize).limit(pageSize)
                .sort({[order]: orderBy});
        }
        const modelObj = await modelQuery;

        return isDisplay ? modelObj.map(obj => obj.display()) : modelObj;
    }

    protected async createAndUpdateById(id = {}, data, isDisplay = true): Promise<T> {
        const modelObj = await this.getModel().findByIdAndUpdate(id, data, {new: true}, (err, obj) => {
            if (err) {
                throw err;
            }

            return obj;
        });

        return isDisplay ? modelObj.display() : modelObj;
    }

    protected async createAndUpdate(where = {}, data, isDisplay = true): Promise<T> {
        const modelObj = await this.getModel().findOneAndUpdate(where, data, {
            upsert: true,
            new: true,
            returnNewDocument: true
        });

        return isDisplay ? modelObj.display() : modelObj;
    }

    protected async update(modelObj: any, updateVal = {}, removeEmptyVal = true): Promise<T> {
        if (removeEmptyVal) {
            updateVal = pickBy(updateVal, identity);
        }
        Object.keys(updateVal).forEach(key => {
            modelObj[key] = updateVal[key];
        });

        return modelObj.save();
    }
}
