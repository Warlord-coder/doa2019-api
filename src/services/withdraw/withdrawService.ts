import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pickBy, toLower} from "lodash";
import {Volume} from "../../models/volume/Volume";
import {PlantRegister} from "../../models/plant-register/PlantRegister";
import {WareHouse} from "../../models/warehouse/WareHouse";
import {WithDraw} from "../../models/withdraw/WithDraw";
import {Utility} from "../../utils/constants";
import {WithDrawPlant} from "../../models/withdraw-plant/WithDrawPlant";

@Service()
export class WithdrawService extends BaseService<Volume> {

    constructor(
        @Inject(WareHouse) private wareHouseModal: MongooseModel<WareHouse> | any,
        @Inject(Volume) private volumeModal: MongooseModel<Volume> | any,
        @Inject(WithDraw) private withDrawModal: MongooseModel<WithDraw> | any,
        @Inject(WithDrawPlant) private withDrawPlantModal: MongooseModel<WithDrawPlant> | any,
        @Inject(PlantRegister) private plantRegisterModal: MongooseModel<PlantRegister> | any) {
        super();
    }

    public async createWithDrawal(withDrawlInfo: any): Promise<any> {
        const count = await this.withDrawModal.count();

        return this.create({...withDrawlInfo, w_no: `W${Utility.getFormattedNumber(count + 1)}`}, false);
    }

    public async submitM5(plantIds, id): Promise<any> {
        const warehouses = await this.wareHouseModal.find({_id: {$in: plantIds}, m5_assigned: null, submitTo: "m5"});
        for await (const warehouse of warehouses) {
            if (warehouse) {
                warehouse.m5_assigned = id;
                await warehouse.save();
                // await this.withDrawPlantModal.create({...warehouse.display(), withDrawStatus: "draft"});
                await this.withDrawPlantModal.create({
                    m5_assigned:id , 
                    withDrawStatus: "draft", 
                    m9plant_warehouse_id : warehouse._id, 
                });
            }
        }

        return null;
    }

    public async updateWithDraw(updateVolumeInfo: any, id): Promise<any> {
        const volume: any = await this.findById(id, "Volume", false);

        return this.update(volume, updateVolumeInfo, false);
    }

    public async unassignedList(): Promise<any> {
        const warehouses = await this.wareHouseModal.find({submitTo: "m5", m5_assigned: null})
            .populate("updatedBy")
            .populate("createdBy")
            .populate("passport")
            .populate({
                path: "plant_gene_name",
                populate: [{
                    path: "plantCategory"
                }]
            })
            .populate("source_province")
            .populate("reg_plant_sci_name")
            .populate("volume");

        return warehouses.map(warehouse => warehouse.display());
    }

    public async list(module): Promise<any> {
        const where = {};
        const wareHouseArr = [];
        const warehouses: any = await this.findAll({}, "With Draw");

        for await (const warehouse of warehouses) {
            const {_id} = warehouse;
            const [draft, pending, approve, total] = await Promise.all([this.withDrawPlantModal.count({
                m5_assigned: _id,
                withDrawStatus: "draft"
            }), this.withDrawPlantModal.count({
                m5_assigned: _id,
                withDrawStatus: "pending",
                admin_amount: null
            }), this.withDrawPlantModal.count({
                m5_assigned: _id,
                withDrawStatus: "pending",
                admin_amount: {$ne: null}
            }), this.withDrawPlantModal.count({m5_assigned: _id})]);
            wareHouseArr.push({...warehouse, draft, pending, approve, total});
        }

        return wareHouseArr;
    }

    public async withdrawplantlist(): Promise<any> {
        const withdrawplants: any = await this.withDrawPlantModal.find({});
        return withdrawplants;
    }
    public async requestlist(module): Promise<any> {
        const where = {};
        const wareHouseArr = [];
        const warehouses: any = await this.findAll({}, "With Draw");

        for await (const warehouse of warehouses) {
            const {_id} = warehouse;
            const [draft, pending, approve, total] = await Promise.all([this.withDrawPlantModal.count({
                m5_assigned: _id,
                withDrawStatus: "draft"
            }), this.withDrawPlantModal.count({
                m5_assigned: _id,
                withDrawStatus: "pending",
                admin_amount: null
            }), this.withDrawPlantModal.count({
                m5_assigned: _id,
                withDrawStatus: "pending",
                admin_amount: {$ne: null}
            }), this.withDrawPlantModal.count({m5_assigned: _id})]);
            if(draft!=total)
                wareHouseArr.push({...warehouse, draft, pending, approve, total});
        }

        return wareHouseArr;
    }

    public async getSpecificWithDrawPlant(_id) {
        const modelObj = await this.withDrawPlantModal.findOne({_id, deletedAt: null})
            .populate({
                path: "register_alert_form",
                populate: [{
                    path: "warehouse",
                    populate: [{
                        path: "m8_lab_id"
                    }]
                }, {
                    path: "labId"
                }]
            });

        if (modelObj) {
            const {register_alert_form = {}, ...other} = modelObj.display();
            const {warehouse = {}} = register_alert_form;
            delete register_alert_form.warehouse;

            return {...warehouse, ...register_alert_form, ...other};
        }

        throw new Error("Not Found");
    }

    public async updateSpecificPlant(_id, updatedInfo) {
        const modelObj = await this.withDrawPlantModal.findOne({_id, deletedAt: null});

        if (!modelObj) {
            return null;
        }

        Object.keys(updatedInfo).forEach(key => {
            modelObj[key] = updatedInfo[key];
        });

        return modelObj.save();
    }

    public async renewWarehousePlant(_id) {
        const modelObj = await this.withDrawPlantModal.findOne({_id, deletedAt: null});
        console.log(modelObj.m9plant_warehouse_id)
        if(modelObj.m9plant_warehouse_id){
            const warehouses = await this.wareHouseModal.find({_id : modelObj.m9plant_warehouse_id})
            for await (const warehouse of warehouses) {
                if (warehouse) {
                    warehouse.m5_assigned = null;
                    await warehouse.save();
                }
            }
            console.log(warehouses)
        }

        if (!modelObj) {
            return null;
        }
        return;
    }

    public async getSpecificWithDraw(id: string): Promise<any> {
        const Volumes = await this.findById(id, "WithDraw", true);
        const [pending, total] = await Promise.all([this.withDrawPlantModal.count({
            m5_assigned: id,
            distributionStatus: ["draft", "pending"]
        }), this.withDrawPlantModal.count({m5_assigned: id})]);

        return {...Volumes, pending, total};
    }

    public async getPlantsWithDrawList(id: string, withDrawStatus: string): Promise<any> {
        const Volumes = await this.withDrawPlantModal.find({m5_assigned: id, withDrawStatus})
            .populate("updatedBy")
            .populate("createdBy")
            .populate("passport")
            .populate("volume");

        return Volumes;
    }

    public async getPlantsWithDrawListStatus(withDrawStatus: string): Promise<any> {
        const Volumes = await this.withDrawPlantModal.find({withDrawStatus:withDrawStatus})
            .populate("updatedBy")
            .populate("createdBy")
            .populate("passport")
            .populate("reg_gene_type")
            .populate("reg_plant_sci_name")
            .populate("reg_gene_category")
            .populate("m5_assigned")
            .populate({path:"plant_gene_name",populate:[{path:"plantCategory"},{path:"plantSpecie"}]})
            .populate("volume");
            const plantList = [];
            for await (const obj of Volumes) {
                if(obj.m9plant_warehouse_id!=null){
                    const [wh] = await Promise.all([
                        this.wareHouseModal.findOne({_id:obj.m9plant_warehouse_id})
                        .populate("reg_gene_type")
                        .populate({path:"plant_gene_name",populate:[{path:'plantCategory'},{path:'plantSpecie'}]})
                        .populate("reg_plant_sci_name")
                        .populate("reg_gene_category")
                    ]);
                    plantList.push({obj,wh});
                }
            }
           
        return plantList;
        //return Volumes;
    }

    public async findByVolumeNumber(volume_no): Promise<any> {
        const Volume = await this.findOne({volume_no}, "Volume", true);

        return Volume;
    }

    public countVolume(passport_no = null): Promise<any> {
        const where: any = {};
        if (passport_no) {
            where.passport_no = passport_no;
        }

        return this.volumeModal.count(where);
    }

    getModel() {
        return this.withDrawModal;
    }
    public async getByDepartment(submitTo = null, filter = {}){
        const withdraws = await this.withDrawPlantModal.find().populate('m5_assigned');
        return withdraws;
    }
    public async getByPlantType(submitTo = null, filter = {}){
        const warehouses = await this.wareHouseModal.find({'m5_assigned': {$ne: null}}).populate({
            path: "reg_plant_sci_name",
            populate: [{ path: "plant_category" }]
        });
        const withdraws = await this.withDrawPlantModal.find().populate({
            path: "reg_plant_sci_name",
            populate: [{path: "plant_category"}]
        }).populate('m5_assigned');

        return {warehouse: warehouses, withdraws: withdraws};
    }
}
