import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {User} from "../../models/users/User";
import {BaseService} from "../BaseService";
import {IUser} from "../../interfaces/IUser";
import {identity, pickBy, toLower} from "lodash";
import * as UUIDV4 from "uuid/v4";
import * as moment from "moment";
import AppConstants from "../../utils/constants";
import {SeedUsers} from "../../models/seed-users/SeedUsers";

@Service()
export class UserService extends BaseService<User> {

    constructor(@Inject(User) private userModel: MongooseModel<User> | any) {
        super();
    }

    public async createUser(user: any): Promise<User | any> {
        user["password"] = await User.encrypt(user.password);

        return this.create(user, false);
    }

    public updateUser(user: User, updatedVal = {}, removeEmptyValues = true): Promise<User | any> {
        return this.update(user, updatedVal, removeEmptyValues);
    }

    public findUser(id: string): Promise<User | any> {
        return this.findById(id, "User", false);
    }

    public totalUsersInRole(id: string): Promise<User | any> {
        return this.userModel.count({role: id, deletedAt: null});
    }

    public async findAndUpdate(id: string, updatedValues: object | any): Promise<IUser | any> {
        const user = await this.findById(id, "User", false);
        if (updatedValues.password) {
            updatedValues["password"] = await SeedUsers.encrypt(updatedValues.password);
        }

        return this.update(user, updatedValues, true);
    }

    public findAllUsers(): Promise<User | any> {
        return this.findAll({}, "User", "role");
    }

    public async setForgotPasswordLink(email: string): Promise<IUser | any> {
        const resetPasswordToken = UUIDV4();
        const resetPasswordExpiry = moment().add(1, "days").unix();
        let user = await this.findOne({email}, "User", true, false);
        user = await this.update(user, {updatedAt: Date.now(), resetPasswordToken, resetPasswordExpiry});

        return user;
    }

    public async verifyForgotPasswordToken(token: string): Promise<IUser | any> {
        const user: User | any = await this.findOne({resetPasswordToken: token}, "User", false, false);

        if (!user) {
            throw new Error(AppConstants.ERROR_MESSAGES.USER_NOT_EXIST_RESET_LINK);
        }

        const {resetPasswordExpiry} = user;
        if (resetPasswordExpiry < moment().unix()) {
            throw ({message: AppConstants.ERROR_MESSAGES.RESET_TOKEN_EXPIRED, data: {expired: true}});
        }

        return user;
    }

    public async delete(id) {
        const user = await this.userModel.findByIdAndRemove(id);
        return user;
    }
    getModel() {
        return this.userModel;
    }
}
