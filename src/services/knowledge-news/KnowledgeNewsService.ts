import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {CommonProvince} from "../../models/common-province/CommonProvince";
import {KnowledgeNews} from "../../models/knowledge-news/KnowledgeNews";

@Service()
export class KnowledgeNewsService extends BaseService<CommonProvince> {

    constructor(@Inject(KnowledgeNews) private eventNews: MongooseModel<KnowledgeNews> | any) {
        super();
    }

    public async createEventNews(eventNewsInfo: any): Promise<any> {
        return this.create(eventNewsInfo, false);
    }

    public async updateEventNews(updateEventNewsInfo: any, id): Promise<any> {
        const eventNews = await this.findById(id, "EventNews", false);

        return this.update(eventNews, updateEventNewsInfo, false);
    }

    public async list(where = {}, paginated = {}): Promise<any> {
        const eventNews = await this.findAll(where, "EventNews", "", {...paginated, order: "createdAt"});

        return eventNews;
    }

    public async getLatestFlagNews(where = {}): Promise<any> {
        const eventNews: any = await this.findAll({...where, flag: true}, "EventNews", "", {
            pageSize: 1,
            pageNo: 0,
            order: "createdAt"
        });

        return eventNews.length ? eventNews[0] : null;
    }

    public async getSpecificEventNews(id: string): Promise<any> {
        const eventNews = await this.findById(id, "EventNews", false);

        return eventNews;
    }

    getModel() {
        return this.eventNews;
    }
}
