import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pickBy, toLower} from "lodash";
import {PlantRegister} from "../../models/plant-register/PlantRegister";
import {Utility} from "../../utils/constants";
import {WareHouse} from "../../models/warehouse/WareHouse";
import {VolumeService} from "../volume/VolumeService";
import {PlantCategory} from "../../models/plant-category/PlantCategory";

@Service()
export class PlantRegisterService extends BaseService<PlantRegister> {

    constructor(@Inject(PlantRegister) private plantModal: MongooseModel<PlantRegister> | any,
                private volumeService: VolumeService,
                @Inject(PlantCategory) private plantCategoryModal: MongooseModel<PlantCategory> | any,) {
        super();
    }

    public async createPlant(plantInfo: any, generateNumbers = true): Promise<any> {
        let whereClause = {parentPlant: null};
        let whereClause1 = {volume: null};
        const {is_generate_d_gene} = plantInfo;
        // @ts-ignore
        whereClause = is_generate_d_gene ? {...whereClause, reg_gene_id: {$regex: "D"}} : whereClause;
        whereClause1 = {is_generate_d_gene} ? {volume: plantInfo.volume} : whereClause1;
        const count = await this.plantModal.count(whereClause);
        const count1 = await this.plantModal.count(whereClause1);
        plantInfo.reg_seed_no = count1 + 1;
        if (generateNumbers) {
            plantInfo.reg_gene_id = `${is_generate_d_gene ? "D" : "R"}${Utility.getFormattedNumber(count + 1)}`;
        }

        return this.create(plantInfo, false);
    }

    public async updatePlant(updatePlantInfo: any, id, isRemoveEmptyVal = false): Promise<any> {
        const plant = await this.findById(id, "Plant", false);

        if(plant.m8_lab_id !=null ) {
            if(updatePlantInfo.m8_lab_status=='complete')
            {
                // updatePlantInfo.m8lab_plant_submittedAt = Date.now();
                // updatePlantInfo.m8lab_plant_submittedBy = updatePlantInfo.createdBy;
                // updatePlantInfo.m9warehouse_plant_createdAt = Date.now();
                // updatePlantInfo.m9warehouse_plant_createdBy = updatePlantInfo.createdBy;
                // updatePlantInfo.m9warehouse_plant_updatedAt = Date.now();
                // updatePlantInfo.m9warehouse_plant_updatedBy = updatePlantInfo.createdBy;
            }
            else{
                updatePlantInfo.m8lab_plant_updatedAt = updatePlantInfo.updatedAt;
                updatePlantInfo.m8lab_plant_updatedBy = updatePlantInfo.updatedBy;            
            }
        }
        else{
            if(updatePlantInfo.m3_status=='complete')
            {
                // updatePlantInfo.m3register_plant_submittedAt = Date.now();
                // updatePlantInfo.m3register_plant_submittedBy = plant.createdBy;
                // updatePlantInfo.m8lab_plant_createdAt = Date.now();
                // updatePlantInfo.m8lab_plant_createdBy = plant.createdBy;
                // updatePlantInfo.m8lab_plant_updatedAt = Date.now();
                // updatePlantInfo.m8lab_plant_updatedBy = plant.createdBy;
            }
            else {
                updatePlantInfo.m3register_plant_updatedAt = updatePlantInfo.updatedAt;
                updatePlantInfo.m3register_plant_updatedBy = updatePlantInfo.updatedBy;
            }
        }
        return this.update(plant, updatePlantInfo, isRemoveEmptyVal);
    }

    public async getDepartmentTypes(): Promise<any> {
        const departmentTypes = await this.plantModal
          .find()
          .distinct("department_type");
        return departmentTypes;
      }
    public async filter(search): Promise<any> {
        let where = {};
        let {startDt, endDt, department_type, source_province, source_country, form_type, plant_type} = search;
        where = {reg_plant_sci_name: {$ne: null}};
        if(department_type != "") {
          where['department_type'] = department_type;
        }
        if(source_province != "") {
          where['source_province'] = source_province;
        }
        if(source_country != "") {
          where['source_country'] = source_country;
        }
        const filterResults = [];
        const plantRegisters: any = await this.plantModal.find(where).populate('volume').populate('reg_plant_sci_name');
        let plantCategories : any = await this.plantCategoryModal.find({});
        
        if(form_type == "" && plant_type == "") return {categories: plantCategories, filterResult: plantRegisters};
        for await (const plant of plantRegisters) {
          const {volume, reg_plant_sci_name} = plant;
          if(reg_plant_sci_name == null) continue;
          if(form_type != "" && plant_type == ""){
            if(volume['form_type'] == form_type) filterResults.push(plant);
          }
          if(plant_type != "" && form_type == "") {
            if(reg_plant_sci_name['plant_category'] == plant_type) filterResults.push(plant);
          }
          if(plant_type != "" && form_type != "") { 
            if(reg_plant_sci_name['plant_category'] == plant_type && volume['form_type'] == form_type) filterResults.push(plant);
          }
        }
        
        return {categories: plantCategories, filterResult: filterResults};
    
        
    } 

    public async filterLabStatus(search): Promise<any> {
        let where = {};
        let {startDt, endDt} = search;
        where = {reg_plant_sci_name: {$ne: null}};
        const plantRegisters: any = await this.plantModal.find(where).populate('m8_lab_id').populate('reg_plant_sci_name');

        let plantCategories : any = await this.plantCategoryModal.find({});

        return {categories: plantCategories, filterResult: plantRegisters};

    }
    public async updatePlantByParentId(updatePlantInfo: any, id, isRemoveEmptyVal = false): Promise<any> {
        const plants: any = await this.findAll({parentPlant: id}, "Plant", "", {}, false);

        if (plants.length) {
            return Promise.all(plants.map(plant => {
                return this.update(plant, updatePlantInfo, isRemoveEmptyVal);
            }));
        }
    }

    public async list(volume, query = null, status = null, labId = null): Promise<any> {
        const where: any = {deletedAt: null};
        if (volume !== "all") {
            where.volume = volume;
        }
        if (query === "plant_listing" && status) {
            where.m3_status = status;
            where.parentPlant = null;
        } else if (query === "lab_plants") {
            where.m8_lab_id = labId || {$ne: null};
            if (status) {
                where.m8_lab_status = status;
            }
        } else if (query === "complete_plant_m3") {
            where.m3_status = "complete";
            where.m8_lab_id = null;
            where.parentPlant = null;
        }

        let modelObj = await this.plantModal.find(where)
            .populate("passport")
            .populate("volume")
            .populate("reg_plant_sci_name")
            .populate("plant_gene_name")
            .populate("reg_gene_type")
            .populate("reg_gene_category")
            .populate("source_country")
            .populate("source_country_1")
            .populate("source_province")
            .populate("source_district")
            .populate("updatedBy")
            .populate("m3register_plant_createdBy")
            .populate("m3register_plant_updatedBy")
            .populate("m3register_plant_submittedBy")
            .populate("m8lab_plant_createdBy")
            .populate("m8lab_plant_updatedBy")
            .populate("m8lab_plant_submittedBy")
            .populate("m9warehouse_plant_createdBy")
            .populate("m9warehouse_plant_updatedBy")
            .populate("m9warehouse_plant_submittedBy")
            .populate("createdBy")
            .sort({"updatedAt": "desc"});

        modelObj = modelObj.map(obj => obj.display());


        return modelObj;
    }

    public async listAutoComplete(): Promise<any> {
        let modelObj = await this.plantModal.find({reg_seed_owner: {$ne: null}})
            .populate("passport")
            .populate("volume")
            .populate("createdBy")
            .sort({"updatedAt": "desc"});

        modelObj = modelObj.map(obj => obj.display());


        return modelObj;
    }

    public async listWarehouseRecoverPlants(volume, query = null, status = null, labId = null): Promise<any> {
        const where: any = {deletedAt: null};
        if (volume !== "all") {
            where.volume = volume;
        }
        if (query === "plant_listing" && status) {
            where.m3_status = status;
        } else if (query === "lab_plants") {
            where.m8_lab_id = labId || {$ne: null};
            if (status) {
                where.m8_lab_status = status;
            }
        } else if (query === "complete_plant_m3") {
            where.m3_status = "complete";
            where.m8_lab_id = null;
        }

        let modelObj = await this.plantModal.find({m9warehouse_plant_refer_id: {$ne: null}, deletedAt: null, ...where})
        // let modelObj = await this.plantModal.find({warehouse_id: {$ne: null}, deletedAt: null, ...where})
            .populate({
                path: "m9warehouse_plant_refer_id",
                // path: "warehouse_id",
                populate: [
                    {path: "passport"},
                    {path: "reg_plant_sci_name"},
                    {path: "plant_gene_name"},
                    {path: "reg_gene_type"},
                    {path: "source_country"},
                    {path: "source_country_1"},
                    {path: "source_province"},
                    {path: "source_district"},
                    {path: "createdBy"},
                    {path: "updatedBy"}
                ]
            })
            .populate("passport")
            .populate("volume")
            .populate("reg_plant_sci_name")
            .populate("plant_gene_name")
            .populate("reg_gene_type")
            .populate("reg_gene_category")
            .populate("source_country")
            .populate("source_country_1")
            .populate("source_province")
            .populate("source_district")
            .populate("updatedBy")
            .populate("createdBy")
            .sort({"updatedAt": "desc"});

        modelObj = modelObj.map(obj => obj.display());

        modelObj = modelObj.filter(({m9warehouse_plant_refer_id}) => m9warehouse_plant_refer_id);
        // modelObj = modelObj.filter(({warehouse_id}) => warehouse_id);
        modelObj = modelObj.map(({_id, m9warehouse_plant_refer_id, ...other}) => {
        // modelObj = modelObj.map(({_id, warehouse_id, ...other}) => {
            return {...m9warehouse_plant_refer_id, ...other, _id};
            // return {...warehouse_id, ...other, _id};
        });

        return modelObj;
    }

    public async getSpecificPlant(id: string): Promise<any> {
        const plant = await this.findById(id, "Plant", false);

        return plant;
    }

    public async getSpecificRecoverPlant(id: string): Promise<any> {
        let plant = await this.plantModal.findById(id)
            .populate({
                path: "m9warehouse_plant_refer_id",
                // path: "warehouse_id",
                populate: [{
                    path: "register_alert_form",
                    populate: [{
                        path: "warehouse"
                    }]
                }]
            });

        if (!plant) {
            throw new Error("Plant not found");
        }
        plant = plant.display();
        const {m9warehouse_plant_refer_id, ...other} = plant;
        // const {warehouse_id, ...other} = plant;
        const {register_alert_form = {}} = m9warehouse_plant_refer_id || [];
        // const {register_alert_form = {}} = warehouse_id || [];
        const {warehouse = {}} = register_alert_form || {};
        if (register_alert_form) {
            delete register_alert_form.warehouse;
        }
        plant = {...warehouse || {}, ...register_alert_form, ...m9warehouse_plant_refer_id, ...other};
        // plant = {...warehouse || {}, ...register_alert_form, ...warehouse_id, ...other};

        return plant;
    }

    public async submitIntoM8(plantIds = [], _id = null): Promise<any> {
        return Promise.all(plantIds.map(async plantId => {
            const plant = await this.findById(plantId, "Plant", false);
            const by_user = _id;
            const at_when = Date.now();
            
            if(plant.selfing_input){
                await this.generatePlant(plant , by_user , at_when);
                return this.updatePlant({
                    m3_status: "complete",
                    m3register_plant_submittedAt : at_when,
                    m3register_plant_submittedBy : by_user,
                    m8lab_plant_createdAt : at_when,
                    m8lab_plant_createdBy : by_user,
                    m8lab_plant_updatedAt : at_when,
                    m8lab_plant_updatedBy : by_user,
                    oop_input :undefined,
                    oop_dropdown : undefined,
                    other_input : undefined,
                    other_dropdown : undefined,
                }, plantId);
            }
            else if(!plant.selfing_input && plant.oop_input){
                await this.generatePlant(plant , by_user , at_when);
                return this.updatePlant({
                    m3_status: "complete",
                    m3register_plant_submittedAt : at_when,
                    m3register_plant_submittedBy : by_user,
                    m8lab_plant_createdAt : at_when,
                    m8lab_plant_createdBy : by_user,
                    m8lab_plant_updatedAt : at_when,
                    m8lab_plant_updatedBy : by_user,
                    other_input : undefined,
                    other_dropdown : undefined,
                }, plantId);
            }
            else if(!plant.selfing_input && !plant.oop_input && plant.other_input){
                return this.updatePlant({
                    m3_status: "complete",
                    m3register_plant_submittedAt : at_when,
                    m3register_plant_submittedBy : by_user,
                    m8lab_plant_createdAt : at_when,
                    m8lab_plant_createdBy : by_user,
                    m8lab_plant_updatedAt : at_when,
                    m8lab_plant_updatedBy : by_user,
                }, plantId);
            }
        }));
    }

    public async generatePlant(dbPlant , by_user =null, at_when = null) {
        const plant = dbPlant.display();
        const {
            _id,
            selfing_input, selfing_dropdown,
            oop_input, oop_dropdown,
            other_input, other_dropdown,
            isSplit = false,
            volume
        } = plant;
        delete plant._id;
        delete plant.selfing_input;
        delete plant.selfing_dropdown;
        delete plant.oop_input;
        delete plant.oop_dropdown;
        delete plant.other_input;
        delete plant.other_dropdown;

        if (isSplit) {
            return null;
        }

        // const volumeInfo = await this.volumeService.getSpecificVolume(volume);
        // if (volumeInfo && volumeInfo.form_type === "Register 4") {
        //     return null;
        // }

        const stock = {
            isSelfing: selfing_input && selfing_dropdown,
            isOOP: oop_input && oop_dropdown,
            isOther: other_input && other_dropdown,
        };

        const promiseQuery = [];
        if (stock.isSelfing && stock.isOOP) {
        // if (stock.isSelfing && stock.isOOP) {
            promiseQuery.push(this.create({
                ...plant,
                m3_status: "complete",
                // parentPlant: _id,
                oop_input,
                oop_dropdown,
                m3register_plant_submittedAt : at_when,
                m3register_plant_submittedBy : by_user,
                m8lab_plant_createdAt : at_when,
                m8lab_plant_createdBy : by_user,
                m8lab_plant_updatedAt : at_when,
                m8lab_plant_updatedBy : by_user,
            }, false));
        }
        if ( (stock.isOOP && stock.isOther) || (stock.isSelfing && stock.isOther) ) {
        // if (stock.isSelfing && stock.isOther) {
            promiseQuery.push(this.create({
                ...plant, m3_status: "complete", 
                // parentPlant: _id,
                other_input, other_dropdown,
                m3register_plant_submittedAt : at_when,
                m3register_plant_submittedBy : by_user,
                m8lab_plant_createdAt : at_when,
                m8lab_plant_createdBy : by_user,
                m8lab_plant_updatedAt : at_when,
                m8lab_plant_updatedBy : by_user,
            }, false));
        }

        // if (!stock.isSelfing && stock.isOOP && stock.isOther) {
        //     promiseQuery.push(this.create({
        //         ...plant, m3_status: "complete", 
        //         // parentPlant: _id,
        //         other_input, other_dropdown,
        //         m3register_plant_submittedAt : at_when,
        //         m3register_plant_submittedBy : by_user,
        //         m8lab_plant_createdAt : at_when,
        //         m8lab_plant_createdBy : by_user,
        //         m8lab_plant_updatedAt : at_when,
        //         m8lab_plant_updatedBy : by_user,
        //     }, false));
        // }

        dbPlant.isSplit = true;

        return Promise.all([...promiseQuery, dbPlant.save()]);
    }

    getModel() {
        return this.plantModal;
    }
}
