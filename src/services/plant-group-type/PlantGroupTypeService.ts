import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pickBy, toLower} from "lodash";
import {PlantGroupType} from "../../models/plant-group-type/PlantGroupType";
import {UserService} from "../users/UsersService";

@Service()
export class PlantGroupTypeService extends BaseService<PlantGroupType> {

    constructor(@Inject(PlantGroupType) private categoryModal: MongooseModel<PlantGroupType> | any, private userService: UserService) {
        super();
    }

    public async createGroup(categoryInfo: any): Promise<any> {
        return this.create(categoryInfo, false);
    }

    public async updateGroup(updateGroupInfo: any, id): Promise<any> {
        const category = await this.findById(id, "Group", false);

        return this.update(category, updateGroupInfo, true);
    }

    public async list(where = {}): Promise<any> {
        const categorys = await this.findAll(where, "Groups");

        return categorys;
    }

    public async getSpecificGroup(id: string): Promise<any> {
        const category = await this.findById(id, "Group", false);

        return category;
    }

    getModel() {
        return this.categoryModal;
    }
}
