import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pickBy, toLower} from "lodash";
import {CommonCountry} from "../../models/common-country/CommonCountry";

@Service()
export class CommonCountryService extends BaseService<CommonCountry> {

    constructor(@Inject(CommonCountry) private countryModal: MongooseModel<CommonCountry> | any) {
        super();
    }

    public async createCountry(countryInfo: any): Promise<any> {
        return this.create(countryInfo, false);
    }

    public async updateCountry(updateCountryInfo: any, id): Promise<any> {
        const country = await this.findById(id, "Country", false);

        return this.update(country, updateCountryInfo, false);
    }

    public async list(where = {}): Promise<any> {
        const countries = await this.findAll(where, "Countries");

        return countries;
    }

    public async getSpecificCountry(id: string): Promise<any> {
        const countries = await this.findById(id, "Country", false);

        return countries;
    }

    public async search(keyword: string): Promise<any> {
        const countries = await this.countryModal.find({$or: [{countryName_en: {$regex: '.*' + keyword + '.*', $options: 'i'}}, {countryName_th: {$regex: '.*' + keyword + '.*', $options: 'i'}}]}).limit(20);

        return countries;
    }

    getModel() {
        return this.countryModal;
    }
}
