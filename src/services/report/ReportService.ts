import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pick, pickBy, toLower} from "lodash";
import {Recovery} from "../../models/recovery/Recovery";
import {WareHouse} from "../../models/warehouse/WareHouse";
import {PlantRegister} from "../../models/plant-register/PlantRegister";
import {Lab} from "../../models/lab/Lab";

@Service()
export class ReportService extends BaseService<any> {

    constructor(@Inject(Recovery) private recoverModal: MongooseModel<Recovery> | any,
                @Inject(PlantRegister) private plantModal: MongooseModel<PlantRegister> | any,
                @Inject(Lab) private labModal: MongooseModel<Lab> | any,
                @Inject(WareHouse) private wareHouseModal: MongooseModel<WareHouse> | any) {
        super();
    }

    async getMol3RegisterReport(startAt = Date.now(), endAt = Date.now()) {
        const plants = await this.plantModal.find({
            parentPlant: null,
            createdAt: {$gte: startAt, $lte: endAt}
        }).populate("volume")
            .populate({
                path: "m9warehouse_plant_refer_id",
                // path: "warehouse_id",
                populate: [{
                    path: "reg_plant_sci_name",
                    populate: [{path: "plant_category"}]
                }]
            })
            .populate({
                path: "reg_plant_sci_name",
                populate: [{path: "plant_category"}]
            });

        const mol3Counts = {};


        for (const plant of plants) {
            const {volume = {}, reg_plant_sci_name, m9warehouse_plant_refer_id = {}} = plant;
            // const {volume = {}, reg_plant_sci_name, warehouse_id = {}} = plant;
            // @ts-ignore
            const {plant_category} = reg_plant_sci_name || m9warehouse_plant_refer_id.reg_plant_sci_name || {};
            // const {plant_category} = reg_plant_sci_name || warehouse_id.reg_plant_sci_name || {};
            const {categoryName_en}: any = plant_category || {};
            const {form_type = ""} = volume || {};
            if (categoryName_en) {
                mol3Counts[categoryName_en] = mol3Counts[categoryName_en] || {};

                mol3Counts[categoryName_en][form_type] = (mol3Counts[categoryName_en][form_type] || 0) + 1;
            }
        }


        const keys = Object.keys(mol3Counts);

        for (const key of keys) {
            Object.keys(mol3Counts[key] || {}).forEach(innerKey => {
                mol3Counts[key]["total"] = (mol3Counts[key]["total"] || 0) + (mol3Counts[key][innerKey] || 0);
            });
        }

        return mol3Counts;
    }

    async getMol41RecoveryReport(startAt = Date.now(), endAt = Date.now()) {
    }

    async getMol8LStatusReport() {
        const where = {is_alert: null};
        const labs: any = await this.labModal.find(where);

        return Promise.all(labs.map(async lab => {
            lab = lab.display();
            const {_id} = lab;
            const [pending = 0, complete = 0] = await Promise.all([this.plantModal.count({
                m8_lab_id: _id,
                m8_lab_status: "draft"
            }), this.plantModal.count({m8_lab_id: _id, m8_lab_status: "complete"})]);


            return {...lab, pending, complete, total: pending + complete};
        }));
    }

    getModel() {
        return null;
    }
}
