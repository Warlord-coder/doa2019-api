import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pickBy, toLower} from "lodash";
import {PlantCategory} from "../../models/plant-category/PlantCategory";
import {UserService} from "../users/UsersService";

@Service()
export class PlantCategoryService extends BaseService<PlantCategory> {

    constructor(@Inject(PlantCategory) private categoryModal: MongooseModel<PlantCategory> | any, private userService: UserService) {
        super();
    }

    public async createCategory(categoryInfo: any): Promise<any> {
        return this.create(categoryInfo, false);
    }

    public async updateCategory(updateCategoryInfo: any, id): Promise<any> {
        const category = await this.findById(id, "Category", false);

        return this.update(category, updateCategoryInfo, true);
    }

    public async list(where = {}): Promise<any> {
        const categorys = await this.findAll(where, "Categorys");

        return categorys;
    }

    public async getSpecificCategory(id: string): Promise<any> {
        const category = await this.findById(id, "Category", false);

        return category;
    }

    getModel() {
        return this.categoryModal;
    }

    public async search(keyword: string): Promise<any> {
        const categorys = await this.categoryModal.find({$or: [{categoryName_en: {$regex: '.*' + keyword + '.*', $options: 'i'}}, {categoryName_th: {$regex: '.*' + keyword + '.*', $options: 'i'}}]}).limit(20);

        return categorys;
    }
}
