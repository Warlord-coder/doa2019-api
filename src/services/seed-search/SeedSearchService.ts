import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pick, pickBy, toLower} from "lodash";
import {Recovery} from "../../models/recovery/Recovery";
import {WareHouse} from "../../models/warehouse/WareHouse";
import {PlantRegister} from "../../models/plant-register/PlantRegister";
import {Lab} from "../../models/lab/Lab";
import {AttributeList} from "../../models/attribute-list/AttributeList";
import {PlantGroupService} from "../plant-group/PlantGroupService";
import {PlantGroupTypeService} from "../plant-group-type/PlantGroupTypeService";
import {PlantCharacterService} from "../plant-character/PlantCharacterService";
import {ancestorWhere} from "tslint";
import {CommonCountryService} from "../common-country/CommonCountryService";
import {CommonProvinceService} from "../common-province/CommonProvinceService";
import {PlantTypeService} from "../plant-type/PlantTypeService";
import {AttributeListService} from "../attribute-list/AttributeListService";
import {AttributeSetService} from "../attribute-set/AttributeSetService";
import {AttributeSet} from "../../models/attribute-set/AttributeSet";
@Service()
export class SeedSearchService extends BaseService<any> {

    constructor(@Inject(Recovery) private recoverModal: MongooseModel<Recovery> | any,
                @Inject(PlantRegister) private plantModal: MongooseModel<PlantRegister> | any,
                @Inject(Lab) private labModal: MongooseModel<Lab> | any,
                private plantGroupService: PlantGroupService,
                private plantGroupTypeService: PlantGroupTypeService,
                private PlantCharacterService: PlantCharacterService,
                private countryservice: CommonCountryService,
                private plantTypeService: PlantTypeService,
                private provinceService: CommonProvinceService,
                private attributeListService: AttributeListService,
                private attributeSetService: AttributeSetService,
                @Inject(AttributeSet) private attributeModal: MongooseModel<AttributeSet> | any,
                @Inject(WareHouse) private wareHouseModal: MongooseModel<WareHouse> | any) {
        super();
    }

    async simpleSearch({searchCriteria, searchKeyword}) {
        const type = this.getSearchField(searchCriteria);
        if (!type) {
            return [];
        }
        const whereClause = {
            submitTo: "m2",
        };
        const warehouseList = await this.wareHouseModal.find(whereClause)
            .populate("reg_gene_category")
            .populate("reg_gene_type")
            .populate({
                path: "reg_plant_sci_name",
                populate: [{
                    path: "plant_category"
                }]
            })
            .populate("plant_gene_name")
            .populate("source_province")
            .sort({"updatedAt": "desc"});

        
        
        const {fields = [], key, innerKey} = type;
        return warehouseList.filter(data => {
            if(data[key] == undefined) return false;
            let queryObj = data[key];
            
            if (innerKey && queryObj) {
                queryObj = queryObj[innerKey];
            }

            if (!queryObj) {
                return false;
            }
            
            
            if(key === 'reg_plant_sci_name') {
                if(queryObj[fields[1]] == undefined) queryObj[fields[1]] = '';
                if(queryObj[fields[0]] == undefined) queryObj[fields[0]] = '';
                if(!toLower(queryObj[fields[1]].trim() + ' ' + queryObj[fields[0]].trim()).includes(toLower(searchKeyword.trim()))) {
                    return false;
                }
            } else {
                if (!toLower(queryObj[fields[0]]).includes(toLower(searchKeyword)) && !toLower(queryObj[fields[1]]).includes(toLower(searchKeyword))) {
                    return false;
                }
            }

            return true;
        });
    }

    async getAttributes() {
        return await this.attributeModal.find();
    }
    async advanceSearch(query) {
        const {
            keyword, plantCategory, alterPlant, plantType, plantSpecies,
            plantCommonName, plantGroup, plantGroupType, source_c, source_p, plantCharacter, lab_test_growth_percent,
            wh_sample_ref_molecular = null, isSpecieSelected = false
        } = query;

        const whereClause = {
            submitTo: "m2",
        };

        const warehouseList = await this.wareHouseModal.find(whereClause)
            .populate("reg_gene_category")
            .populate("reg_gene_type")
            .populate({
                path: "reg_plant_sci_name",
                populate: [{
                    path: "plant_category"
                }]
            })
            .populate("source_province")
            .populate("plant_gene_name")
            .populate("source_country")
            .sort({"updatedAt": "desc"});

        return warehouseList.filter(data => {
            if (plantSpecies) {
                const queryObj = data["reg_plant_sci_name"];
                if (!queryObj || !toLower(queryObj.species).includes(toLower(plantSpecies))) {
                    return false;
                }
            }

            const {room5 = [], room10 = [], reg_character_brief = "", reg_plant_alter_name = []} = data;
            if (lab_test_growth_percent) {
                const growthPercent = [];
                for (const roomInfo of room5) {
                    growthPercent.push(toLower(roomInfo.lab_test_growth_percent));
                }
                for (const roomInfo of room10) {
                    growthPercent.push(toLower(roomInfo.lab_test_growth_percent));
                }

                if (!growthPercent.includes(toLower(lab_test_growth_percent))) {
                    return false;
                }
            }

            if (!isSpecieSelected && plantCategory) {
                const {fields = [], key, innerKey} = this.getSearchField("Plant Category");
                let queryObj = data[key];
                if(data[key] == undefined) return false;
                if (innerKey && queryObj) {
                    queryObj = queryObj[innerKey];
                }
                if(!queryObj) return false;
                if (!toLower(queryObj[fields[0]]).includes(toLower(plantCategory)) && !toLower(queryObj[fields[1]]).includes(toLower(plantCategory))) {
                    return false;
                }
            }

            if (!isSpecieSelected && plantType) {
                const {fields = [], key, innerKey} = this.getSearchField("Plant type");
                let queryObj = data[key];
                if (innerKey && queryObj) {
                    queryObj = queryObj[innerKey];
                }
                if (!toLower(queryObj[fields[0]]).includes(toLower(plantType)) && !toLower(queryObj[fields[1]]).includes(toLower(plantType))) {
                    return false;
                }
            }

            if (!isSpecieSelected && plantCommonName) {
                const {fields = [], key} = this.getSearchField("Plant common name");
                const queryObj = data[key];
                if(data[key] == undefined) return false;
                if(queryObj[fields[0]] == undefined) queryObj[fields[0]] = "";
                if(queryObj[fields[1]] == undefined) queryObj[fields[1]] = "";
                if (!toLower(queryObj[fields[0]]).includes(toLower(plantCommonName)) && !toLower(queryObj[fields[1]]).includes(toLower(plantCommonName))) {
                    return false;
                }
            } 

            if (plantGroup) {
                const {fields = [], key} = this.getSearchField("Plant type");
                const queryObj = data[key];
                if(data[key] == undefined) return false;
                if (!toLower(queryObj[fields[0]]).includes(toLower(plantGroup)) && !toLower(queryObj[fields[1]]).includes(toLower(plantGroup))) {
                    return false;
                }
            }

            if (plantGroupType) {
                const {fields = [], key, innerKey} = this.getSearchField("Plant Category");
                let queryObj = data[key];
                if(data[key] == undefined) return false;
                if (innerKey && queryObj) {
                    queryObj = queryObj[innerKey];
                }
                if(!queryObj) return false;
                if (!toLower(queryObj[fields[0]]).includes(toLower(plantGroupType)) && !toLower(queryObj[fields[1]]).includes(toLower(plantGroupType))) {
                    return false;
                }
            }

            if (source_c) {
                const {fields = [], key} = this.getSearchField("Source Country");
                const queryObj = data[key];
                if(data[key] == undefined) return false;
                if (!toLower(queryObj[fields[0]]).includes(toLower(source_c)) && !toLower(queryObj[fields[1]]).includes(toLower(source_c))) {
                    return false;
                }
            }

            if (source_p) {
                const {fields = [], key} = this.getSearchField("Source Province");
                const queryObj = data[key];
                if(data[key] == undefined) return false;
                if (!toLower(queryObj[fields[0]]).includes(toLower(source_p)) && !toLower(queryObj[fields[1]]).includes(toLower(source_p))) {
                    return false;
                }
            }

            if (plantCharacter) {
                if (!toLower(reg_character_brief).includes(toLower(plantCharacter))) {
                    return false;
                }
            }

            if (!isSpecieSelected && alterPlant) {
                const namesInfo = reg_plant_alter_name.map(({display}) => toLower(display));
                if (!namesInfo.includes(toLower(alterPlant))) {
                    return false;
                }
            }

            if (wh_sample_ref_molecular !== "" && wh_sample_ref_molecular !== null && String(wh_sample_ref_molecular) !== String(data.wh_sample_ref_molecular || false)) {
                return false;
            }


            return true;
        });
    }

    getSearchField(field) {
        switch (field) {
            case "Plant Category":
                return {
                    fields: ["categoryName_en", "categoryName_th"],
                    key: "reg_plant_sci_name",
                    innerKey: "plant_category"
                };
            case "Plant type":
                return {fields: ["plantTypeName_en", "plantTypeName_th"], key: "plant_gene_name"};
            case "Plant Species":
                return {fields: ["species", "genus"], key: "reg_plant_sci_name"};
            case "Plant common name":
                return {fields: ["reg_plant_common_name"], key: "reg_plant_sci_name"};
            case "Source Country":
                return {fields: ["countryName_en", "countryName_th"], key: "source_country"};
            case "Source Province":
                return {fields: ["provinceName_en", "provinceName_th"], key: "source_province"};
        }

        return null;
    }

    getModel() {
        return null;
    }

    async getAutocompleteInfo() {
        const [groups = [], groupTypes = [], countries = [], provinces = [], characters = [], plantTypes = [], attributeSetListing=[]] = await Promise.all([
            this.plantGroupService.list({isDeleted: null}),
            this.plantGroupTypeService.list({isDeleted: null}),
            this.countryservice.list({isDeleted: null}),
            this.provinceService.list({isDeleted: null}),
            this.PlantCharacterService.list({isDeleted: null}, "plant_category"),
            this.plantTypeService.list({isDeleted: null}),
            this.attributeListService.list(),
        ]);

        const alterPlantListing = [];
        const plantCommonListing = [];
        const plantCategoryListing = [];

        for (const character of characters) {
            const {reg_plant_alter_name = [], reg_plant_common_name = [], plant_category = {}} = character;
            if (reg_plant_alter_name && reg_plant_alter_name.length) {
                alterPlantListing.push(...reg_plant_alter_name.map(name => ({name})));
            }
            if (reg_plant_common_name) {
                plantCommonListing.push({name: reg_plant_common_name});
            }
            if (plant_category && plant_category.categoryName_en) {
                plantCategoryListing.push({name: plant_category.categoryName_en});
            }
        }

        return {
            groups,
            groupTypes,
            characters,
            countries,
            provinces,
            plantTypes,
            alterPlantListing,
            plantCommonListing,
            plantCategoryListing,   
            attributeSetListing
        };
    }
}
