import {Inject, Service} from "@tsed/common";
import {Strategy} from "passport-local";
import AppConstants from "../../utils/constants";
import * as passport from "passport";
import {Strategy as LocalStrategy} from "passport-local";
import {User} from "../../models/users/User";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {config} from "bluebird";
import JWTService from "../jwt/JWTService";
import {SeedUsers} from "../../models/seed-users/SeedUsers";


@Service()
export default class PassportService extends BaseService<any> {
    constructor(@Inject(User) private userModel: MongooseModel<User> | any,
                @Inject(SeedUsers) private userSeedModel: MongooseModel<SeedUsers> | any) {
        super();
    }

    public localAuthenticate = async (email, password, done) => {
        try {
            const user = await this.findOne({email: email.toLowerCase(), deletedAt: null}, "User", false, false);

            if (!user) {
                return done(null, false, {
                    message: AppConstants.ERROR_MESSAGES.INVALID_CREDENTIAL
                });
            }

            user.authenticate(password, async (authError, authenticated) => {
                if (authError) {
                    return done(authError);
                }
                if (!authenticated) {
                    return done(null, false, {message: AppConstants.ERROR_MESSAGES.INVALID_CREDENTIAL});
                } else {

                    const {_id, role} = user;
                    const authToken = await JWTService.sign(_id, role, {expiresIn: AppConstants.TOKEN_EXPIRY});
                    const tokenExpiry = AppConstants.TOKEN_EXPIRY;
                    const lastLogin = Date.now();
                    user.updatedAt = Date.now();

                    const updatedUser = await this.update(user, {lastLogin, tokenExpiry, authToken}, false);
                    done(null, updatedUser);
                }
            });
        } catch (err) {
            done(err);
        }
    };

    public static setup(PassportInstance: PassportService) {
        passport.use(new LocalStrategy({
            usernameField: "email",
            passwordField: "password"
        }, (email, password, done) => {
            return PassportInstance.localAuthenticate(email, password, done);
        }));
    }

    getModel() {
        return this.userModel;
    }
}
