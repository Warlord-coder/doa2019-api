import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pickBy, toLower} from "lodash";
import {Lab} from "../../models/lab/Lab";
import {VolumeService} from "../volume/VolumeService";
import {Utility} from "../../utils/constants";
import {PlantRegisterService} from "../plant-register/PlantRegisterService";
import {WareHouseService} from "../ware-house/WareHouseService";
import {LabAlertFormService} from "../LabAlert/LabAlertService";
import {PlantRegister} from "../../models/plant-register/PlantRegister";
import {LabAlertForm} from "../../models/LabAlertForm/LabAlertForm";
import * as moment from 'moment';
@Service()
export class LabService extends BaseService<Lab> {

    constructor(@Inject(Lab) private labModal: MongooseModel<Lab> | any,
                private wareHouseService: WareHouseService,
                private plantRegisterService: PlantRegisterService,
                private labAlertFormService: LabAlertFormService,
                @Inject(LabAlertForm) private labalertModal: MongooseModel<LabAlertForm> | any,
                @Inject(PlantRegister) private plantModal: MongooseModel<PlantRegister> | any,
                private volumeService: VolumeService) {
        super();
    }

    public async createLab(labInfo: any): Promise<any> {
        const {volume_no, is_alert} = labInfo;
        if (volume_no) {
            const {_id} = await this.volumeService.findByVolumeNumber(volume_no);
            labInfo.volume_no = _id;
        }

        // const labWhere = {is_alert: null};	
        // if (is_alert) {	
        //     labWhere.is_alert = {$ne: null};	
        // }	
        const totalRecords = await this.labModal.count();

        return this.create({
            ...labInfo,
            isNewLab :true,
            isPlantCancel : false,
            lab_no: "L" + String(Utility.getFormattedNumber(totalRecords + 1)) + "/"+ moment().format("YYYY")
            // lab_no: "L" + String(Utility.getFormattedNumber(totalRecords + 1)) + "/2019"
        }, false);
    }

    public async submitIntoLab(lab, plantIds = []): Promise<any> {
        return Promise.all(plantIds.map(plantId => {
            return Promise.all([this.plantRegisterService.updatePlant({
                m8_lab_id: lab,
                m8_lab_status: "draft"
            }, plantId), this.plantRegisterService.updatePlantByParentId({
                m8_lab_id: lab,
                m8_lab_status: "draft"
            }, plantId)]);
        }));
    }

    public async submitIntoPlantAlert(lab, plantIds = []): Promise<any> {
        return Promise.all(plantIds.map(async plantId => {
            const warehouseInfo = await this.wareHouseService.updatePlant({labAlert: true, labAlertId: lab}, plantId);

            return this.labAlertFormService.createLabAlertForm({
                warehouse: plantId,
                alertFormStatus: "draft",
                labId: lab
            });
        }));
    }

    public async submitIntoM9(plantIds = [], _id = null): Promise<any> {
        return Promise.all(plantIds.map(async plantId => {
            const updatedInfo = await this.plantRegisterService.updatePlant({
                m8_lab_status: "complete",
                m8lab_plant_submittedAt : Date.now(),
                m8lab_plant_submittedBy : _id,
                m9warehouse_plant_createdAt : Date.now(),
                m9warehouse_plant_createdBy : _id,
                m9warehouse_plant_updatedAt : Date.now(),
                m9warehouse_plant_updatedBy : _id,
            }, plantId);

            return this.wareHouseService.createWH(updatedInfo.display());
        }));
    }

    public async updateLab(updateLabInfo: any, id): Promise<any> {
        const lab = await this.findById(id, "Lab", false);
        if(updateLabInfo.isNewLab == false)
            lab.isNewLab = false;
            lab.isPlantCancel = true
        return this.update(lab, updateLabInfo, true);
    }

    public async list(isAlert = null): Promise<any> {
        const where = {is_alert: null};
        if (isAlert) {
            where.is_alert = true;
        }
        const labs: any = await this.findAll(where, "Lab", "volume_no");

        return Promise.all(labs.map(async lab => {
            const {_id} = lab;
            const totalVols = await this.volumeService.countVolume(_id);
            const [isDraft, totalRecords = 0] = await Promise.all([this.plantModal.count({m8_lab_id: _id, $or:[{m8_lab_status: "draft"},{m8_lab_status: "pending"},{m8_lab_status: "approve"}]}), this.plantModal.count({m8_lab_id: _id})]);

            return {...lab, totalVols, isDraft, totalRecords};
        }));
    }
    public async listM8(isAlert = null): Promise<any> {
        const where = {is_alert: null};
        if (isAlert) {
            where.is_alert = true;
        }
        const labs: any = await this.findAll(where, "Lab", "volume_no");

        return Promise.all(labs.map(async lab => {
            const {_id} = lab;
            const totalVols = await this.volumeService.countVolume(_id);
            const [isDraft, totalRecords = 0] = await Promise.all([this.labalertModal.count({labId: _id, $or:[{alertFormStatus: "draft"},{alertFormStatus: "pending"},{alertFormStatus: "approve"}]}), this.labalertModal.count({m8_lab_id: _id})]);
            return {...lab, totalVols, isDraft, totalRecords};
        }));
    }


    public async listUnassignAlertPlants(): Promise<any> {
        const whPlants: any = await this.wareHouseService.list("m2", {labAlert: null, labAlertId: null});

        return whPlants;
    }

    public async getSpecificLab(id: string): Promise<any> {
        const [lab, totalPlants = 0] = await Promise.all([this.findById(id, "Lab", true), this.plantModal.count({m8_lab_id: id})]);

        return {...lab, totalPlants};
    }

    getModel() {
        return this.labModal;
    }

    public async submitIntoPlantAlertM9(lab, plantIds = []): Promise<any> {	
        return Promise.all(plantIds.map(async plantId => {	
            const warehouseInfo = await this.wareHouseService.updatePlant({labAlert: true, labAlertId: lab}, plantId);	
            return this.labAlertFormService.createLabAlertForm({	
                warehouse: plantId,	
                alertFormStatus: "draft",	
                labId: lab	
            });	
        }));	
    }

}
