import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pickBy, toLower} from "lodash";
import {AttributeList} from "../../models/attribute-list/AttributeList";

@Service()
export class AttributeListService extends BaseService<AttributeList> {

    constructor(@Inject(AttributeList) private attributeModal: MongooseModel<AttributeList> | any) {
        super();
    }

    public async createAttributeList(attributeInfo: any): Promise<any> {
        return this.create(attributeInfo, false);
    }

    public async updateAttributeList(updateAttributeListInfo: any, id): Promise<any> {
        const attribute = await this.findById(id, "Attribute List", false);

        return this.update(attribute, updateAttributeListInfo, false);
    }

    public async list(): Promise<any> {
        const attributeList = await this.findAll({}, "Attribute List", "attributes");

        return attributeList;
    }

    public async getSpecificAttributeList(id: string): Promise<any> {
        const attributeList = await this.findById(id, "Attribute List", false);

        return attributeList;
    }

    public async getFilterList(filter): Promise<any> {
        let orWhere = [];
        let {start, length, keyword} = filter;
        if(keyword != '') {
            orWhere = [];
            orWhere.push({setName: {$regex: '.*' + keyword + '.*', $options: 'i'}});
        } else {
            orWhere = [{}];
        }
        let modelObj = await this.attributeModal.find({$or: orWhere}).populate('createdBy').skip(parseInt(start)).limit(parseInt(length))
            
        modelObj = modelObj.map(obj => obj.display());

        let totalRows = await this.attributeModal.find({$or: orWhere}).count();
        
        return {data: modelObj, total_rows: totalRows};
    }
    
    getModel() {
        return this.attributeModal;
    }
}
