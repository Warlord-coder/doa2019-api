import {Inject, Service} from "@tsed/common";
import {MongooseModel} from "@tsed/mongoose";
import {BaseService} from "../BaseService";
import {identity, pickBy, toLower} from "lodash";
import {CommonProvince} from "../../models/common-province/CommonProvince";

@Service()
export class CommonProvinceService extends BaseService<CommonProvince> {

    constructor(@Inject(CommonProvince) private provinceModal: MongooseModel<CommonProvince> | any) {
        super();
    }

    public async createProvince(provinceInfo: any): Promise<any> {
        return this.create(provinceInfo, false);
    }

    public async updateProvince(updateProvinceInfo: any, id): Promise<any> {
        const province = await this.findById(id, "Province", false);

        return this.update(province, updateProvinceInfo, false);
    }

    public async list(where = {}): Promise<any> {
        const provinces = await this.findAll(where, "Provinces");

        return provinces;
    }

    public async getSpecificProvince(id: string): Promise<any> {
        const province = await this.findById(id, "Province", false);

        return province;
    }

    public async search(keyword: string): Promise<any> {
        const provinces = await this.provinceModal.find({$or: [{provinceName_en: {$regex: '.*' + keyword + '.*', $options: 'i'}}, {provinceName_th: {$regex: '.*' + keyword + '.*', $options: 'i'}}]}).limit(20);

        return provinces;
    }
    getModel() {
        return this.provinceModal;
    }
}
