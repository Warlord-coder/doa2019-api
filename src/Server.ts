import {GlobalAcceptMimesMiddleware, ServerLoader, ServerSettings} from "@tsed/common";
import "@tsed/swagger";
import {$log} from "ts-log-debug";
import * as Express from "express";
import * as mongoose from "mongoose";
import * as json2xls from "json2xls";

import "@tsed/multipartfiles";
import "./middlewares/handler/ResponseHandler";
import "./middlewares/handler/ErrorHandler";
import "./middlewares/auth/AuthMiddleware";
import "./middlewares/upload/UploadMiddleware";
import Config from "./config";

const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const compress = require("compression");
const methodOverride = require("method-override");
const cors = require("cors");
const rootDir = __dirname;

@ServerSettings({
    rootDir,
    acceptMimes: ["application/json"],
    logger: {
        debug: false,
        logRequest: false,
        requestFields: ["reqId", "method", "url", "headers", "query", "params", "duration"]
    },
    mongoose: {
        url: Config.MONGO_URI
    },
    swagger: {
        path: "/api-docs"
    },
    debug: false
})

export class Server extends ServerLoader {
    /**
     * This method let you configure the middleware required by your application to works.
     * @returns {Server}
     */
    $onMountingMiddlewares(): void | Promise<any> {
        this
            .use(GlobalAcceptMimesMiddleware)
            .use(cookieParser())
            .use(compress({}))
            .use(methodOverride())
            .use(cors({
                origin: "*",
                optionsSuccessStatus: 200
            }))
            .use(bodyParser.json())
            .use(Express.static("uploads"))
            .use(json2xls.middleware)
            .use(bodyParser.urlencoded({
                extended: true
            }));

        return null;
    }

    $onReady() {
        mongoose.set("debug", process.env.NODE_ENV !== Config.ENVIRONMENTS.PROD);
        $log.debug("Server initialized");
    }

    $onServerInitError(error): any {
        $log.error("Server encounter an error =>", error);
    }
}
