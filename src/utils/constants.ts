import {identity, omit, pickBy} from "lodash";
import *as queryString from "query-string";
import * as moment from "moment";
import * as numeral from "numeral";
import * as puppeteer from "puppeteer";
import Config from "../config";
import * as randomatic from "randomatic";
import {findNumbers} from "libphonenumber-js";
import {$log} from "ts-log-debug";

const momentTimeZone = require("moment-timezone");

export default class AppConstants {
    static ERROR_MESSAGES = {
        EMAIL_EXIST: "Provided Email already Exist",
        OWNER_NOT_EXIST: "Owner not exist",
        MISSING_AUTHORIZATION_HEADER:
            "Missing Authorization Header",
        ROLE: "You don't have permission to access",
        ADMIN_ITSELF_ERROR:
            "Admin cannot delete itself",
        USER_NOT_EXIST: "User not exist in system",
        USER_NOT_EXIST_RESET_LINK:
            "Unable to find user with this link. Contact support",
        VERIFICATION_LINK_NOT_EXIST:
            "Verification Link not exist",
        INVITE_TOKEN_EXPIRED:
            "Your invite is expired. Contact to admin",
        RESET_TOKEN_EXPIRED:
            "Forgot Password token is expire. Please click forgot your password again.",
        RESET_PASSWORD_IS_SAME:
            "Old and New passwords cannot be same",
        RESET_PASSWORD_FAILED:
            "Your have provided the wrong Current Password",
        INVALID_CREDENTIAL:
            "Invalid Email or Password",
        INVITE_PENDING:
            "Your account is still in pending. Please Accept Invite to proceed.",
        SOMETHING_WENT_WRONG:
            "Something went wrong, please try again.",
        OWNER_NOT_DELETED: "Owner cannot be deleted",
        SETTINGS_NOT_MODIFIED:
            "Unable to update Settings.",
        POST_ANALYSIS_NOT_FOUND:
            "Post Analysis of this call not found",
        POST_ANALYSIS_NOT_MODIFIED:
            "Unable to update call post analysis.",
        SETTINGS_NOT_FOUND:
            "You have not added any Settings for this Account.",
        EVENTS_NOT_FOUND:
            "Sorry, this event does not exist.",
        EVENTS_NOT_MODIFIED:
            "Events created on Ripcord Calendar cannot be unconfirmed.",
        EVENTS_NOT_ADDED:
            "Error while Creating Event into Ripcord Calendar",
        EVENTS_NOT_DELETED:
            "Error while Deleting Event into Ripcord Calendar",
        EVENT_DATE_MODIFY_ERROR:
            "Sorry, the event you're looking to modify/cancel is in the past.",
        NYLAS_VERIFICATION_ERROR:
            "X-Nylas-Signature failed verification",
        MISSING_NYLAS_TOKEN:
            "This Email account is not Synced with RipCord.",
        PRIMARY_EMAIL_NOT_ENABLED:
            "Please Enable the Primary Email Account in User Settings",
        EVENT_SENT_VIA_SMS:
            "Your Account Calendar does not allow you to post events via Ripcord, but the recipient has been informed VIA SMS ",
        ALERT_NOT_ADDED:
            "Error while Creating Alert in System",
        ALERT_NOT_FOUND:
            "You have not added such alerts for this Account.",
        ALERT_NOT_MODIFIED:
            "Unable to update Alerts Messages.",
        ALERT_NOT_DELETED:
            "Error while Deleting the Alert Message",
        PRIMARY_CALENDAR:
            "No Calendar Marked Primary",
        BOOKMARK_NOT_ADDED:
            "Error while Creating Bookmark in System",
        BOOKMARK_NOT_FOUND:
            "You have not added such Bookmarks for this Account.",
        BOOKMARK_NOT_MODIFIED:
            "Unable to update Bookmarks Messages.",
        BOOKMARK_NOT_DELETED:
            "Error while Deleting the Bookmark Message",
        APP_NOT_EXIST:
            "App not founnd",
        PHONE_NUMBER_NO_FOUND:
            "User Phone number not found",
        ALREADY_IN_CALL:
            "You are already in a call",
        CONVERSATION_NOT_EXIST:
            "Conversation does not exist, or you do not have access",
        CALENDAR_UNSYNCED:
            "You have not Synced any Calendar with Ripcord",
        CALENDAR_ALREADY_SYNCED:
            "The calendar you are trying to mark as primary is already in use by Ripcord User",
        COUPON_NOT_CREATED:
            "Error while Creating a Coupon in System",
        COUPON_NOT_FOUND:
            "You have not added such Coupon.",
        COUPON_NOT_VALIDATE:
            "Failed to validate Coupon. Its validity might have expired Or its usage limit is Exceeded",
        COUPON_CODE_NOT_MODIFIED:
            "Coupon Code cannot be modified.",
        COUPON_NOT_DELETED:
            "Error while Deleting the Coupon",
        COUPON_NOT_REDEEMED:
            "Failed to redeem the coupon"
    };

    static SUCCESS_MESSAGES = {
        ACCOUNT_CREATION_VERIFY_EMAIL:
            "Account created successfully. Please verify your email account by click on your email inbox",
        ACCOUNT_CREATION:
            "Account created successfully.",
        USER_CREATION: "User Created Successfully",
        SUBSCRIBE_PLAN:
            "You Subscribe Plan Successfully",
        PHONE_NUMBER_SELECT:
            "Phone Number Select Successfully",
        PHONE_CALL_STARTED: "Phone Call Started...",
        USER_DELETED: "User Deleted Successfully",
        USER_UPDATED: "User Updated Successfully",
        RESET_PASSWORD:
            "Reset Password Link sent Successfully",
        PASSWORD_RESET: "Password Reset Successfully",
        PROFILE_CREATED: "Account Setup Successfully",
        EMAIL_CREATED:
            "Account Email Setup Successfully",
        ACCOUNT_VERIFIED:
            "Account Verified Successfully",
        CALENDAR_SYNCED:
            "Your Email Provider Calendars are Synced Successfully",
        CALENDAR_UNSYNCED:
            "You have unsynced your Calendars Successfully",
        SETTINGS_CREATED:
            "Notification settings are Updated Successfully",
        CALL_SCHEDULED: "Call Scheduled Successfully",
        CALL_DELETED: "Call Deleted Successfully",
        PRIMARY_CALENDAR: "Calendar Marked Primary Successfully",
        EVENT_DELETED:
            "Calendar Event Deleted Successfully",
        EVENT_ADDED:
            "Calendar Events Added Successfully",
        EVENT_UPDATED:
            "Calendar Event Updated Successfully",
        SCHEDULER_EXECUTED:
            "Scheduler Executed Successfully",
        POLLING_POSTED:
            "Your Poll Posted Successfully",
        ALERT_ADDED:
            "Alert Message Added Successfully",
        ALERT_MODIFIED:
            "Alert Message Updated Successfully",
        ALERT_DELETED:
            "Alert Message Deleted Successfully",
        BOOKMARK_ADDED:
            "Bookmark Message Added Successfully",
        BOOKMARK_MODIFIED:
            "Bookmark Updated Successfully",
        BOOKMARK_DELETED:
            "Bookmark Deleted Successfully",
        STRIPE_METERED_BILLING:
            "Stripe Metered Billing is Inititated Successfully",
        STRIPE_CARD_UPDATED:
            "Your Stripe's Card Source has been Updated Successfully",
        STRIPE_ACCOUNT_SUSPENDED:
            "Account has been suspended",
        STRIPE_ACCOUNT_SUSTAINED:
            "Your subscription payment has been received and Account is Reactivated Successfully",
        POST_CALL_ANALYSIS:
            "Post Call analysis of the month is retrieved Successfully",
        APP_CREATED:
            "App created Successfully",
        APP_MODIFIED:
            "App Updated Successfully",
        APP_DELETED:
            "App Deleted Successfully",
        WORKING: "Working...",
        COUPON_DELETED:
            "Coupon Code Deleted Successfully",
        COUPON_REDEEMED:
            "You have redeemed the Coupon successfully"
    };

    static USER_ROLES = {
        COACH: "COACH",
        EXTERNAL_COACH: "EXTERNAL-COACH",
        ADMIN: "ADMIN",
        TRAINEE: "TRAINEE"
    };

    static GENDER = {
        MALE: "MALE",
        FEMALE: "FEMALE"
    };

    static TOKEN_EXPIRY = 86400000;

    static STATUS = {
        ACTIVE: "ACTIVE",
        INACTIVE: "INACTIVE"
    };

    static CHAT_GROUP = {
        ADMINS: "ADMINS",
        COACHS: "COACHS",
        TRAINEES: "TRAINEES"
    };

    static CALENDAR = {
        NAME_CHECK: "Calendar",
        SYNC: {
            GOOGLE: "google",
            OFFICE365: "office365"
        }
    };
}

export class Utility {

    static matchRegex(toMatch: string, regexp: any) {
        return new RegExp(regexp).test(toMatch);
    }

    static async searchPhoneNumber(text: string) {
        // @ts-ignore
        const numbers = await findNumbers(text, "US", {v2: true});
        if (!numbers || numbers.length === 0) {
            return null;
        }

        const {number: {number: userPhone = null} = {}} = numbers[0] || {};

        return userPhone;
    }

    static getGroup(role) {
        if (role === AppConstants.USER_ROLES.ADMIN) {
            return AppConstants.CHAT_GROUP.ADMINS;
        }

        if (role === AppConstants.USER_ROLES.COACH) {
            return AppConstants.CHAT_GROUP.COACHS;
        }

        if (role === AppConstants.USER_ROLES.TRAINEE) {
            return AppConstants.CHAT_GROUP.TRAINEES;
        }

        return null;
    }

    static getSalesPersonCallSlackContent(name, url, id, channel) {
        return {
            "text": `Would you like to listen to ${name} sales call?`,
            channel,
            "attachments": [
                {
                    "text": "Choose an option please...",
                    "color": "#3AA3E3",
                    callback_id: id,
                    "attachment_type": "default",
                    "actions": [
                        {
                            "name": "yes",
                            "text": "Yes",
                            "type": "button",
                            "value": "true",
                            url
                        },
                        {
                            "name": "no",
                            "text": "No",
                            "type": "button",
                            "value": "false"
                        },
                        {
                            "name": "dont_ask",
                            "text": "Don't Ask Again Today",
                            "style": "danger",
                            "type": "button",
                            "value": "delay"
                        }
                    ]
                }
            ]
        };
    }

    static getSlackMessagePayload(payload) {
        return omit(payload, identity);
    }

    static getQueryString(params: object) {
        params = pickBy(params, identity);

        return queryString.stringify(params);
    }

    static getFormattedDateTime(date: any, format = "dddd, MMMM DD, YYYY", zone = null) {
        if (!date) {
            return "";
        }

        $log.info(zone, momentTimeZone(date).tz(zone) ? momentTimeZone(date).tz(zone).format(format) : "false", date);

        if (zone) {
            return momentTimeZone(date).tz(zone).format(format);
        }

        return moment(date).format();
    }

    static getFormattedTime(_duration, numeralDefaultFormat = "hh:mm") {
        const duration = numeral(_duration);
        numeral.defaultFormat(numeralDefaultFormat);

        return duration.format();
    }

    static saveFileAsPdf = async (callId: string, token: string, path: string) => {
        const browser = await puppeteer.launch({args: ["--no-sandbox", "--disable-setuid-sandbox"]});
        const page = await browser.newPage();
        await page.goto(`${Config.WEB_URL}/calls/${callId}/${token}`, {waitUntil: "networkidle2"});
        await page.pdf({path, format: "A4"});

        await browser.close();
    };

    static getRandomNumber(pattern = "R00000") {
        return randomatic(pattern);
    }

    static checkYear(val,date: any) {	
        let d = new Date(date);	
        let t = new Date();	
        let year = d.getFullYear();	
        let year1 = t.getFullYear();	
        let total = year1 - year;
        if(!isNaN(year)){	
            if(val <= total){	
                return true	
            }else{	
                return false	
            }	
        }	
        return false;	
    }
    
    static getFormattedNumber = (number) => numeral(number).format("00000");
}
