<p>Hello <%=fullName %><br/>
You have requested for reset password for the following account:<br/>
User: <%=to %><br/>
If this was a mistake, just ignore this email.</p>
<p>To reset your password, visit the following link</p>
<a href="<%=process.env.WEB_URL %>/account/reset-password/<%=resetPasswordToken %>">Reset Password Link</a><br/>
Or<br/>
<%=process.env.WEB_URL %>/account/reset-password/<%=resetPasswordToken %>
<br/> <br/>
Please note that this link is valid for 24 hours.
<br/> <br/>
Thanks <br/>
www.thai-plant.com
