export default class Config {
    public static STRIPE_KEY = process.env.STRIPE_KEY || "";

    public static EMAIL_CONFIG = {
        smtpConfig: {
            host: process.env.SMTP_HOST || "smtp.gmail.com",
            port: 587,
            secure: false,
            auth: {
                user: process.env.SMTP_USER,
                pass: process.env.SMTP_PASS
            },
            tls: {
                rejectUnauthorized: false
            }
        },
        from: process.env.EMAIL_FROM
    };

    public static UPLOAD_URL = process.env.UPLOAD_URL;

    public static ENVIRONMENTS = {
        PROD: "production"
    };

    public static EMAIL_TEMPLATES = {
        resetPassword: {
            subject: "Request to Reset Password for MEAN Thai PLANT MANAGEMENT SYSTEM",
            bodyPath: `${__dirname}/email-templates/reset-password.tpl`
        }
    };

    public static JWT_SECRET = process.env.JWT_SECRET;

    public static SECRET_TOKEN = process.env.SECRET_TOKEN || "8324a810-7741-4d25-b879-e53f58e41f25";

    public static MONGO_URI = process.env.MONGO_URI;

    public static API_PORT = process.env.API_PORT;

    public static API_URL = process.env.API_URL;

    public static SOCKET_URL = process.env.SOCKET_URL;

    public static SOCKET_PORT = process.env.SOCKET_PORT;

    public static REDIS = {
        HOST: process.env.REDIS_HOST,
        PORT: process.env.REDIS_PORT || 6379,
        TIME_OUT: process.env.REDIS_REQUEST_TIME_OUT || 5000
    };

    public static WATSON = {
        IDENTITY_TOKEN_URL: process.env.WATSON_IDENTITY_TOKEN_URL || "https://iam.bluemix.net/identity/token",
        GRANT_TYPE: process.env.WATSON_GRANT_TYPE || "urn:ibm:params:oauth:grant-type:apikey",
        SPEECH_TO_TEXT: {
            URL: process.env.WATSON_SPEECH_TO_TEXT_URL || "wss://stream.watsonplatform.net/speech-to-text/api/v1/recognize?model=en-US_BroadbandModel",
            API_KEY: process.env.WATSON_SPEECH_TO_TEXT_API_KEY
        },
        NLU: {
            URL: process.env.WATSON_SPEECH_TO_TEXT_URL || "https://gateway.watsonplatform.net/natural-language-understanding/api",
            API_KEY: process.env.WATSON_NLU_API_KEY,
            VERSION: process.env.WATSON_NLU_VERSION || "2018-11-16"
        }
    };

    private static _NYLAS = {
        APP_ID: process.env.NYLAS_APP_ID,
        SECRET_ID: process.env.NYLAS_APP_SECRET,
        SCOPE: process.env.NYLAS_SCOPE,
        PROVIDER: {
            GOOGLE: process.env.NYLAS_GOOGLE_PROVIDER || "gmail",
            YAHOO: process.env.NYLAS_YAHOO_PROVIDER || "yahoo",
            OUTLOOK: process.env.NYLAS_OUTLOOK_PROVIDER || "outlook",
            OFFICE365: process.env.NYLAS_OFFICE_PROVIDER
        },
        URI: process.env.NYLAS_URL,
        EVENT: {
            DELETE: "event.deleted",
            CREATE: "event.created",
            UPDATE: "event.updated"
        }
    };
    public static get NYLAS() {
        return Config._NYLAS;
    }

    public static set NYLAS(value) {
        Config._NYLAS = value;
    }

    public static AWS = {
        REGION: process.env.AWS_REGION || "us-east-1",
        API_VERSION: process.env.API_VERSION || "2006-03-01",
        BUCKET: process.env.AWS_BUCKET,
        SECRET_KEY: process.env.AWS_ACCESS_SECRET_KEY,
        ACCESS_KEY: process.env.AWS_ACCESS_KEY_ID,
        ENV_PREFIX: process.env.AWS_BUCKET_ENVIRONMENT_PREFIX
    };

    public static NEXMO = {
        API_KEY: process.env.NEXMO_API_KEY,
        API_SECRET: process.env.NEXMO_API_SECRET,
        NUMBER: process.env.NEXMO_FROM_NUMBER,
        VOICE: {
            APP_ID: process.env.NEXMO_APP_ID,
            PRIVATE_KEY: process.env.NEXMO_PRIVATE_KEY_PATH
        }
    };

    public static WEB_URL = process.env.WEB_URL;
    public static SLACK_APP = {
        ID: process.env.SLACK_APP_ID,
        CLIENT_ID: process.env.SLACK_CLIENT_ID,
        CLIENT_SECRET: process.env.SLACK_CLIENT_SECRET,
        URI: process.env.SLACK_URI
    };

    public static ELASTIC = {
        HOST: process.env.ELASTIC_HOST || "http://localhost:9200"
    };

    public static STRIPE = {
        WARNING_COUNT: process.env.STRIPEWARNING || 4,
        WEBHOOK_KEY: process.env.STRIPE_WEBHOOK_SECRET_KEY,
        METERED_BILLING: {
            CURRENCY: process.env.STRIPE_CURRENCY || "usd",
            INTERVAL: process.env.STRIPE_INTERVAL || "month",
            NICKNAME: process.env.STRIPE_NICKNAME || "Metered Plan",
            AMOUNT: process.env.STRIPE_AMOUNT || 50,
            USAGE_TYPE: process.env.STRIPE_USAGE_TYPE || "metered",
            MINUTES_USAGE_LIMIT: process.env.STRIPE_RECORDING_USAGE_LIMIT || 10000,
            USERS_USAGE_LIMIT: process.env.STRIPE_USER_USAGE_LIMIT || 30,
            CARD_TOKEN: process.env.STRIPE_CARD_TOKEN || "tok_visa"
        }
    };

    public static CRON_JOB_IDS = {
        STRIPE_JOB: "4f4f5984-6ccc-11e9-a923-1681be663d3e"
    };

    public static SEED = {
        ZAPIER: {
            APP_NAME: process.env.ZAPIER_APP_NAME || `ripcord-${process.env.ENVIRONMENT_MODE}`,
            CLIENT_ID: process.env.ZAPIER_CLIENT_ID || "5b02553e-517f-48b6-9a29-308e95a45f94",
            CLIENT_SECRET: process.env.ZAPIER_CLIENT_SERCRET || "ef49cfdb-b0a9-4a78-9c84-bb4b3560dc8c",
            REDIRECT_URI: process.env.ZAPIER_REDIRECT_URI || "https://zapier.com/dashboard/auth/oauth/return/App15335CLIAPI/",
            APP_ID: process.env.ZAPIER_APP_ID || "8l2p591z",
            DESCRIPTION: "Ripcord Zapier APP",
            HOMEPAGE_URI: process.env.ZAPIER_HOMEPAGE_URI || "https://talk.ripcord.io"
        }
    };
}
