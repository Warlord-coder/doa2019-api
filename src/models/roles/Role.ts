import {Property} from "@tsed/common";
import {Model} from "@tsed/mongoose";
import {BaseModel} from "../Base";

@Model()
export class Role extends BaseModel {
    @Property()
    name: string;

    @Property()
    department_en: string;


    @Property()
    department_th: string;


    @Property()
    rolesInfo: string[];


    /**
     *
     * @param skipFields
     */
    public display(skipFields = []) {
        return super.display(skipFields, [], this);
    }
}
