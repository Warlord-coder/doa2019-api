import {Property} from "@tsed/common";
import {Indexed, Model, Schema} from "@tsed/mongoose";
import * as mongoose from "mongoose";
import {BaseModel} from "../Base";

@Model()
export class WithDraw extends BaseModel {
    @Property()
    w_no: string;

    @Property()
    dateOfReceiving: string;

    @Property()
    serviceRequestLetter: string;

    @Property()
    serviceRequestLetterAttachment: string;

    @Property()
    agenciesRequestSeedGermination: string;

    @Property()
    agenciesSubRequestSeedGermination: string;

    @Property()
    agenciesSubRequestSeedGerminationInput: string;

    @Property()
    numberOfRequestor: string;

    @Property()
    requestorAddress: string;

    @Property()
    telephoneNumber: string;

    @Property()
    email: string;

    @Property()
    objective: string;

    @Property()
    owner: String;

    @Property()
    withdraw_approve_doc_attached: string;


    @Property()
    withdraw_doc_for_getting_service_attached: string;


    @Property()
    withdraw_doc_53_attached: string;


    @Property()
    withdraw_doc_citizenID_attached: string;


    @Property()
    withdraw_doc_research_attached: string;


    @Property()
    withdraw_doc_teaching_plan_attached: string;


    @Property()
    withdraw_doc_request_attached: string;


    @Property()
    withdraw_submit_plant_doc_attached: string;


    @Property()
    withdraw_doc_MTA_attached: string;

    @Property()
    withdraw_remark: string;

    @Property()
    withdraw_summary_stock: string;

    @Property()
    withdraw_doc_MTA: string;

    @Property()
    withdraw_submit_plant_doc: string;

    @Property()
    withdraw_summit_plant_date: string;

    @Property()
    withdraw_doc_request_date: string;

    @Property()
    withdraw_doc_request: string;

    @Property()
    withdraw_doc_receive_date: string;

    @Property()
    withdraw_approve_date: string;

    @Property()
    withdraw_approve_doc: string;

    @Property()
    withdraw_summary_stock_select: string;

    /**
     *
     * @param skipFields
     */
    public display(skipFields = []) {
        return super.display(skipFields, [], this);
    }
}
