import {Property} from "@tsed/common";
import {Model, Schema} from "@tsed/mongoose";
import {BaseModel} from "../Base";
import * as mongoose from "mongoose";

@Model()
export class Lab extends BaseModel {
    @Property()
    lab_no: string;

    @Property()
    reg_seed_receive: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "Volume"
    })
    volume_no: string;

    @Property()
    stock_type: string;

    @Property()
    is_alert: boolean;

    @Property()
    notification_date: string;

    @Property()
    isNewLab: boolean;

    @Property()
    isPlantCancel: boolean;
    /**
     *
     * @param skipFields
     */
    public display(skipFields = []) {
        return super.display(skipFields, [], this);
    }
}
