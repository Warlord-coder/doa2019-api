import {Property} from "@tsed/common";
import {Model} from "@tsed/mongoose";
import {BaseModel} from "../Base";

@Model()
export class Passport extends BaseModel {
    @Property()
    passport_no: string;

    @Property()
    reg_date: string;

    @Property()
    reg_seed_receive: string;

    @Property()
    reg_seed_sent_no: string;

    @Property()
    reg_attach_file: string;

    @Property()
    isVolumeLock: boolean;

    @Property()
    isNewPassport: boolean;

    /**
     *
     * @param skipFields
     */
    public display(skipFields = []) {
        return super.display(skipFields, [], this);
    }
}
