import {Property} from "@tsed/common";
import {Indexed, Model, Schema} from "@tsed/mongoose";
import {BaseModel} from "../Base";
import AppConstants from "../../utils/constants";

@Model()
export class KnowledgeNews extends BaseModel {
    @Property()
    topic: string;


    @Property()
    shortDesc: string;

    @Property()
    longDesc: string;

    @Property()
    imageFile: string;

    @Property()
    attachFile: string;

    @Property()
    flag: boolean;

    @Property()
    @Indexed()
    @Schema({
        enum: [AppConstants.STATUS.ACTIVE,
            AppConstants.STATUS.INACTIVE]
    })
    status: string;

    /**
     *
     * @param skipFields
     */
    public display(skipFields = []) {
        return super.display(skipFields, [], this);
    }
}
