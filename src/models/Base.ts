import {IgnoreProperty, Property} from "@tsed/common";
import {Schema} from "@tsed/mongoose";
import * as mongoose from "mongoose";
import {difference} from "lodash";

import {User} from "./users/User";

export abstract class BaseModel {

    @IgnoreProperty()
    _id: String;

    @Schema({
        timestamps: true,
        default: Date.now
    })
    createdAt: Date;

    @Schema({
        timestamps: true,
        default: Date.now
    })
    updatedAt: Date;

    @Property()
    @Schema({
        timestamps: true
    })
    deletedAt: Date;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    })
    createdBy: String;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    })
    updatedBy: String;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    })
    deletedBy: String;

    @Property()
    isDeleted: boolean

    protected skipFields() {
        return ["__v", "resetPasswordToken", "resetPasswordExpiry", "password", "lastLogin", "receiveCandidates",
            "accessToken", "refreshToken", "accessToken", "refreshToken", "channelId",
            "slackJoinMsgSend",
            "sharedTokens", "channelId", "channelResourceId"];
    }

    /**
     *
     * @param skipFields
     */
    public display(skipFields = [], keys = [], ref) {
        const userInfo = {};
        skipFields = [...ref.skipFields(), ...skipFields];
        // @ts-ignore
        difference([...Object.keys(this.toObject()), ...keys], skipFields).forEach(field => {
            if (ref[field] && ref[field].display) {
                userInfo[field] = this[field].display();

                return;
            }

            userInfo[field] = this[field];
        });

        return userInfo;
    }

}

