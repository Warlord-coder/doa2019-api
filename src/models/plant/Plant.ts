import {Property} from "@tsed/common";
import {Indexed, Model, Schema} from "@tsed/mongoose";
import {BaseModel} from "../Base";
import AppConstants from "../../utils/constants";

@Model()
export class Plant extends BaseModel {
    @Property()
    @Indexed()
    @Schema({
        enum: [AppConstants.STATUS.ACTIVE,
            AppConstants.STATUS.INACTIVE]
    })
    plant_status: string;

    @Property()
    plantTypeName_sci_en: string;

    @Property()
    plantImg: string;


    @Property()
    plantTypeName_sci_global_en: string;

    @Property()
    plantTypeName_sci_other_en: string;

    @Property()
    plantTypeName_sci_th: string;

    @Property()
    plantTypeName_sci_global_th: string;

    @Property()
    plantTypeName_sci_other_th: string;

    @Property()
    plant_category: string;

    @Property()
    plant_character: string;

    @Property()
    plant_group: string;

    @Property()
    plant_group_type: string;

    /**
     *
     * @param skipFields
     */
    public display(skipFields = []) {
        return super.display(skipFields, [], this);
    }
}
