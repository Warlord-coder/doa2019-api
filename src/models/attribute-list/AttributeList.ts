import {Property} from "@tsed/common";
import * as mongoose from "mongoose";
import {Model, Schema} from "@tsed/mongoose";
import {BaseModel} from "../Base";
import {AttributeSet} from "../attribute-set/AttributeSet";

@Model()
export class AttributeList extends BaseModel {
    @Property()
    setName: string;

    @Property()
    @Schema({
        type: [mongoose.Schema.Types.ObjectId],
        ref: "AttributeSet"
    })
    attributes: AttributeSet[];

    /**
     *
     * @param skipFields
     */
    public display(skipFields = []) {
        return super.display(skipFields, [], this);
    }
}
