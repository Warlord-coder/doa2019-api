import {Property} from "@tsed/common";
import {Indexed, Model, Schema} from "@tsed/mongoose";
import {BaseModel} from "../Base";
import AppConstants from "../../utils/constants";

@Model()
export class CommonCondition extends BaseModel {
    @Property()
    @Indexed()
    @Schema({
        enum: [AppConstants.STATUS.ACTIVE,
            AppConstants.STATUS.INACTIVE]
    })
    status: string;

    @Property()
    name: string;


    @Property()
    type: string;

    @Property()
    value: string;

    @Property()
    conditionAt: string;

    /**
     *
     * @param skipFields
     */
    public display(skipFields = []) {
        return super.display(skipFields, [], this);
    }
}
