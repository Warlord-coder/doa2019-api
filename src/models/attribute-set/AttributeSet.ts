import {Property} from "@tsed/common";
import {Model} from "@tsed/mongoose";
import {BaseModel} from "../Base";

@Model()
export class AttributeSet extends BaseModel {
    @Property()
    code: string;

    @Property()
    min: string;

    @Property()
    max: string;

    @Property()
    name_en: string;

    @Property()
    name_th: string;

    @Property()
    require: string;

    @Property()
    fieldInput: string;

    @Property()
    isVisibleFrontend: string;

    @Property()
    optionValue: object[];

    /**
     *
     * @param skipFields
     */
    public display(skipFields = []) {
        return super.display(skipFields, [], this);
    }
}
