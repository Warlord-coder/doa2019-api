import {Property} from "@tsed/common";
import {Indexed, Model, Schema} from "@tsed/mongoose";
import * as mongoose from "mongoose";
import {BaseModel} from "../Base";

@Model()
export class Volume extends BaseModel {
    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "Passport"
    })
    passport_no: string;


    @Property()
    volume_no: string;

    @Property()
    form_type: String;

    @Property()
    isRegisterFormLock: boolean;

    /**
     *
     * @param skipFields
     */
    public display(skipFields = []) {
        return super.display(skipFields, [], this);
    }
}
