import {Property} from "@tsed/common";
import * as mongoose from "mongoose";
import {Model, Schema} from "@tsed/mongoose";
import {BaseModel} from "../Base";

@Model()
export class PlantRegister extends BaseModel {
    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "Passport"
    })
    passport: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "Volume"
    })
    volume: string;


    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "PlantCharacter"
    })
    reg_plant_sci_name: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "PlantGroup"
    })
    reg_gene_type: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "PlantType"
    })
    plant_gene_name: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "PlantGroupType"
    })
    reg_gene_category: string;

    @Property()
    reg_plant_alter_name: any;

    @Property()
    reg_seed_collect_date: string;

    @Property()
    selfing_input: string;

    @Property()
    selfing_dropdown: string;

    @Property()
    oop_input: string;

    @Property()
    oop_dropdown: string;

    @Property()
    other_input: string;

    @Property()
    other_dropdown: string;

    @Property()
    department_type: string;

    @Property()
    department_sub_type: string;

    @Property()
    department_name: string;

    @Property()
    reg_seed_no: string;

    @Property()
    reg_gene_id: string;

    @Property()
    reg_seed_recovery_date: string;

    @Property()
    reg_seed_receive_date: string;

    @Property()
    reg_seed_collect_person: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "CommonCountry"
    })
    source_country: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "CommonCountry"
    })
    source_country_1: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "CommonProvince"
    })
    source_province: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "CommonAumper"
    })
    source_district: string;


    @Property()
    source_sea_level: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "WareHouse"
    })
    m9warehouse_plant_refer_id: string;

    @Property()
    source_address: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "PlantRegister"
    })
    parentPlant: string;

    @Property()
    reg_agreement_field: string;

    @Property()
    lat: string;

    @Property()
    long: string;

    @Property()
    source_by_department: string;

    @Property()
    reg_seed_owner: string;

    @Property()
    reg_check_seed_ref: string;

    @Property()
    reg_check_molecular: string;

    @Property()
    reg_seed_sender: string;

    @Property()
    reg_character_brief: string;

    @Property()
    reg_character_for_record: string;

    @Property()
    reg_sent_lab_date: string;

    @Property()
    reg_remark: string;

    @Property()
    reg_character_attached_file: string;

    @Property()
    reg_agreement_attach_file: string;

    @Property()
    m3_status: string;

    @Property()
    isSplit: boolean;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "Lab"
    })
    m8_lab_id: string;

    @Property()
    m8_lab_status: string;

    @Property()
    lab_test_plant_amount_input: string;

    @Property()
    lab_test_plant_amount_dropdown: string;

    @Property()
    lab_test_plant_amount: string;

    @Property()
    lab_test_moisture_date: string;

    @Property()
    lab_test_moisture_method: string;

    @Property()
    lab_test_moisture_amount: string;

    @Property()
    lab_test_moisture_person: string;

    @Property()
    lab_test_growth_percent: string;

    @Property()
    lab_test_growth_date: string;

    @Property()
    lab_test_growth_method: string;

    @Property()
    lab_test_growth_amount: string;

    @Property()
    lab_test_growth_person: string;

    @Property()
    lab_test_strong_percent: string;

    @Property()
    lab_test_strong_date: string;

    @Property()
    lab_test_strong_method: string;

    @Property()
    lab_test_strong_amount: string;

    @Property()
    lab_test_strong_person: string;

    @Property()
    lab_test_alive_percent: string;

    @Property()
    lab_test_alive_date: string;

    @Property()
    lab_test_alive_method: string;

    @Property()
    lab_test_alive_amount: string;

    @Property()
    lab_test_alive_person: string;

    @Property()
    lab_seed_rest: string;

    @Property()
    lab_seed_rest_fix_method: string;

    @Property()
    lab_seed_rest_fix_date: string;

    @Property()
    lab_seed_amount: string;

    @Property()
    lab_10seed_weight: string;

    @Property()
    lab_seed_est_amount: string;

    @Property()
    lab_plaint_character: string;

    @Property()
    lab_admin_name: string;

    @Property()
    lab_remark: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    })
    m3register_plant_updatedBy: string;

    @Property()
    m3register_plant_submittedAt: Date;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    })
    m3register_plant_submittedBy: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    })
    m3register_plant_createdBy: string;

    @Property()
    m3register_plant_createdAt: Date;

    @Property()
    m3register_plant_updatedAt: Date;
// //////////// _m8_ ///////////////////

    @Property()
    m8lab_plant_updatedAt: Date;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    })
    m8lab_plant_updatedBy: string;

    @Property()
    m8lab_plant_submittedAt: Date;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    })
    m8lab_plant_submittedBy: string;

    @Property()
    m8lab_plant_createdAt: Date;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    })
    m8lab_plant_createdBy: string;

// //////////// _m9_ ///////////////////

    @Property()
    m9warehouse_plant_updatedAt: Date;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    })
    m9warehouse_plant_updatedBy: string;

    @Property()
    m9warehouse_plant_submittedAt: Date;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    })
    m9warehouse_plant_submittedBy: string;

    @Property()
    m9warehouse_plant_createdAt: Date;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    })
    m9warehouse_plant_createdBy: string;

    @Property()
    m2doa_plant_createdAt: Date;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    })
    m2doa_plant_creatdBy: string;

    @Property()
    m2doa_plant_updatedAt: Date;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    })
    m2doa_plant_updatedBy: string;

    /**
     *
     * @param skipFields
     */
    public display(skipFields = [], ref) {
        return super.display(skipFields, [], ref || this);
    }
}
