import {Property} from "@tsed/common";
import {Model} from "@tsed/mongoose";
import {BaseModel} from "../Base";

@Model()
export class Recovery extends BaseModel {
    @Property()
    m_no: string;

    @Property()
    revival_plant: string;

    @Property()
    date_of_submission: string;

    @Property()
    place: string;

    @Property()
    recover_file: string;

    /**
     *
     * @param skipFields
     */
    public display(skipFields = []) {
        return super.display(skipFields, [], this);
    }
}
