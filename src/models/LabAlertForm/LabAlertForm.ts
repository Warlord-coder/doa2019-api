import {Property} from "@tsed/common";
import {Model, Schema} from "@tsed/mongoose";
import * as mongoose from "mongoose";
import {WareHouse} from "../warehouse/WareHouse";
import {BaseModel} from "../Base";

@Model()
export class LabAlertForm extends BaseModel {
    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "WareHouse"
    })
    warehouse: string;

    @Property()
    alertFormStatus: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "Lab"
    })
    labId: string;

    @Property()
    @Schema({	
        type: mongoose.Schema.Types.ObjectId,	
        ref: "passports"	
    })	
    passport: string;	
    @Property()	
    @Schema({	
        type: mongoose.Schema.Types.ObjectId,	
        ref: "plantgroup"	
    })	
    reg_gene_type: string;	

    @Property()	
    register_alert_form: string;

    @Property()	
    wh_status: string;	

    @Property()	
    @Schema({	
        type: mongoose.Schema.Types.ObjectId,	
        ref: "plantype"	
    })	
    plant_gene_name: string;

    @Property()
    lab_test_plant_amount_input: string;


    @Property()
    lab_test_plant_amount_dropdown: string;


    @Property()
    lab_test_plant_amount: string;


    @Property()
    lab_test_moisture_date: string;


    @Property()
    lab_test_moisture_method: string;


    @Property()
    lab_test_moisture_amount: string;


    @Property()
    lab_test_moisture_person: string;


    @Property()
    lab_test_growth_percent: string;


    @Property()
    lab_test_growth_date: string;


    @Property()
    lab_test_growth_method: string;


    @Property()
    lab_test_growth_amount: string;


    @Property()
    lab_test_growth_person: string;


    @Property()
    lab_test_strong_percent: string;


    @Property()
    lab_test_strong_date: string;


    @Property()
    lab_test_strong_method: string;

    @Property()
    lab_test_strong_amount: string;

    @Property()
    lab_test_strong_person: string;

    @Property()
    lab_test_alive_percent: string;

    @Property()
    lab_test_alive_date: string;

    @Property()
    lab_test_alive_method: string;

    @Property()
    lab_test_alive_amount: string;

    @Property()
    lab_test_alive_person: string;

    @Property()
    lab_seed_rest: string;

    @Property()
    lab_seed_rest_fix_method: string;

    @Property()
    lab_seed_rest_fix_date: string;

    @Property()
    lab_seed_amount: string;

    @Property()
    lab_10seed_weight: string;

    @Property()
    lab_seed_est_amount: string;

    @Property()
    lab_admin_name: string;

    @Property()
    lab_remark: string;

    @Property()
    selection_alert: string;

    @Property()
    alert_test_amount: Number;

    @Property()
    lab_test_check_seed_date: string;

    @Property()
    lab_test_check_seed_name: string;

    @Property()
    wh_batch_moisture_percent: string;

    @Property()
    lab_plaint_character: string;

    /**
     *
     * @param skipFields
     */
    public display(skipFields = []) {
        return super.display(skipFields, [], this);
    }
}
