import {Property} from "@tsed/common";
import {Indexed, Model, Schema} from "@tsed/mongoose";
import {BaseModel} from "../Base";
import AppConstants from "../../utils/constants";

@Model()
export class PlantCategory extends BaseModel {
    @Property()
    @Indexed()
    @Schema({
        enum: [AppConstants.STATUS.ACTIVE,
            AppConstants.STATUS.INACTIVE]
    })
    status: string;

    @Property()
    symbol: string;

    @Property()
    categoryName_en: string;


    @Property()
    categoryName_th: string;

    /**
     *
     * @param skipFields
     */
    public display(skipFields = []) {
        return super.display(skipFields, [], this);
    }
}
