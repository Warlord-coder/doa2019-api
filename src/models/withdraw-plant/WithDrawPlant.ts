import {PlantRegister} from "../plant-register/PlantRegister";
import {WareHouse} from "../warehouse/WareHouse";
import {Property} from "@tsed/common";
import * as mongoose from "mongoose";
import {Indexed, Model, Schema} from "@tsed/mongoose";
@Model({
    schemaOptions: {
        strict: false
    }
})
export class WithDrawPlant extends WareHouse {

    @Property()
    room5: undefined;

    @Property()
    anteArray: undefined;

    @Property()
    room10: undefined;

    @Property()
    room130: undefined;

    @Property()
    withDrawStatus: string;

    @Property()
    withdraw_request_stock: string;

    @Property()
    withdraw_request_stockSelect: string;

    @Property()
    admin_amount: string;

    @Property()
    m9plant_warehouse_id: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "WithDraw"
    })
    m5_assigned: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "PlantCharacter"
    })
    reg_plant_sci_name: string;
    /**
     *
     * @param skipFields
     */
    public display(skipFields = [], ref) {
        return super.display(skipFields, ref || this);
    }
}
