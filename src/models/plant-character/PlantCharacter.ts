import {Property} from "@tsed/common";
import  * as mongoose from "mongoose";
import {Indexed, Model, Schema} from "@tsed/mongoose";
import {BaseModel} from "../Base";
import AppConstants from "../../utils/constants";

@Model()
export class PlantCharacter extends BaseModel {
    @Property()
    @Indexed()
    @Schema({
        enum: [AppConstants.STATUS.ACTIVE,
            AppConstants.STATUS.INACTIVE]
    })
    status: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "PlantCategory"
    })
    plant_category: string;

    @Property()
    genus: string;

    @Property()
    species: string;

    @Property()
    reg_plant_common_name: string;

    @Property()
    reg_plant_alter_name: string[];

    /**
     *
     * @param skipFields
     */
    public display(skipFields = []) {
        return super.display(skipFields, [], this);
    }
}
