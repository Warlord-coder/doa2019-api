import * as uniqueValidator from "mongoose-unique-validator";
import {Email, Format, Pattern, Property, Required} from "@tsed/common";
import {Indexed, Model, MongoosePlugin, PreHook, Schema, Unique} from "@tsed/mongoose";
import {BaseModel} from "../Base";
import AppConstants from "../../utils/constants";
import * as bcrypt from "bcrypt-nodejs";
import * as mongoose from "mongoose";

@Model()
@MongoosePlugin(uniqueValidator, {message: "must be unique"})
export class User extends BaseModel {
    @Property()
    @Required()
    name: {
        firstName: String,
        lastName: String
    };

    @Property()
    @Unique()
    @Required()
    @Indexed()
    @Email()
    @Pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
    @Format("email")
    @Schema({lowercase: true})
    email: String;

    @Property()
    @Indexed()
    @Schema({
        enum: [AppConstants.GENDER.MALE,
            AppConstants.GENDER.FEMALE]
    })
    gender: String;

    @Property()
    @Indexed()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "Role"
    })
    role: String;

    @Property()
    @Indexed()
    @Schema({
        enum: [AppConstants.STATUS.ACTIVE,
            AppConstants.STATUS.INACTIVE]
    })
    status: String;

    @Property()
    @Unique()
    userName: String;

    @Property()
    profileImg: String;

    @Property()
    password: String;

    @Property()
    @Indexed()
    authToken: String;

    @Property()
    @Indexed()
    tokenExpiry: Number;

    @Property()
    @Indexed()
    resetPasswordToken: String;

    @Property()
    @Indexed()
    resetPasswordExpiry: Number;

    @Property()
    @Schema({
        timestamps: true
    })
    lastLogin: Date;

    @Property()
    isSuperAdmin: Boolean;
    /**
     *
     * @param skipFields
     */
    public display(skipFields = ["authToken", "tokenExpiry", "verifyAccountToken", "verifyAccountTokenExpiry"]) {
        return super.display(skipFields, ["fullName"], this);
    }

    /**
     * Authenticate - check if the passwords are the same
     *
     * @param {String} password
     * @param {Function} callback
     * @return {Boolean}
     * @api public
     */
    public authenticate(password, callback) {
        if (!callback) {
            return bcrypt.compareSync(password, this.password);
        }
        bcrypt.compare(password, this.password, callback);
    }

    public static encrypt(password): Promise<any> {
        return new Promise((resolve, reject) => {
            User.encryptPassword(password, (encryptErr, hashedPassword) => {
                if (encryptErr) {
                    return reject(encryptErr);
                }

                return resolve(hashedPassword);
            });
        });
    }

    /**
     * Encrypt password Static Method
     *
     * @param {String} password
     * @param {Function} callback
     * @return {String}
     * @api public
     */
    private static encryptPassword(password, callback) {
        if (!password) {
            if (!callback) {
                return null;
            } else {
                return callback("Missing password field");
            }
        }

        bcrypt.genSalt(10, (err, salt) => {
            if (err) {
                return callback(err);
            }
            bcrypt.hash(password, salt, () => {
            }, (err, hash) => {
                if (err) {
                    callback(err);
                }
                callback(null, hash);
            });
        });
    }

    private get fullName() {
        const {name: {firstName = "", lastName = ""} = {}} = this;

        return `${firstName} ${lastName}`;
    }
}
