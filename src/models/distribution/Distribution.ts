import {Property} from "@tsed/common";
import {Model} from "@tsed/mongoose";
import {BaseModel} from "../Base";

@Model()
export class Distribution extends BaseModel {
    @Property()
    d_no: string;

    @Property()
    distribution_request_date: string;

    @Property()
    distribution_request_form_no: string;

    @Property()
    distribution_request_form_no_attached: string;

    @Property()
    distribution_request_department: string;

    @Property()
    department_sub_type: string;

    @Property()
    service_request_name: string;

    @Property()
    service_request_address: string;

    @Property()
    service_request_telno: string;

    @Property()
    department_name: string;

    @Property()
    service_request_email: string;

    @Property()
    service_request_objective: string;

    @Property()
    service_request_amount: number;

    @Property()
    distribution_approve_doc_attached: string;


    @Property()
    distribution_doc_for_getting_service_attached: string;


    @Property()
    distribution_doc_53_attached: string;


    @Property()
    distribution_doc_citizenID_attached: string;


    @Property()
    distribution_doc_research_attached: string;


    @Property()
    distribution_doc_teaching_plan_attached: string;


    @Property()
    distribution_doc_request_attached: string;


    @Property()
    distribution_submit_plant_doc_attached: string;


    @Property()
    distribution_doc_MTA_attached: string;

    @Property()
    distribution_remark: string;

    @Property()
    distribution_summary_stock: string;

    @Property()
    distribution_doc_MTA: string;

    @Property()
    distribution_submit_plant_doc: string;

    @Property()
    distribution_summit_plant_date: string;

    @Property()
    distribution_doc_request_date: string;

    @Property()
    distribution_doc_request: string;

    @Property()
    distribution_doc_receive_date: string;

    @Property()
    distribution_approve_date: string;

    @Property()
    distribution_approve_doc: string;

    @Property()
    distribution_summary_stock_select: string;

    /**
     *
     * @param skipFields
     */
    public display(skipFields = []) {
        return super.display(skipFields, [], this);
    }
}
