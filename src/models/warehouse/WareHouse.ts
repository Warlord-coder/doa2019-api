import {Property} from "@tsed/common";
import {Model, Schema} from "@tsed/mongoose";
import * as mongoose from "mongoose";
import {PlantRegister} from "../plant-register/PlantRegister";
import {Recovery} from "../recovery/Recovery";
import {Lab} from "../lab/Lab";

@Model({
    schemaOptions: {
        strict: false
    }
})
export class WareHouse extends PlantRegister {
    @Property()
    wh_status: string;

    @Property()
    gs_no: string;

    @Property()
    wh_status_alert: boolean;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "PlantType"
    })
    plant_gene_name: string;
    
    @Property()
    isDistribution: boolean;

    @Property()
    room5: object[];

    @Property()
    anteArray: object[];

    @Property()
    room10: object[];

    @Property()
    room130: object[];

    @Property()
    wh_sample_ref_seed_check_box: string;

    @Property()
    wh_sample_ref_molecular_checkbox: string;

    @Property()
    selectedBatchRoom: string;

    @Property()	
    @Schema({	
        type: mongoose.Schema.Types.ObjectId,	
        ref: "Lab"	
    })	
    labId: string;

    @Property()
    wh_create_info_checkbox: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "LabAlertForm"
    })
    register_alert_form: string;

    @Property()
    submitTo: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "PlantCharacter"
    })
    reg_plant_sci_name: string;

    @Property()	
    reg_plant_alter_name: any;

    @Property()	
    m4_status: boolean;

    @Property()
    wh_sample_ref_molecular_file: string;

    @Property()
    wh_sample_ref_seed_file: string;

    @Property()
    labAlert: boolean;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "Lab"
    })
    labAlertId: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "Recovery"
    })
    recovery_id: string;

    @Property()
    recovery_status: string;

    @Property()
    roomRadio: string;

    @Property()
    breed_owner: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "Volume"
    })
    m3_recovery_assign: string;

    @Property()
    area_information: string;

    @Property()
    other_information: string;

    @Property()
    description_file: string;

    @Property()
    note: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "PlantRegister"
    })
    register_plant: string;

    @Property()
    recovery_sent_no: string;

    @Property()
    recovery_sent_date: string;

    @Property()
    recovery_amount: string;

    @Property()
    recovery_amount_dropdown: string;

    @Property()
    recovery_place: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "CommonProvince"
    })
    recovery_province: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "CommonProvince"
    })
    source_province: string;
    
    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "CommonAumper"
    })
    source_district: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "CommonAumper"
    })
    recovery_district: string;

    @Property()
    recovery_sub_district: string;

    @Property()
    recovery_address: string;

    @Property()
    recovery_lat: string;

    @Property()
    recovery_long: string;

    @Property()
    recovery_sea_level: string;

    @Property()
    recovery_rec_recover_date: string;

    @Property()
    recovery_rec_recover_collect_date: string;

    @Property()
    recovery_rec_recover_collect_person: string;

    @Property()
    recovery_rec_date: string;

    @Property()
    recovery_rec_no: string;

    @Property()
    recovery_rec_co_person: string;

    @Property()
    recovery_rec_detail: string;

    @Property()
    recovery_rec_method: string;

    @Property()
    recovery_rec_gene_check: string;

    @Property()
    recovery_rec_remark: string;

    @Property()
    recovery_test_growth_percent: string;

    @Property()
    recovery_seed_amount: string;

    @Property()
    attribute: object;
    
    @Property()
    m9warehouse_plant_submittedAt: Date;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    })
    m9warehouse_plant_submittedBy: string;
    
    @Property()
    m4recovery_plant_createdAt: Date;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    })
    m4recovery_plant_creatdBy: string;
    
    @Property()
    m4recovery_plant_updatedAt: Date;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    })
    m4recovery_plant_updatedBy: string;
    
    @Property()
    m4recovery_plant_submittedAt: Date;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    })
    m4recovery_plant_submittedBy: string;

    @Property()
    isPlantCancel: boolean;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "Lab"
    })
    m8_lab_id: string;

    @Property()
    @Schema({
        type: mongoose.Schema.Types.ObjectId,
        ref: "Distribution"
    })
    m5_assigned: string;

    /**
     *
     * @param skipFields
     */
    public display(skipFields = [], ref) {
        return super.display(skipFields, ref || this);
    }
}
