import {Property} from "@tsed/common";
import {Indexed, Model, Schema} from "@tsed/mongoose";
import {BaseModel} from "../Base";
import AppConstants from "../../utils/constants";

@Model()
export class CommonUnit extends BaseModel {
    @Property()
    @Indexed()
    @Schema({
        enum: [AppConstants.STATUS.ACTIVE,
            AppConstants.STATUS.INACTIVE]
    })
    status: string;

    @Property()
    unitName_en: string;


    @Property()
    unitName_th: string;

    /**
     *
     * @param skipFields
     */
    public display(skipFields = []) {
        return super.display(skipFields, [], this);
    }
}
