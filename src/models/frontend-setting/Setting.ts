import {Property} from "@tsed/common";
import {Indexed, Model, Schema} from "@tsed/mongoose";
import {BaseModel} from "../Base";
import AppConstants from "../../utils/constants";

@Model()
export class Setting extends BaseModel {
    @Property()
    topic: string;

    @Property()
    desc: string;

    @Property()
    imageFile: string;

    @Property()
    @Indexed()
    @Schema({
        enum: [AppConstants.STATUS.ACTIVE,
            AppConstants.STATUS.INACTIVE]
    })
    status: string;

    @Property()
    address: string;

    @Property()
    email: string;

    @Property()
    tel: string;

    @Property()
    description: string;

    /**
     *
     * @param skipFields
     */
    public display(skipFields = []) {
        return super.display(skipFields, [], this);
    }
}
