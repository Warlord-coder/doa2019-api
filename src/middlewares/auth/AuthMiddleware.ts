import {
    AuthenticatedMiddleware,
    EndpointInfo,
    EndpointMetadata,
    Next,
    OverrideMiddleware,
    Req,
    Res
} from "@tsed/common";
import AppConstants from "../../utils/constants";
import JWTService from "../../services/jwt/JWTService";
import {UserService} from "../../services/users/UsersService";
import {difference} from "lodash";
import {SeedUsersService} from "../../services/seed-users/UsersService";

@OverrideMiddleware(AuthenticatedMiddleware)
export class AuthMiddleware {
    constructor(private userService: UserService,
                private seedService: SeedUsersService) {
    }

    private _isContainsSpecificRole({role}, requestedRoles: any) {
        if (!requestedRoles.includes(role)) {
            throw new Error(AppConstants.ERROR_MESSAGES.ROLE);
        }

        return true;
    }

    private async _isAuthenticated(request: Express.Request | any, response: Express.Response, options: any) {
        const {headers: {authorization = ""} = {}, query: {token = null} = {}} = request;
        if (!authorization && !token && options === false) {
            return;
        }

        request.headers.authorization = authorization || token;

        return new Promise((resolve, reject) => {
            JWTService.verify(request, response, async (err) => {
                if (err) return reject(err);
                try {
                    const {user: {id = null} = {}} = request;
                    // @ts-ignore
                    const user = await this[!options.isSeed ? "userService" : "seedService"].findUser(id);
                    if (Array.isArray(options) && options.length) {
                        this._isContainsSpecificRole(user, options);
                    }
                    request.user = user;

                    return resolve();
                } catch (err) {
                    reject(err);
                }
            });
        });
    }

    async use(@EndpointInfo() endpoint: EndpointMetadata,
              @Req() request: Express.Request,
              @Next() next: Express.NextFunction,
              @Res() response: Express.Response) {


        const options = endpoint.store.get(AuthenticatedMiddleware);

        try {
            await this._isAuthenticated(request, response, options);
            next();
        } catch (err) {
            return next(err);
        }
    }
}
