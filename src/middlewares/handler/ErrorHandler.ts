import * as Express from "express";
import {Err, GlobalErrorHandlerMiddleware, OverrideMiddleware, Req, Res} from "@tsed/common";
import {Exception} from "ts-httpexceptions";

@OverrideMiddleware(GlobalErrorHandlerMiddleware)
export class ErrorHandler {

    use(@Err() error: any,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        const {message, status = 403, errors: [{message: errMessage = ""} = {}] = []} = error;

        if (error instanceof Exception || error.status) {
            request.log.error({
                error: {
                    message: error.message,
                    stack: error.stack,
                    status: error.status,
                    origin: error.origin
                }
            });
        }

        response.status(status).send({
            success: false,
            message: errMessage || message,
            statusCode: status
        });

        return;
    }
}
