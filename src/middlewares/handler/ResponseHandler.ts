import {ConverterService, OverrideMiddleware, ResponseData, SendResponseMiddleware, Response} from "@tsed/common";

@OverrideMiddleware(SendResponseMiddleware)
export class ResponseHandler extends SendResponseMiddleware {

    constructor(protected converterService: ConverterService) {
        super(converterService);
    }

    public use(@ResponseData() result: any, @Response() response: Express.Response | any) {
        const {statusCode = 403, status} = result;

        return response.status(status || statusCode).json(result);
    }
}
