// @ts-ignore
import {UseBefore} from "@tsed/common";
import UploadMiddleware from "./UploadMiddleware";
import {Store} from "@tsed/core";

export function Upload(options: any) {
    return Store.decorate((store) => {
        store.set(UploadMiddleware, options);

        return UseBefore(UploadMiddleware);
    });
}
