import {
    EndpointInfo,
    EndpointMetadata,
    IMiddleware,
    Middleware,
    Next,
    Request,
    Res
} from "@tsed/common";
import {any} from "bluebird";
import {includes} from "lodash";
import * as multer from "multer";
import * as mkdirp from "mkdirp";

const storage = multer.diskStorage({
    destination(req, file, cb) {
        const {options = {}} = req;
        const {path = "profile-pictures"} = options;
        const fileDirectory = `${__dirname}/../../../uploads/${path}`;
        mkdirp.sync(fileDirectory);
        cb(null, fileDirectory);
    },
    filename(req, file, cb) {
        const {originalname} = file;
        cb(null, `${Date.now()}-${originalname}`);
    }
});
const upload = multer({storage});


@Middleware()
export default class UploadMiddleware implements IMiddleware {
    async use(@EndpointInfo() endpoint: EndpointMetadata,
              @Request() request,
              @Res() response: Express.Response,
              @Next() next: Express.NextFunction) {
        const options = endpoint.store.get(UploadMiddleware);
        const {key = "file", array = false} = options || {};
        request.options = options;
        if (array) {
            upload.array("file", 15)(request, response, next);

            return;
        }
        upload.single(key || "file")(request, response, next);

        return;
    }
}

