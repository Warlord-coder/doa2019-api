import {
    Authenticated,
    BodyParams,
    Controller, Delete,
    Get,
    MergeParams,
    PathParams,
    Post,
    Put,
    Req,
    Required,
    Res,
    Status
} from "@tsed/common";
import * as Express from "express";
import {isNil} from "lodash";

import BaseController from "../Base/BaseCtrl";
import {PlantCategoryService} from "../../services/plant-category/PlantCategoryService";
import {EventNewsService} from "../../services/event-news/EventNewsService";
import {Upload} from "../../middlewares/upload/UploadDecorator";
import AppConstants from "../../utils/constants";


@Controller("/news")
@MergeParams(true)
export class EventNewsController extends BaseController {
    constructor(private eventNewsService: EventNewsService) {
        super();
    }


    /**
     *
     * @param request
     * @param response
     */
    @Get("/list")
    @Status(200, {description: "Get a list of event news"})
    async list(@Req() request: Express.Request,
               @Res() response: Express.Response) {
        try {
            const {query: {hideDeletedEntries = false} = {}} = request;
            const where: any = {};
            if (hideDeletedEntries) {
                where.isDeleted = null;
            }
            const categoriesInfo: any = await this.eventNewsService.list(where);

            return this.successRes(categoriesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Get("/list-frontend")
    @Status(200, {description: "Get a list of event news"})
    async listNewsFrontend(@Req() request: Express.Request,
                           @Res() response: Express.Response) {
        try {
            const {query: {hideDeletedEntries = false} = {}} = request;
            const where: any = {status: AppConstants.STATUS.ACTIVE};
            if (hideDeletedEntries) {
                where.isDeleted = null;
            }
            const [news, flagNews]: any = await Promise.all([this.eventNewsService.list(where), this.eventNewsService.getLatestFlagNews(where)]);

            return this.successRes({news, flagNews});

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param id
     * @param request
     * @param response
     */
    @Get("/:id")
    @Status(200, {description: "Get a specific event news"})
    async get(
        @PathParams("id") id: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.eventNewsService.getSpecificEventNews(id);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param id
     * @param request
     * @param response
     */
    @Get("/frontend/:id")
    @Status(200, {description: "Get a specific event news"})
    async getNewsFrontend(
        @PathParams("id") id: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const [newsDetails = {}, relatedNews = []]: any = await Promise.all([this.eventNewsService.getSpecificEventNews(id), this.eventNewsService.list({
                status: AppConstants.STATUS.ACTIVE,
                _id: {$ne: id}
            }, {
                pageNo: 0,
                pageSize: 3
            })]);

            return this.successRes({
                newsDetails,
                relatedNews
            });

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Post("/create")
    @Upload({path: "news-files", array: true})
    @Authenticated()
    @Status(200, {description: "Create a new category"})
    async create(@Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {files = [], body = {}, user: {_id: createdBy = null} = {},} = request;
            const {isImageFile, isAttachFile} = body;

            if (isImageFile && files.length) {
                request.body["imageFile"] = files[0].filename;
            }
            if (isAttachFile && files.length) {
                request.body["attachFile"] = files[1] ? files[1].filename : files[0].filename;
            }
            const dbRolesInfo: any = await this.eventNewsService.createEventNews({
                ...request.body,
                createdBy
            });

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Put("/:id")
    @Upload({path: "news-files", array: true})
    @Authenticated()
    @Status(200, {description: "Update a existing category"})
    async update(@PathParams("id") id: string,
                 @Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {files = [], body = {}, user: {_id: updatedBy = null} = {}} = request;
            const {isImageFile, isAttachFile} = body;

            if (isImageFile && files.length) {
                request.body["imageFile"] = files[0].filename;
            }
            if (isAttachFile && files.length) {
                request.body["attachFile"] = files[1] ? files[1].filename : files[0].filename;
            }
            const dbRolesInfo: any = await this.eventNewsService.updateEventNews({
                ...request.body,
                updatedAt: Date.now(),
                updatedBy
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Delete("/:id")
    @Authenticated()
    @Status(200, {description: "Delete a existing Plant Category"})
    async delete(@Req() request: Express.Request | any,
                 @Res() response: Express.Response,
                 @PathParams("id") id: string) {
        try {
            const {user: {_id: deletedBy = null} = {}} = request;
            const dbRolesInfo: any = await this.eventNewsService.updateEventNews({
                isDeleted: true,
                deletedBy,
                deletedAt: Date.now()
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }
}

