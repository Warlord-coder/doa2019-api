import {Authenticated, BodyParams, Controller, Get, MergeParams, Post, Req, Required, Res, Status} from "@tsed/common";
import * as Express from "express";
import * as json2xls from "json2xls";
import * as moment from "moment";

import BaseController from "../Base/BaseCtrl";
import {AccessmentService} from "../../services/accessment/AccessmentService";


@Controller("/accessement")
@MergeParams(true)
export class AccessementController extends BaseController {
    constructor(private accessementService: AccessmentService) {
        super();
    }


    /**
     *
     * @param request
     * @param response
     */
    @Get("/list")
    @Authenticated()
    @Status(200, {description: "Get a list of accessement"})
    async list(@Req() request: Express.Request,
               @Res() response: Express.Response | any) {
        try {
            const {query = {}} = request;
            const {excelImport = false, rating = "", toDate = "", fromDate = ""} = query;
            const where: any = {};

            let assementInfo: any = await this.accessementService.list(where);

            if (!excelImport) {
                return this.successRes(assementInfo);
            } else {
                assementInfo = this.accessementService.getFilterItems(assementInfo, rating, toDate, fromDate);
                const jsonVal = assementInfo.map((info, index) => {
                    const {rating, comment, createdAt} = info;

                    return {
                        "No": `${index + 1}`,
                        "Rating": rating,
                        "Comment": comment,
                        "Sent Date": moment(createdAt).format("DD/MM/YYYY")
                    };
                });

                return response.xls("Assessment-report.xlsx", jsonVal);
            }

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Post("/create")
    @Status(200, {description: "Create a new accessement"})
    async create(@Req() request: Express.Request | any, @Res() response: Express.Response) {
        try {
            const {body = {}} = request;
            const dbRolesInfo: any = await this.accessementService.createAccessement({
                ...body
            });

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }
}

