import {
    Authenticated,
    BodyParams,
    Controller, Delete,
    Get,
    MergeParams,
    PathParams,
    Post,
    Put,
    Req,
    Required,
    Res,
    Status
} from "@tsed/common";
import * as Express from "express";
import {isNil} from "lodash";

import BaseController from "../Base/BaseCtrl";
import {CommonProvinceService} from "../../services/common-province/CommonProvinceService";
import {CommonAumperService} from "../../services/common-aumper/CommonAumperService";


@Controller("/common-aumper")
@MergeParams(true)
export class CommonAumperController extends BaseController {
    constructor(private commonAumperService: CommonAumperService) {
        super();
    }


    /**
     *
     * @param request
     * @param response
     */
    @Get("/list")
    @Status(200, {description: "Get a list of province"})
    async list(@Req() request: Express.Request,
               @Res() response: Express.Response) {
        try {
            const {query: {hideDeletedEntries = false} = {}} = request;
            const where: any = {};
            if (hideDeletedEntries) {
                where.isDeleted = null;
            }

            const groupsInfo: any = await this.commonAumperService.list(where);

            return this.successRes(groupsInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param id
     * @param request
     * @param response
     */
    @Get("/:id")
    @Status(200, {description: "Get a specific province"})
    async get(
        @PathParams("id") id: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.commonAumperService.getSpecificAumper(id);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param status
     * @param provinceName_en
     * @param provinceName_th
     * @param rolesInfo
     * @param request
     * @param response
     */
    @Post("/create")
    @Status(200, {description: "Create a new province"})
    async create(@Required() @BodyParams("status") status: string,
                 @Required() @BodyParams("aumperName_en") aumperName_en: string,
                 @Required() @BodyParams("aumperName_th") aumperName_th: object,
                 @Req() request: Express.Request,
                 @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.commonAumperService.createAumper({
                status,
                aumperName_en,
                aumperName_th,
                // createdBy,
            });

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param status
     * @param provinceName_en
     * @param provinceName_th
     * @param id
     * @param request
     * @param response
     */
    @Put("/:id")
    @Status(200, {description: "Update a existing province"})
    async update(@Required() @BodyParams("status") status: string,
                 @Required() @BodyParams("aumperName_en") aumperName_en: object,
                 @Required() @BodyParams("aumperName_th") aumperName_th: object,
                 @PathParams("id") id: string,
                 @Req() request: Express.Request,
                 @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.commonAumperService.updateAumper({
                status,
                aumperName_en,
                aumperName_th,
                // updatedBy:
                updatedAt: Date.now()
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Delete("/:id")
    @Authenticated()
    @Status(200, {description: "Delete a existing Plant Category"})
    async delete(@Req() request: Express.Request | any,
                 @Res() response: Express.Response,
                 @PathParams("id") id: string) {
        try {
            const {user: {_id: deletedBy = null} = {}} = request;
            const dbRolesInfo: any = await this.commonAumperService.updateAumper({
                isDeleted: true
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }
}

