import {
    Authenticated,
    BodyParams,
    Controller, Delete,
    Get,
    MergeParams,
    PathParams,
    Post,
    Put,
    Req,
    Required,
    Res,
    Status
} from "@tsed/common";
import * as Express from "express";
import {isNil} from "lodash";

import BaseController from "../Base/BaseCtrl";
import {PassportRegisterService} from "../../services/passport-register/PassportRegister";
import {Upload} from "../../middlewares/upload/UploadDecorator";
import {VolumeService} from "../../services/volume/VolumeService";


@Controller("/passports")
@MergeParams(true)
export class PlantCharacterController extends BaseController {
    constructor(private passportRegisterService: PassportRegisterService) {
        super();
    }

    /**
     *
     * @param request
     * @param response
     */
    @Get("/list")
    @Authenticated()
    @Status(200, {description: "Get a list of passports"})
    async list(@Req() request: Express.Request | any,
               @Res() response: Express.Response) {
        try {
            const {query: {status = "draft"} = {}} = request;
            const passportList: any = await this.passportRegisterService.list(status);

            return this.successRes(passportList);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

     /**
     *
     * @param request
     * @param response
     */
    @Get("/export-excel")
    @Authenticated()
    @Status(200, {description: "Get a list of excel results"})
    async exportExcel(@Req() request: Express.Request | any,
               @Res() response: Express.Response) {
        try {
            
            const {query: {startDt = "2019-01-01"} = {}} = request;
            const {query: {endDt = "2029-12-31"} = {}} = request;

            const registers:any = await this.passportRegisterService.exportExcel(startDt, endDt);

            const result = {result: registers};

            return this.successRes(result);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param id
     * @param request
     * @param response
     */
    @Get("/:id")
    @Authenticated()
    @Status(200, {description: "Get a specific passport"})
    async get(
        @PathParams("id") id: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.passportRegisterService.getSpecificPassport(id);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param reg_date
     * @param reg_seed_receive
     * @param reg_seed_sent_no
     * @param request
     * @param response
     */
    @Post("/create")
    @Upload({path: "passport-reg-files"})
    @Authenticated()
    @Status(200, {description: "Create a new passport"})
    async create(@Required() @BodyParams("reg_date") reg_date: string,
                 @Required() @BodyParams("reg_seed_receive") reg_seed_receive: string,
                 @BodyParams("reg_seed_sent_no") reg_seed_sent_no: string,
                 @Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {file: {filename: reg_attach_file = undefined} = {}, user: {_id: createdBy = null} = {}} = request;
            const dbRolesInfo: any = await this.passportRegisterService.createPassport({
                reg_date,
                reg_seed_receive,
                reg_seed_sent_no,
                createdBy,
                reg_attach_file
            });

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param reg_date
     * @param reg_seed_receive
     * @param reg_seed_sent_no
     * @param id
     * @param request
     * @param response
     */
    @Put("/:id")
    @Upload({path: "passport-reg-files"})
    @Authenticated()
    @Status(200, {description: "Update a existing passport"})
    async update(@PathParams("id") id: string,
                 @Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {body = {}, user: {_id: updatedBy = null} = {}} = request;
            const dbRolesInfo: any = await this.passportRegisterService.updatePassport({
                ...body,
                updatedBy,
                updatedAt: Date.now()
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     * @param id
     */
    @Delete("/:id")
    @Authenticated()
    @Status(200, {description: "Delete a existing passport"})
    async delete(@Req() request: Express.Request | any,
                 @Res() response: Express.Response,
                 @PathParams("id") id: string) {
        try {
            const {user: {_id: deletedBy = null} = {}} = request;
            const dbRolesInfo: any = await this.passportRegisterService.updatePassport({
                deletedBy,
                deletedAt: Date.now()
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }
}

