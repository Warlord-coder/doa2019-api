import {
    Authenticated,
    BodyParams,
    Controller,
    Get,
    MergeParams,
    Next, PathParams,
    Post, Put,
    Req,
    Required,
    Res,
    Status
} from "@tsed/common";
import * as Express from "express";
import * as passport from "passport";
import * as multer from "multer";

import BaseController from "../Base/BaseCtrl";
import {IResponse} from "../../interfaces/IResponse";
import AppConstants from "../../utils/constants";
import {SeedUsersService} from "../../services/seed-users/UsersService";
import PassportSeedService from "../../services/passportSeedUsers/PassportSeedService";
import {Upload} from "../../middlewares/upload/UploadDecorator";

const upload = multer();

@Controller("/seed-users")
@MergeParams(true)
export class SeedUsersController extends BaseController {
    constructor(private seedUserService: SeedUsersService,
                private passportService: PassportSeedService) {
        super();
    }

    private _passportSetup = () => {
        PassportSeedService.setup(this.passportService);
    };

    /**
     *
     * @param email
     * @param password
     * @param request
     * @param response
     * @param next
     */
    @Post("/register")
    @Status(200, {description: "Create User from admin management"})
    async create(@Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {body = {}} = request;
            const user: any = await this.seedUserService.createUser({
                ...body,
                createdAt: Date.now(),
                updatedAt: Date.now()
            });

            return this.successRes(user.display());

        } catch (error) {
            return this.handleError(error.message || error, 402);
        }
    }

    /**
     *
     * @param email
     * @param password
     * @param request
     * @param response
     * @param next
     */
    @Post("/login")
    @Status(200, {description: "User logged in and get token with user information"})
    async login(@Required() @BodyParams("email") email: string,
                @Required() @BodyParams("password") password: string,
                @Req() request: Express.Request,
                @Res() response: Express.Response,
                @Next() next: Express.NextFunction) {
        this._passportSetup();

        return passport.authenticate("local", async (err, user: any, info: string) => {
            try {
                const error = err || info;
                if (error || !user) {
                    const {message} = error;
                    throw new Error(message || error || AppConstants.ERROR_MESSAGES.SOMETHING_WENT_WRONG);
                }
                const {authToken, lastLogin} = user;

                return response.json({
                    success: true,
                    status: 200,
                    data: {
                        authToken,
                        tokenExpiry: AppConstants.TOKEN_EXPIRY, ...user.display(),
                        lastLogin
                    }
                });
            } catch ({message}) {
                return response.status(401).json({success: false, status: 401, message});
            }
        })(request, response, null);
    }

    /**
     *
     * @param email
     * @param password
     * @param request
     * @param response
     * @param next
     */
    @Post("/resetPassword")
    @Status(200, {description: "User logged in and get token with user information"})
    async resetPassword(
                @Req() request: Express.Request,
                @Res() response: Express.Response,
                @Next() next: Express.NextFunction) {

        const {body = {}} = request;
        const user: any = await this.seedUserService.resetPassword({
            ...body,
            updatedAt: Date.now()
        });
        return response.json({
            success: true,
            status: 200,
            data: {}
        });
    }

    /**
     *
     * @param request
     */
    @Get("/me")
    @Authenticated({isSeed: true})
    @Status(200, {description: "Get Current Logged In User information"})
    public async currentUserInfo(@Req() request: Express.Request | any): Promise<IResponse> {
        try {
            const {user = {}} = request;
            const {id = ""} = user;
            const userInfo = await this.seedUserService.findUser(id);

            return this.successRes(userInfo.display(), 200);
        } catch ({message}) {
            return this.handleError(message, 401);
        }
    }

    @Get("/list")
    @Authenticated()
    @Status(200, {description: "List all users information"})
    public async list(@Req() request: Express.Request | any): Promise<IResponse> {
        try {
            const users = await this.seedUserService.findAllUsers();

            return this.successRes(users, 200);
        } catch ({message}) {
            return this.handleError(message, 401);
        }
    }

    /**
     *
     * @param request
     */
    @Get("/:id")
    @Authenticated()
    @Status(200, {description: "Get Current Logged In User information"})
    public async getSpecific(@Req() request: Express.Request | any,
                             @PathParams("id") id: string): Promise<IResponse> {
        try {
            const userInfo = await this.seedUserService.findUser(id);

            return this.successRes(userInfo.display(), 200);
        } catch ({message}) {
            return this.handleError(message, 401);
        }
    }

    /**
     *
     * @param request
     */
    @Put("/:id")
    @Authenticated()
    @Status(200, {description: "Get Current Logged In User information"})
    public async updateSpecific(@Req() request: Express.Request | any,
                                @PathParams("id") id: string): Promise<IResponse> {
        try {
            const {user: {_id = ""} = {}} = request;
            const userInfo = await this.seedUserService.findAndUpdate(id, {
                ...request.body,
                updatedBy: _id,
                updatedAt: Date.now()
            });

            return this.successRes(userInfo.display(), 200);
        } catch ({message}) {
            return this.handleError(message, 401);
        }
    }


}
