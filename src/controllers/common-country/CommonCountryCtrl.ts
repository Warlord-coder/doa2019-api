import {
    Authenticated,
    BodyParams,
    Controller, Delete,
    Get,
    MergeParams,
    PathParams,
    Post,
    Put,
    Req,
    Required,
    Res,
    Status
} from "@tsed/common";
import * as Express from "express";
import {isNil} from "lodash";

import BaseController from "../Base/BaseCtrl";
import {CommonUnitService} from "../../services/common-unit/CommonUnitService";
import {CommonCountryService} from "../../services/common-country/CommonCountryService";


@Controller("/common-country")
@MergeParams(true)
export class CommonCountryController extends BaseController {
    constructor(private commonCountryService: CommonCountryService) {
        super();
    }


    /**
     *
     * @param name
     * @param department_en
     * @param department_th
     * @param request
     * @param response
     */
    @Get("/list")
    @Status(200, {description: "Get a list of country"})
    async list(@Req() request: Express.Request,
               @Res() response: Express.Response) {
        try {
            const {query: {hideDeletedEntries = false} = {}} = request;
            const where: any = {};
            if (hideDeletedEntries) {
                where.isDeleted = null;
            }

            const groupsInfo: any = await this.commonCountryService.list(where);

            return this.successRes(groupsInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param name
     * @param department_en
     * @param department_th
     * @param rolesInfo
     * @param request
     * @param response
     */
    @Post("/search")
    @Status(200, {description: "Get a list of categories"})
    async search(
            @BodyParams("keyword") keyword: string,
            @Req() request: Express.Request,
            @Res() response: Express.Response) {
        try {
            
            const categoriesInfo: any = await this.commonCountryService.search(keyword);

            return this.successRes(categoriesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param id
     * @param request
     * @param response
     */
    @Get("/:id")
    @Status(200, {description: "Get a specific country"})
    async get(
        @PathParams("id") id: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.commonCountryService.getSpecificCountry(id);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param status
     * @param countryName_en
     * @param countryName_th
     * @param request
     * @param response
     */
    @Post("/create")
    @Status(200, {description: "Create a new country"})
    async create(@Required() @BodyParams("status") status: string,
                 @Required() @BodyParams("countryName_en") countryName_en: string,
                 @Required() @BodyParams("countryName_th") countryName_th: object,
                 @Req() request: Express.Request,
                 @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.commonCountryService.createCountry({
                status,
                countryName_en,
                countryName_th,
                // createdBy,
            });

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param status
     * @param countryName_en
     * @param countryName_th
     * @param id
     * @param request
     * @param response
     */
    @Put("/:id")
    @Status(200, {description: "Update a existing country"})
    async update(@Required() @BodyParams("status") status: string,
                 @Required() @BodyParams("countryName_en") countryName_en: object,
                 @Required() @BodyParams("countryName_th") countryName_th: object,
                 @PathParams("id") id: string,
                 @Req() request: Express.Request,
                 @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.commonCountryService.updateCountry({
                status,
                countryName_en,
                countryName_th,
                // updatedBy:
                updatedAt: Date.now()
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Delete("/:id")
    @Authenticated()
    @Status(200, {description: "Delete a existing Plant Category"})
    async delete(@Req() request: Express.Request | any,
                 @Res() response: Express.Response,
                 @PathParams("id") id: string) {
        try {
            const {user: {_id: deletedBy = null} = {}} = request;
            const dbRolesInfo: any = await this.commonCountryService.updateCountry({
                isDeleted: true
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }
}

