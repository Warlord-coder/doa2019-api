import {
    Authenticated,
    BodyParams,
    Controller, Delete,
    Get,
    MergeParams,
    PathParams,
    Post,
    Put,
    Req,
    Required,
    Res,
    Status
} from "@tsed/common";
import * as Express from "express";
import {isNil} from "lodash";

import BaseController from "../Base/BaseCtrl";
import {VolumeService} from "../../services/volume/VolumeService";
import {Utility} from "../../utils/constants";


@Controller("/volumes")
@MergeParams(true)
export class PlantCharacterController extends BaseController {
    constructor(private volumeService: VolumeService) {
        super();
    }

    @Get("/list/:passportId")
    @Authenticated()
    @Status(200, {description: "Get a list of volumes"})
    async list(@Req() request: Express.Request | any,
               @Res() response: Express.Response,
               @PathParams("passportId") passportId: string) {
        try {
            const {query: {filter = null} = {}} = request;
            const volumesList: any = await this.volumeService.list(passportId, filter);

            return this.successRes(volumesList);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }


    @Get("/:id")
    @Authenticated()
    @Status(200, {description: "Get a specific volumes"})
    async get(
        @PathParams("id") id: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.volumeService.getSpecificVolume(id);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Post("/create")
    @Authenticated()
    @Status(200, {description: "Create a new volume"})
    async create(@Required() @BodyParams("passport_no") passport_no: string,
                 @Required() @BodyParams("form_type") form_type: string,
                 @Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {user: {_id: createdBy = null} = {}} = request;

            const totalVolumes = await this.volumeService.countVolume();
            const dbRolesInfo: any = await this.volumeService.createVolume({
                passport_no,
                form_type,
                volume_no: String(Utility.getFormattedNumber(totalVolumes + 1)),
                createdBy
            });

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Put("/:id")
    @Authenticated()
    @Status(200, {description: "Update a existing volumes"})
    async update(@PathParams("id") id: string,
                 @Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {user: {_id: updatedBy = null} = {}, body = {}} = request;
            const dbRolesInfo: any = await this.volumeService.updateVolume({
                ...body,
                updatedBy,
                updatedAt: Date.now()
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Delete("/:id")
    @Status(200, {description: "Delete a existing volumes"})
    async delete(@Req() request: Express.Request | any,
                 @Res() response: Express.Response,
                 @PathParams("id") id: string) {
        try {
            const {user: {_id: deletedBy = null} = {}} = request;
            const dbRolesInfo: any = await this.volumeService.updateVolume({
                deletedBy,
                deletedAt: Date.now()
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }
}


