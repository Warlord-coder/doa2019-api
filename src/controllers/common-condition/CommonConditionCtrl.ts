import {
    Authenticated,
    BodyParams,
    Controller, Delete,
    Get,
    MergeParams,
    PathParams,
    Post,
    Put,
    Req,
    Required,
    Res,
    Status
} from "@tsed/common";
import * as Express from "express";
import {isNil} from "lodash";

import BaseController from "../Base/BaseCtrl";
import {CommonConditionService} from "../../services/common-condition/CommonConditionService";


@Controller("/common-condition")
@MergeParams(true)
export class CommonConditionController extends BaseController {
    constructor(private commonConditionService: CommonConditionService) {
        super();
    }


    /**
     *
     * @param name
     * @param department_en
     * @param department_th
     * @param request
     * @param response
     */
    @Get("/list")
    @Status(200, {description: "Get a list of condition"})
    async list(@Req() request: Express.Request,
               @Res() response: Express.Response) {
        try {
            const {query: {hideDeletedEntries = false} = {}} = request;
            const where: any = {};
            if (hideDeletedEntries) {
                where.isDeleted = null;
            }

            const groupsInfo: any = await this.commonConditionService.list(where);

            return this.successRes(groupsInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param id
     * @param request
     * @param response
     */
    @Get("/:id")
    @Status(200, {description: "Get a specific condition"})
    async get(
        @PathParams("id") id: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.commonConditionService.getSpecificCondition(id);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param status
     * @param name
     * @param type
     * @param value
     * @param conditionAt
     * @param request
     * @param response
     */
    @Post("/create")
    @Status(200, {description: "Create a new condition"})
    async create(@Required() @BodyParams("status") status: string,
                 @Required() @BodyParams("name") name: string,
                 @Required() @BodyParams("type") type: object,
                 @Required() @BodyParams("value") value: object,
                 @Required() @BodyParams("conditionAt") conditionAt: object,
                 @Req() request: Express.Request,
                 @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.commonConditionService.createCondition({
                status,
                name,
                type,
                value,
                conditionAt,
                // createdBy,
            });

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param status
     * @param name
     * @param type
     * @param value
     * @param conditionAt
     * @param id
     * @param request
     * @param response
     */
    @Put("/:id")
    @Status(200, {description: "Update a existing condition"})
    async update(@Required() @BodyParams("status") status: string,
                 @Required() @BodyParams("name") name: string,
                 @Required() @BodyParams("type") type: string,
                 @Required() @BodyParams("value") value: string,
                 @Required() @BodyParams("conditionAt") conditionAt: string,
                 @PathParams("id") id: string,
                 @Req() request: Express.Request,
                 @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.commonConditionService.updateCondition({
                status,
                name,
                type,
                value,
                conditionAt,
                // updatedBy:
                updatedAt: Date.now()
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Delete("/:id")
    @Authenticated()
    @Status(200, {description: "Delete a existing Plant Category"})
    async delete(@Req() request: Express.Request | any,
                 @Res() response: Express.Response,
                 @PathParams("id") id: string) {
        try {
            const {user: {_id: deletedBy = null} = {}} = request;
            const dbRolesInfo: any = await this.commonConditionService.updateCondition({
                isDeleted: true
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }
}

