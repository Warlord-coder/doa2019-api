import {Controller, Get, MergeParams, Post, Req, Res, Status} from "@tsed/common";
import * as Express from "express";
import {isNil} from "lodash";
import * as multer from "multer";

import BaseController from "../Base/BaseCtrl";

import PassportService from "../../services/passport/PassportService";
import {SeedUsersService} from "../../services/seed-users/UsersService";
import {SeedSearchService} from "../../services/seed-search/SeedSearchService";

@Controller("/seed-search")
@MergeParams(true)
export class SeedUsersController extends BaseController {
    constructor(private seedSearchService: SeedSearchService) {
        super();
    }


    @Post("/simple")
    @Status(200, {description: "Simple search for frontend"})
    async simple(@Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {body = {}} = request;
            const {searchKeyword, searchCriteria} = body;

            if (!searchKeyword || !searchCriteria) {
                return this.successRes([]);
            }

            const result = await this.seedSearchService.simpleSearch(body);

            return this.successRes(result);

        } catch (error) {
            return this.handleError(error.message || error, 400);
        }
    }


    @Post("/advance")
    @Status(200, {description: "Advance Search for Frontend"})
    async create(@Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {body = {}} = request;
            const result = await this.seedSearchService.advanceSearch(body);

            return this.successRes(result);

        } catch (error) {
            return this.handleError(error.message || error, 400);
        }
    }

    @Get("/getAttributes")
    @Status(200, {description: "getAttributes "})
    async getAttributes(@Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const result = await this.seedSearchService.getAttributes();

            return this.successRes(result);

        } catch (error) {
            return this.handleError(error.message || error, 400);
        }
    }

    @Get("/autocompelete")
    @Status(200, {description: "Autocomplete "})
    async get(@Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const result = await this.seedSearchService.getAutocompleteInfo();

            return this.successRes(result);

        } catch (error) {
            return this.handleError(error.message || error, 400);
        }
    }


}

