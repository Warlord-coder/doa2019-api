import {
    Authenticated,
    BodyParams,
    Controller, Delete,
    Get,
    MergeParams,
    PathParams,
    Post,
    Put,
    Req,
    Required,
    Res,
    Status
} from "@tsed/common";
import * as Express from "express";
import {isNil} from "lodash";

import BaseController from "../Base/BaseCtrl";
import {Upload} from "../../middlewares/upload/UploadDecorator";
import {RecoverService} from "../../services/recover/RecoverService";
import {WareHouseService} from "../../services/ware-house/WareHouseService";


@Controller("/recovers")
@MergeParams(true)
export class PlantCharacterController extends BaseController {
    constructor(private recoverService: RecoverService,
                private wareHouseService: WareHouseService) {
        super();
    }

    /**
     *
     * @param request
     * @param response
     */
    @Get("/list")
    @Authenticated()
    @Status(200, {description: "Get a list of recovery"})
    async list(@Req() request: Express.Request,
               @Res() response: Express.Response) {
        try {
            const passportList: any = await this.recoverService.list();

            return this.successRes(passportList);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }


    /**
     *
     * @param request
     * @param response
     */
    @Get("/list-pending")
    @Authenticated()
    @Status(200, {description: "Get a list of recovery"})
    async recoverPlants(@Req() request: Express.Request,
                        @Res() response: Express.Response) {
        try {
            const [passportList, others]: any = await Promise.all([this.wareHouseService.list("m4"), this.wareHouseService.listAlert("m4")]);

            return this.successRes([...passportList, ...others]);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Get("/list-M4pending")
    @Authenticated()
    @Status(200, {description: "Get a list of recovery"})
    async recoverPlantsM4(@Req() request: Express.Request,
                        @Res() response: Express.Response) {
        try {
            const data: any = await this.wareHouseService.listM4('m2');
            let submitM4: any = await this.wareHouseService.listM4('m4');
            return this.successRes([...data, ...submitM4]);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Get("/list-complete-recovery-register")
    @Authenticated()
    @Status(200, {description: "Get a list of recovery"})
    async listCompleteRecoveryRegister(@Req() request: Express.Request,
                                       @Res() response: Express.Response) {
        try {
            const where: any = {
                recovery_status: "complete",
                recovery_id: {$ne: null},
                m3_recovery_assign: null
            };

            const [passportList, others]: any = await Promise.all([this.wareHouseService.list(null, where), this.wareHouseService.listAlert(null, where)]);

            return this.successRes([...passportList, ...others]);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }


    /**
     *
     * @param request
     * @param response
     */
    @Post("/submit-register-module/:id")
    @Authenticated()
    @Status(200, {description: "submit to register module"})
    async submitRegisterModule(
        @PathParams("id") id: string,
        @Required() @BodyParams("plantIds") plantIds: any,
        @Req() request: Express.Request | any,
        @Res() response: Express.Response) {
        try {
            const {user: {_id: createdBy = null} = {}} = request;
            const passportList: any = await this.recoverService.submitToRegisterModule(plantIds, id, createdBy);

            return this.successRes(passportList);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }


    /**
     *
     * @param request
     * @param response
     */
    @Post("/mark-complete/:id")
    @Authenticated()
    @Status(200, {description: "Get a list of recovery"})
    async markCompleteRecover(
        @PathParams("id") id: string,
        @Required() @BodyParams("plantIds") plantIds: any,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const passportList: any = await this.recoverService.submitToRecovery(plantIds, id, "complete", null);

            return this.successRes(passportList);

        } catch ({message}) {   
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param id
     * @param request
     * @param response
     */
    @Get("/plants/:id")
    @Authenticated()
    @Status(200, {description: "Get a specific recovery plants"})
    async getRecoverPlants(
        @PathParams("id") id: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const {query: {status = "draft"} = {}} = request;
            let dbRolesInfo: any = await this.recoverService.getRecoverycIdStatus(id, status);	
            if(status == 'pending'){	
                let approveInfo: any = await this.recoverService.getRecoverycIdStatus(id, 'approve');	
                let d  = [ ...dbRolesInfo, ...approveInfo];	
                dbRolesInfo = d;	
            }	

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Post("/submitToPending/:id")	
    @Authenticated()	
    @Status(200, {description: "Submit a plant to recovery house"})	
    async submitToRecoveryPending(@Required() @BodyParams("plantIds") plantIds: any,	
                                  @PathParams("id") id: string,	
                                  @Req() request: Express.Request | any,	
                                  @Res() response: Express.Response) {	
        try {	
            const {user: {_id: updatedBy = null} = {}} = request;	
            const dbRolesInfo: any = await this.recoverService.submitToRecovery(plantIds, id,'pending', null);	
            return this.successRes(dbRolesInfo);	
        } catch ({message}) {	
            return this.handleError(message, 400);	
        }	
    }

    @Post("/list-m4")	
    @Authenticated()	
    @Status(200, {description: "Get a list of recovery"})	
    async recoverAlert(@Req() request: Express.Request,	
                        @Res() response: Express.Response) {	
        try {	
            const status = request.query.status;	
            const plants = request.body.plantIds;
            const passportList: any = await this.wareHouseService.getRecoveryByStatus(status);	
            return this.successRes(passportList);	
        } catch ({message}) {	
            return this.handleError(message, 400);	
        }	
    }	

    @Post("/getByPlace")	
    @Authenticated()	
    @Status(200, {description: "Get a list of recovery"})	
    async getByPlace(
            @Required() @BodyParams("startDt") startDt: string,
            @Required() @BodyParams("endDt") endDt: string,
            @Req() request: Express.Request,	
            @Res() response: Express.Response) {	
        try {	

            const recoveryList: any = await this.recoverService.getByPlace({startDt: startDt, endDt: endDt});	
            return this.successRes(recoveryList);	
        } catch ({message}) {	
            return this.handleError(message, 400);	
        }	
    }
    
    @Post("/getByCategory")	
    @Authenticated()	
    @Status(200, {description: "Get a list of recovery"})	
    async getByCategory(
            @Required() @BodyParams("startDt") startDt: string,
            @Required() @BodyParams("endDt") endDt: string,
            @Req() request: Express.Request,	
            @Res() response: Express.Response) {	
        try {	

            const recoveryList: any = await this.recoverService.getByCategory({startDt: startDt, endDt: endDt});	
            return this.successRes(recoveryList);	
        } catch ({message}) {	
            return this.handleError(message, 400);	
        }	
    }

    /**
     *
     * @param id
     * @param request
     * @param response
     */
    @Get("/:id")
    @Authenticated()
    @Status(200, {description: "Get a specific recovery"})
    async get(
        @PathParams("id") id: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.recoverService.getSpecificRecovery(id);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param revival_plant
     * @param date_of_submission
     * @param place
     * @param request
     * @param response
     */
    @Post("/create")
    @Upload({path: "recovery-files"})
    @Authenticated()
    @Status(200, {description: "Create a new recovery"})
    async create(@BodyParams("revival_plant") revival_plant: string,
                 @Required() @BodyParams("date_of_submission") date_of_submission: string,
                 @Required() @BodyParams("place") place: string,
                 @Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {file: {filename: recover_file = undefined} = {}, user: {_id: createdBy = null} = {}} = request;
            const dbRolesInfo: any = await this.recoverService.createRecovery({
                revival_plant,
                date_of_submission,
                place,
                createdBy,
                recover_file
            });

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Post("/:id")
    @Authenticated()
    @Status(200, {description: "Submit a plant to recovery house"})
    async submitToRecoverySection(@Required() @BodyParams("plantIds") plantIds: any,
                                  @PathParams("id") id: string,
                                  @Req() request: Express.Request | any,
                                  @Res() response: Express.Response) {
        try {
            let status = (request.body.status ? request.body.status: null);	
            const dbRolesInfo: any = await this.recoverService.submitToRecovery(plantIds, id, status , request['user']._id);
            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }


    /**
     *
     * @param reg_date
     * @param reg_seed_receive
     * @param reg_seed_sent_no
     * @param id
     * @param request
     * @param response
     */
    @Put("/:id")
    @Upload({path: "recovery-reg-files"})
    @Authenticated()
    @Status(200, {description: "Update a existing recovery"})
    async update(@Required() @BodyParams("reg_date") reg_date: string,
                 @Required() @BodyParams("reg_seed_receive") reg_seed_receive: string,
                 @Required() @BodyParams("reg_seed_sent_no") reg_seed_sent_no: string,
                 @PathParams("id") id: string,
                 @Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {file: {filename: reg_attach_file = undefined} = {}, user: {_id: updatedBy = null} = {}} = request;
            const dbRolesInfo: any = await this.recoverService.updateRecovery({
                reg_date,
                reg_seed_receive,
                reg_seed_sent_no,
                reg_attach_file,
                updatedBy,
                updatedAt: Date.now()
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     * @param id
     */
    @Delete("/:id")
    @Authenticated()
    @Status(200, {description: "Delete a existing recovery"})
    async delete(@Req() request: Express.Request | any,
                 @Res() response: Express.Response,
                 @PathParams("id") id: string) {
        try {
            const {user: {_id: deletedBy = null} = {}} = request;
            const dbRolesInfo: any = await this.recoverService.updateRecovery({
                deletedBy,
                deletedAt: Date.now()
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

   
    	
    @Get("/list-alertM9/:id")	
    @Authenticated()	
    @Status(200, {description: "Get a specific labs alert form"})	
    async specificArray(@Req() request: Express.Request,	
                   @Res() response: Express.Response) {	
        try {	
            const {params: {id = ""} = {}} = request;	
            const passportList: any = await this.recoverService.getSpecificId(id);	
            return this.successRes(passportList);	
        } catch ({message}) {	
            return this.handleError(message, 400);	
        }	
    }	
    @Put("/plants/change-status")	
    @Authenticated()	
    @Status(200, {description: "Get a specific passport"})	
    async updatePlantStatus(	
        @Req() request: Express.Request | any,	
        @Res() response: Express.Response) {	
        try {	
            const {plants = [], status: distributionStatus} = request.body;	
            for await (const plant of plants) {	
                await this.recoverService.updateSpecificPlant(plant, {distributionStatus});	
            }	
            return this.successRes();	
        } catch ({message}) {	
            return this.handleError(message, 400);	
        }	
    }

    @Put("/setStock/:id")	
    @Authenticated()	
    @Status(200, {description: "Get a specific passport"})	
    async updatePlant(	
        @PathParams("id") id: string,	
        @Req() request: Express.Request | any,	
        @Res() response: Express.Response) {	
        try {	
            let room = request.body.batch;	
            if(room.search("-5") != -1){	
                let whsInfo:any = await this.wareHouseService.updateStockAlertM4(id, request.body,'room5');	
            }	
            if(room.search("-10") != -1){	
                let whsInfo:any = await this.wareHouseService.updateStockAlertM4(id, request.body,'room10');	
            }	
            return this.successRes([]);	
        } catch ({message}) {	
            return this.handleError(message, 400);	
        }	
    }

}

