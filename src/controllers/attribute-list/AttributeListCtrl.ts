import {
    Authenticated,
    BodyParams,
    Controller,
    Get,
    MergeParams,
    PathParams,
    Post,
    Put,
    Req,
    Required,
    Res,
    Status
} from "@tsed/common";
import * as Express from "express";
import {isNil} from "lodash";

import BaseController from "../Base/BaseCtrl";
import {AttributeListService} from "../../services/attribute-list/AttributeListService";


@Controller("/attribute-lists")
@MergeParams(true)
export class AttributeListCtrl extends BaseController {
    constructor(private attributeListService: AttributeListService) {
        super();
    }


    /**
     *
     * @param request
     * @param response
     */
    @Get("/list")
    @Authenticated()
    @Status(200, {description: "Get a list of attributes list"})
    async list(@Req() request: Express.Request,
               @Res() response: Express.Response) {
        try {
            const attributesList: any = await this.attributeListService.list();

            return this.successRes(attributesList);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Post("/filterList")
    @Status(200, {description: "Get a list of group types"})
    async getFilterList(
            @Required() @BodyParams("start") start: string,
            @Required() @BodyParams("length") length: string,
            @BodyParams("keyword") keyword: string,
            @Req() request: Express.Request,
            @Res() response: Express.Response) {
        try {
            const groupsInfo: any = await this.attributeListService.getFilterList({start: start, length: length, keyword: keyword});

            return this.successRes(groupsInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Get("/:id")
    @Authenticated()
    @Status(200, {description: "Get a specific attribute list"})
    async get(
        @PathParams("id") id: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.attributeListService.getSpecificAttributeList(id);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Post("/create")
    @Authenticated()
    @Status(200, {description: "Create a new country"})
    async create(@Required() @BodyParams("attributeList") attributeList: object,
                 @Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {user: {_id = null} = {}} = request;
            const dbRolesInfo: any = await this.attributeListService.createAttributeList({
                ...attributeList,
                createdBy: _id,
            });

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Put("/:id")
    @Status(200, {description: "Update a existing attribute  list"})
    async update(@Required() @BodyParams("attributeList") attributeList: object,
                 @PathParams("id") id: string,
                 @Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {user: {_id = null} = {}} = request;
            const dbRolesInfo: any = await this.attributeListService.updateAttributeList({
                ...attributeList,
                updatedBy: _id,
                updatedAt: Date.now()
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }
}

