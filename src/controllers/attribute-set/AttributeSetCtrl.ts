import {
    Authenticated,
    BodyParams,
    Controller,
    Get,
    MergeParams,
    PathParams,
    Post,
    Put,
    Req,
    Required,
    Res,
    Status
} from "@tsed/common";
import * as Express from "express";
import {isNil} from "lodash";

import BaseController from "../Base/BaseCtrl";
import {CommonCountryService} from "../../services/common-country/CommonCountryService";
import {AttributeSetService} from "../../services/attribute-set/AttributeSetService";


@Controller("/attribute-sets")
@MergeParams(true)
export class AttributeSetCtrl extends BaseController {
    constructor(private attributeSetService: AttributeSetService) {
        super();
    }


    /**
     *
     * @param request
     * @param response
     */
    @Get("/list")
    @Authenticated()
    @Status(200, {description: "Get a list of attributes"})
    async list(@Req() request: Express.Request,
               @Res() response: Express.Response) {
        try {
            const groupsInfo: any = await this.attributeSetService.list();

            return this.successRes(groupsInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Post("/filterList")
    @Status(200, {description: "Get a list of group types"})
    async getFilterList(
            @Required() @BodyParams("start") start: string,
            @Required() @BodyParams("length") length: string,
            @BodyParams("keyword") keyword: string,
            @Req() request: Express.Request,
            @Res() response: Express.Response) {
        try {
            const groupsInfo: any = await this.attributeSetService.getFilterList({start: start, length: length, keyword: keyword});

            return this.successRes(groupsInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param id
     * @param request
     * @param response
     */
    @Get("/:id")
    @Authenticated()
    @Status(200, {description: "Get a specific attribute"})
    async get(
        @PathParams("id") id: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.attributeSetService.getSpecificAttribute(id);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param attributeSet
     * @param request
     * @param response
     */
    @Post("/create")
    @Authenticated()
    @Status(200, {description: "Create a new country"})
    async create(@Required() @BodyParams("attributeSet") attributeSet: object,
                 @Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {user: {_id = null} = {}} = request;
            const dbRolesInfo: any = await this.attributeSetService.createAttribute({
                ...attributeSet,
                createdBy: _id,
            });

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param id
     * @param request
     * @param response
     */
    @Put("/:id")
    @Status(200, {description: "Update a existing attribute set"})
    async update(@Required() @BodyParams("attributeSet") attributeSet: object,
                 @PathParams("id") id: string,
                 @Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {user: {_id = null} = {}} = request;
            const dbRolesInfo: any = await this.attributeSetService.updateAttribute({
                ...attributeSet,
                updatedBy: _id,
                updatedAt: Date.now()
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }
}

