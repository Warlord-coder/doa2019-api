import {
    Authenticated,
    BodyParams,
    Controller,
    Delete,
    Get,
    MergeParams,
    PathParams,
    Post,
    Put,
    Req,
    Required,
    Res,
    Status
} from "@tsed/common";
import * as Express from "express";
import {isNil} from "lodash";

import BaseController from "../Base/BaseCtrl";
import {LabService} from "../../services/lab/LabService";
import {LabAlertFormService} from "../../services/LabAlert/LabAlertService";
import {WareHouseService} from "../../services/ware-house/WareHouseService";

@Controller("/lab-alert-forms")
@MergeParams(true)
export class LabAlertFormController extends BaseController {
    constructor(private labAlertFormService: LabAlertFormService,private whs:WareHouseService) {
        super();
    }

    @Get("/list-alert")	
    @Authenticated()	
    @Status(200, {description: "Get a list of labs alert  form"})	
    async listAlert(@Req() request: Express.Request,	
               @Res() response: Express.Response) {	
        try {	
            const {params: {}, query: {status = "pending"} = {}} = request;	
            let passportList: any = await this.labAlertFormService.listAlert( status);	
            if(status == 'pending'){	
                const passportListApprove: any = await this.labAlertFormService.listAlert('approve');	
                let d  = [ ...passportList, ...passportListApprove];	
                passportList = d;	
            }	
            	
            return this.successRes(passportList);	
        } catch ({message}) {	
            return this.handleError(message, 400);	
        }	
    }

    @Get("/getAll")	
    @Authenticated()	
    @Status(200, {description: "Get a list of labs alert  form"})	
    async getAll(@Req() request: Express.Request,	
               @Res() response: Express.Response) {	
        try {	
            let passportList: any = await this.labAlertFormService.listAll();	

            return this.successRes(passportList);	
        } catch ({message}) {	
            return this.handleError(message, 400);	
        }	
    }
    
    /**
     *
     * @param request
     * @param response
     */
    @Get("/list/:labId")	
    @Authenticated()	
    @Status(200, {description: "Get a list of labs alert  form"})	
    async list(@Req() request: Express.Request,	
               @Res() response: Express.Response) {	
        try {	
            const {params: {labId = ""} = {}, query: {status = "draft"} = {}} = request;	
            let passportList: any = await this.labAlertFormService.list(labId, status);	
            if(status == 'pending'){	
                const passportListApprove: any = await this.labAlertFormService.list(labId, 'approve');	
                let d  = [ ...passportList, ...passportListApprove];	
                passportList = d;	
            }	
            return this.successRes(passportList);	
        } catch ({message}) {	
            return this.handleError(message, 400);	
        }	
    }

    @Put("/:id")
    @Authenticated()
    @Status(200, {description: "submit a lab"})
    async update(@Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {body = {}, params: {id = null} = {}} = request;
            const dbRolesInfo: any = await this.labAlertFormService.updateAlertForm(body, id);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Get("/:id")
    @Authenticated()
    @Status(200, {description: "Get a specific labs alert form"})
    async specific(@Req() request: Express.Request,
                   @Res() response: Express.Response) {
        try {
            const {params: {id = ""} = {}} = request;
            const passportList: any = await this.labAlertFormService.specific(id);

            return this.successRes(passportList);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }
    
    @Post("/getGrowth")
    @Authenticated()
    @Status(200, {description: "submit a lab"})
    async getGrowth(@Required() @BodyParams("id") id: String,
                   @Req() request: Express.Request | any,
                   @Res() response: Express.Response) {
        try {
            const passportList: any = await this.labAlertFormService.specificGrowth(id);
            return this.successRes(passportList);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }


    @Post("/submit-m9")
    @Authenticated()
    @Status(200, {description: "submit a lab"})
    async submitM9(@Required() @BodyParams("plantIds") plant_ids: [],
                   @Req() request: Express.Request | any,
                   @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.labAlertFormService.submitIntoM9(plant_ids);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Post("/list-alertM9")	
    @Authenticated()	
    @Status(200, {description: "Get a list of labs alert  form"})	
    async listAlertM9(@Req() request: Express.Request,	
               @Res() response: Express.Response) {	
        try {	
            const {params: {}, query: {status = request.body.status} = {}} = request;	
            let passportList: any = await this.labAlertFormService.listAlertM9( status);	
            // if(status == 'pending'){	
            //     const passportListApprove: any = await this.labAlertFormService.listAlertM9('approve');	
            //     let d  = [ ...passportList, ...passportListApprove];	
            //     passportList = d;	
            // }	
            // let d = passportList.filter((item, index,arr)=>{	
            //     const c = arr.map(item=> item.labId.lab_no);	
            //     return  index === c.indexOf(item.labId.lab_no)	
            //   })	
            return this.successRes(passportList);	
        } catch ({message}) {	
            return this.handleError(message, 400);	
        }	
    }
    @Get("/list-alertM9/:id")	
    @Authenticated()	
    @Status(200, {description: "Get a specific labs alert form"})	
    async specificArray(@Req() request: Express.Request,	
                   @Res() response: Express.Response) {	
        try {	
            const {params: {id = ""} = {}} = request;	
            const passportList: any = await this.labAlertFormService.specificLabid(id);	
            return this.successRes(passportList);	
        } catch ({message}) {	
            return this.handleError(message, 400);	
        }	
    }	
	

    @Put("/setStock/:id")	
    @Authenticated()	
    @Status(200, {description: "Get a specific passport"})	
    async updatePlant(	
        @PathParams("id") id: string,	
        @Req() request: Express.Request | any,	
        @Res() response: Express.Response) {	
        try {	
            const labAlertInfo:any = await this.labAlertFormService.updateSpecificAlert(id, request.body);	
            let whId = request.body.whId;	
            let room = request.body.batch;	
            if(room.search("-5") != -1){	
                let whsInfo:any = await this.whs.updateStockAlert(whId, request.body,'room5');	
            }	
            if(room.search("-10") != -1){	
                let whsInfo:any = await this.whs.updateStockAlert(whId, request.body,'room10');	
            }	
            return this.successRes([]);	
        } catch ({message}) {	
            return this.handleError(message, 400);	
        }	
    }

    @Post("/submit-m9-complete")	
    @Authenticated()	
    @Status(200, {description: "submit a lab"})	
    async submitM9Complete(@Required() @BodyParams("plantIds") plant_ids: [],	
                   @Req() request: Express.Request | any,	
                   @Res() response: Express.Response) {	
        try {	
            const dbRolesInfo: any = await this.labAlertFormService.submitIntoM9Complete(plant_ids);	
            return this.successRes(dbRolesInfo);	
        } catch ({message}) {	
            return this.handleError(message, 400);	
        }	
    }    

    @Get("/id/:id")	
    @Authenticated()	
    @Status(200, {description: "Get a specific labs alert form"})	
    async specificId(@Req() request: Express.Request,	
                   @Res() response: Express.Response) {	
        try {	
            const {params: {id = ""} = {}} = request;	
            const passportList: any = await this.labAlertFormService.specificId(id);	
            return this.successRes(passportList);	
        } catch ({message}) {	
            return this.handleError(message, 400);	
        }	
    }
}