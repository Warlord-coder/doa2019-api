import {
    Authenticated,
    BodyParams,
    Controller,
    Delete,
    Get,
    MergeParams,
    PathParams,
    Post,
    Put,
    Req,
    Required,
    Res,
    Status
} from "@tsed/common";
import * as Express from "express";
import {isNil} from "lodash";

import BaseController from "../Base/BaseCtrl";
import {Upload} from "../../middlewares/upload/UploadDecorator";
import {LabService} from "../../services/lab/LabService";
import {LabAlertFormService} from "../../services/LabAlert/LabAlertService";		
import {WareHouseService} from "../../services/ware-house/WareHouseService";	
import {PlantRegisterService} from "../../services/plant-register/PlantRegisterService";


@Controller("/labs")
@MergeParams(true)
export class LabController extends BaseController {
    constructor(private labService: LabService,
        private labAlertFormService: LabAlertFormService,	
        private plantRegisterService: PlantRegisterService,		
        private wh:WareHouseService) {
        super();
    }

    /**
     *
     * @param request
     * @param response
     */
    @Get("/list")
    @Authenticated()
    @Status(200, {description: "Get a list of labs"})
    async list(@Req() request: Express.Request,
               @Res() response: Express.Response) {
        try {
            const {query: {isAlert = false} = {}} = request;
            const passportList: any = await this.labService.list(isAlert);

            return this.successRes(passportList);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Get("/listM8")
    @Authenticated()
    @Status(200, {description: "Get a list of labs"})
    async listM8(@Req() request: Express.Request,
               @Res() response: Express.Response) {
        try {
            const {query: {isAlert = false} = {}} = request;
            const passportList: any = await this.labService.listM8(isAlert);

            return this.successRes(passportList);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Get("/list/alert-plants")
    @Authenticated()
    @Status(200, {description: "Get a list of labs"})
    async listAlertPlants(@Req() request: Express.Request,
                          @Res() response: Express.Response) {
        try {
            const passportList: any = await this.labService.listUnassignAlertPlants();

            return this.successRes(passportList);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }


    /**
     *
     * @param id
     * @param request
     * @param response
     */
    @Get("/:id")
    @Authenticated()
    @Status(200, {description: "Get a specific lab"})
    async get(
        @PathParams("id") id: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.labService.getSpecificLab(id);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }


    @Post("/create")
    @Authenticated()
    @Status(200, {description: "Create a new lab"})
    async create(@BodyParams("reg_seed_receive") reg_seed_receive: string,
                 @BodyParams("volume_no") volume_no: string,
                 @BodyParams("stock_type") stock_type: string,
                 @BodyParams("is_alert") is_alert: string,
                 @BodyParams("notification_date") notification_date: string,
                 @Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {user: {_id: createdBy = null} = {}} = request;
            const dbRolesInfo: any = await this.labService.createLab({
                volume_no,
                reg_seed_receive,
                is_alert,
                stock_type,
                notification_date,
                createdBy
            });

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }


    @Post("/submit/:id")
    @Authenticated()
    @Status(200, {description: "submit a lab"})
    async submit(@Required() @BodyParams("plantIds") plant_ids: [],
                 @Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {params: {id: labId = null} = {}} = request;
            const dbRolesInfo: any = await this.labService.submitIntoLab(labId, plant_ids);
            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Post("/submit-alert-plant/:id")
    @Authenticated()
    @Status(200, {description: "submit a lab"})
    async submitAlertPlant(@Required() @BodyParams("plantIds") plant_ids: [],
                           @Req() request: Express.Request | any,
                           @Res() response: Express.Response) {
        try {
            const {params: {id: labId = null} = {}} = request;
            const dbRolesInfo: any = await this.labService.submitIntoPlantAlert(labId, plant_ids);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }


    @Post("/submit-m9")
    @Authenticated()
    @Status(200, {description: "submit a lab"})
    async submitM9(@Required() @BodyParams("plantIds") plant_ids: [],
                   @Req() request: Express.Request | any,
                   @Res() response: Express.Response) {
        try {
            // const {
            //     files = [],
            //     user: {_id: createdBy = null} = {},
            //     body = {}
            // } = request;
            const dbRolesInfo: any = await this.labService.submitIntoM9(plant_ids, request['user']._id);
            dbRolesInfo.forEach(elementisNewLab => {
                elementisNewLab.isNewLab = false;

                const dbRoleseleInfo: any = this.labService.updateLab({
                    isNewLab : false,
                    isPlantCancel : true,
                }, elementisNewLab.m8_lab_id);

                this.successRes(dbRoleseleInfo);
            });           
            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param reg_date
     * @param reg_seed_receive
     * @param reg_seed_sent_no
     * @param id
     * @param request
     * @param response
     */
    @Put("/:id")
    @Upload({path: "lab-reg-files"})
    @Authenticated()
    @Status(200, {description: "Update a existing lab"})
    async update(@BodyParams("reg_date") reg_date: string,
                 @BodyParams("reg_seed_receive") reg_seed_receive: string,
                 @BodyParams("reg_seed_sent_no") reg_seed_sent_no: string,
                 @BodyParams("is_alert") is_alert: string,
                 @PathParams("id") id: string,
                 @Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {file: {filename: reg_attach_file = undefined} = {}, user: {_id: updatedBy = null} = {}} = request;
            const dbRolesInfo: any = await this.labService.updateLab({
                reg_date,
                reg_seed_receive,
                reg_seed_sent_no,
                reg_attach_file,
                is_alert,
                updatedBy,
                updatedAt: Date.now()
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     * @param id
     */
    @Delete("/:id")
    @Authenticated()
    @Status(200, {description: "Delete a existing lab"})
    async delete(@Req() request: Express.Request | any,
                 @Res() response: Express.Response,
                 @PathParams("id") id: string) {
        try {
            const {user: {_id: deletedBy = null} = {}} = request;
            const dbRolesInfo: any = await this.labService.updateLab({
                deletedBy,
                deletedAt: Date.now()
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }
    @Post("/submit-alert-plantM9/:id")	
    @Authenticated()	
    @Status(200, {description: "submit a lab"})	
    async submitAlertPlantM9(@Required() @BodyParams("plantIds") plant_ids: [],	
                           @Req() request: Express.Request | any,	
                           @Res() response: Express.Response) {	
        try {	
            const {params: {id: labId = null} = {}} = request;	
            const whsList: any = await this.wh.getArrayList(plant_ids);	
            let data = [];	
            for (let i = 0; i < whsList.length; i++) {	
                data.push({	
                    labId:labId,	
                    warehouse:whsList[i]['_id'],	
                    alertFormStatus:'draft'	
                });	
                let whsStatus: any = await this.wh.updateStatusAlert(whsList[i]['_id'],{wh_status_alert:false});	
                const dbRolesInfo: any = await this.labAlertFormService.createLabAlertForm(data[i]);	
            }	
            let dataInfo = {	
                wareHouse:plant_ids,	
                labId:labId,	
                statusAlert:'pending'	
            }	
            return this.successRes(dataInfo);	
        } catch ({message}) {	
            return this.handleError(message, 400);	
        }	
    }   
    /**	
     *	
     * @param request	
     * @param response	
     */	
    @Post("/list/alertM9")	
    @Authenticated()	
    @Status(200, {description: "Get a list of labs"})	
    async listAlert(@Req() request: Express.Request,	
                          @Res() response: Express.Response) {	
        try {	
            const al: any = await this.labAlertFormService.listM9();	
            return this.successRes(al);	
        } catch ({message}) {	
            return this.handleError(message, 400);	
        }	
    }
    @Put("/plants/change-status")	
    @Authenticated()	
    @Status(200, {description: "Get a specific passport"})	
    async updatePlantStatus(	
        @Req() request: Express.Request | any,	
        @Res() response: Express.Response) {	
        try {	
            const {plants = [], status: distributionStatus} = request.body;	
            for await (const plant of plants) {	
                await this.labAlertFormService.updateSpecificPlant(plant, {distributionStatus});	
            }	
            return this.successRes();	
        } catch ({message}) {	
            return this.handleError(message, 400);	
        }	
    }
}