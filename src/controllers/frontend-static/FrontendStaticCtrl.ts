import {
    Authenticated,
    Controller,
    Delete,
    Get,
    MergeParams,
    PathParams,
    Post,
    Put,
    Req,
    Res,
    Status
} from "@tsed/common";
import * as Express from "express";

import BaseController from "../Base/BaseCtrl";
import {Upload} from "../../middlewares/upload/UploadDecorator";
import {SettingsService} from "../../services/settings/SettingsService";
import {EventNewsService} from "../../services/event-news/EventNewsService";
import {KnowledgeNewsService} from "../../services/knowledge-news/KnowledgeNewsService";
import AppConstants from "../../utils/constants";
import {BannerService} from "../../services/banner/BannerService";

@Controller("/static")
@MergeParams(true)
export class FrontendStaticController extends BaseController {
    constructor(private settingService: SettingsService,
                private eventNewsService: EventNewsService,
                private bannerService: BannerService,
                private eventKnowledgeService: KnowledgeNewsService) {
        super();
    }

    @Get("/home-page")
    @Status(200, {description: "Get a specific event news"})
    async getHomePage(
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const [news = [], knowledge = [], banner = []]: any = await Promise.all([
                this.eventNewsService.list({status: AppConstants.STATUS.ACTIVE}, {pageNo: 0, pageSize: 3}),
                this.eventKnowledgeService.list({status: AppConstants.STATUS.ACTIVE}, {pageNo: 0, pageSize: 3}),
                this.bannerService.list({status: AppConstants.STATUS.ACTIVE}, {pageNo: 0, pageSize: 5}),
            ]);

            return this.successRes({news, knowledge, banner});

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param id
     * @param request
     * @param response
     */
    @Get("/settings/:id")
    @Status(200, {description: "Get a specific event news"})
    async getAbout(
        @PathParams("id") id: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.settingService.getSpecificEventNews(id);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }


    /**
     *
     * @param id
     * @param request
     * @param response
     */
    @Get("/contact/:id")
    @Status(200, {description: "Get a specific event news"})
    async getContact(
        @PathParams("id") id: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.settingService.getSpecificEventNews(id);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Post("/settings/create")
    @Upload({path: "news-files", array: true})
    @Status(200, {description: "Create a new category"})
    async createAbout(@Req() request: Express.Request,
                      @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.settingService.createEventNews({
                ...request.body
            });

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Post("/contact/create")
    @Status(200, {description: "Create a new category"})
    async createContact(@Req() request: Express.Request,
                        @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.settingService.createEventNews({
                ...request.body
            });

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Put("/settings/:id")
    @Upload({path: "news-files", array: true})
    @Authenticated()
    @Status(200, {description: "Update a existing category"})
    async updateAbout(@PathParams("id") id: string,
                      @Req() request: Express.Request,
                      @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.settingService.updateEventNews({
                ...request.body,
                updatedAt: Date.now()
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Put("/contact/:id")
    @Upload({path: "news-files", array: true})
    @Authenticated()
    @Status(200, {description: "Update a existing category"})
    async updateContact(@PathParams("id") id: string,
                        @Req() request: Express.Request,
                        @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.settingService.updateEventNews({
                ...request.body,
                updatedAt: Date.now()
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }
}

