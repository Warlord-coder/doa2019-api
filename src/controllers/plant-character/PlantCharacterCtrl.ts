import {
    Authenticated,
    BodyParams,
    Controller, Delete,
    Get,
    MergeParams,
    PathParams,
    Post,
    Put,
    Req,
    Required,
    Res,
    Status
} from "@tsed/common";
import * as Express from "express";
import {isNil} from "lodash";

import BaseController from "../Base/BaseCtrl";
import {PlantCharacterService} from "../../services/plant-character/PlantCharacterService";


@Controller("/plant-characters")
@MergeParams(true)
export class PlantCharacterController extends BaseController {
    constructor(private plantCharacterService: PlantCharacterService) {
        super();
    }


    /**
     *
     * @param name
     * @param department_en
     * @param department_th
     * @param rolesInfo
     * @param request
     * @param response
     */
    @Get("/list")
    @Status(200, {description: "Get a list of characters"})
    async list(@Req() request: Express.Request,
               @Res() response: Express.Response) {
        try {
            const {query: {hideDeletedEntries = false} = {}} = request;
            const where: any = {};
            if (hideDeletedEntries) {
                where.isDeleted = null;
            }

            const charactersInfo: any = await this.plantCharacterService.list(where);

            return this.successRes(charactersInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Post("/filterList")
    @Status(200, {description: "Get a list of group types"})
    async getFilterList(
            @Required() @BodyParams("start") start: string,
            @Required() @BodyParams("length") length: string,
            @BodyParams("keyword") keyword: string,
            @Req() request: Express.Request,
            @Res() response: Express.Response) {
        try {
            const groupsInfo: any = await this.plantCharacterService.getFilterList({start: start, length: length, keyword: keyword});

            return this.successRes(groupsInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param id
     * @param request
     * @param response
     */
    @Get("/:id")
    @Status(200, {description: "Get a specific character"})
    async get(
        @PathParams("id") id: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.plantCharacterService.getSpecificCharacter(id);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param status
     * @param characterName_en
     * @param characterName_th
     * @param rolesInfo
     * @param request
     * @param response
     */
    @Post("/create")
    @Status(200, {description: "Create a new character"})
    async create(@Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.plantCharacterService.createCharacter({
                ...request.body,
                // createdBy,
            });

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param status
     * @param characterName_en
     * @param characterName_th
     * @param id
     * @param request
     * @param response
     */
    @Put("/:id")
    @Status(200, {description: "Update a existing character"})
    async update(@PathParams("id") id: string,
                 @Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.plantCharacterService.updateCharacter({
                ...request.body,
                // updatedBy:
                updatedAt: Date.now()
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Delete("/:id")
    @Authenticated()
    @Status(200, {description: "Delete a existing Plant Category"})
    async delete(@Req() request: Express.Request | any,
                 @Res() response: Express.Response,
                 @PathParams("id") id: string) {
        try {
            const {user: {_id: deletedBy = null} = {}} = request;
            const dbRolesInfo: any = await this.plantCharacterService.updateCharacter({
                isDeleted: true
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }
}

