import {
    Authenticated,
    BodyParams,
    Controller, Delete,
    Get,
    MergeParams,
    PathParams,
    Post,
    Put,
    Req,
    Required,
    Res,
    Status
} from "@tsed/common";
import * as Express from "express";
import {isNil} from "lodash";

import BaseController from "../Base/BaseCtrl";
import {PlantCategoryService} from "../../services/plant-category/PlantCategoryService";
import {EventNewsService} from "../../services/event-news/EventNewsService";
import {Upload} from "../../middlewares/upload/UploadDecorator";
import {BannerService} from "../../services/banner/BannerService";


@Controller("/banner")
@MergeParams(true)
export class BannerController extends BaseController {
    constructor(private bannerService: BannerService) {
        super();
    }


    /**
     *
     * @param request
     * @param response
     */
    @Get("/list")
    @Status(200, {description: "Get a list of event news"})
    async list(@Req() request: Express.Request,
               @Res() response: Express.Response) {
        try {
            const {query: {hideDeletedEntries = false} = {}} = request;
            const where: any = {};
            if (hideDeletedEntries) {
                where.isDeleted = null;
            }
            const categoriesInfo: any = await this.bannerService.list(where);

            return this.successRes(categoriesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param id
     * @param request
     * @param response
     */
    @Get("/:id")
    @Status(200, {description: "Get a specific event news"})
    async get(
        @PathParams("id") id: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.bannerService.getSpecificEventNews(id);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Post("/create")
    @Upload({path: "banner-files"})
    @Authenticated()
    @Status(200, {description: "Create a new category"})
    async create(@Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {
                file: {filename: imageFile = undefined} = {},
                user: {_id: createdBy = null} = {}
            } = request;
            if (imageFile) {
                request.body["imageFile"] = imageFile;
            }
            const dbRolesInfo: any = await this.bannerService.createEventNews({
                ...request.body,
                createdBy
            });

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Put("/:id")
    @Upload({path: "banner-files"})
    @Authenticated()
    @Status(200, {description: "Update a existing category"})
    async update(@PathParams("id") id: string,
                 @Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {
                file: {filename: imageFile = undefined} = {},
                user: {_id: updatedBy = null} = {}
            } = request;
            if (imageFile) {
                request.body["imageFile"] = imageFile;
            }
            const dbRolesInfo: any = await this.bannerService.updateEventNews({
                ...request.body,
                updatedAt: Date.now(),
                updatedBy
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Delete("/:id")
    @Authenticated()
    @Status(200, {description: "Delete a existing Plant Category"})
    async delete(@Req() request: Express.Request | any,
                 @Res() response: Express.Response,
                 @PathParams("id") id: string) {
        try {
            const {user: {_id: deletedBy = null} = {}} = request;
            const dbRolesInfo: any = await this.bannerService.updateEventNews({
                isDeleted: true,
                deletedBy,
                deletedAt: Date.now()
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }
}

