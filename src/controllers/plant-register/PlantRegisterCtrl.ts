import {
    Authenticated,
    BodyParams,
    Controller, Delete,
    Get,
    MergeParams,
    PathParams,
    Post,
    Put,
    Req,
    Required,
    Res,
    Status
} from "@tsed/common";
import * as Express from "express";
import {isNil} from "lodash";

import BaseController from "../Base/BaseCtrl";
import {PassportRegisterService} from "../../services/passport-register/PassportRegister";
import {Upload} from "../../middlewares/upload/UploadDecorator";
import {PlantRegisterService} from "../../services/plant-register/PlantRegisterService";
import AppConstants, {Utility} from "../../utils/constants";
import {PlantCharacterService} from "../../services/plant-character/PlantCharacterService";
import {PlantTypeService} from "../../services/plant-type/PlantTypeService";


@Controller("/plant-register")
@MergeParams(true)
export class PlantRegisterController extends BaseController {
    constructor(private plantRegisterService: PlantRegisterService,
                private plantTypeService: PlantTypeService,
                private plantCharacterService: PlantCharacterService,
                private passportRegisterService: PassportRegisterService) {
        super();
    }
    @Get("/list/sender-autocomplete")
    @Authenticated()
    @Status(200, {description: "Get a list of passports"})
    async listAutoComplete(@Req() request: Express.Request,
                           @Res() response: Express.Response,
                           @PathParams("volumeId") volumeId: string) {
        try {

            const plantList = await this.plantRegisterService.listAutoComplete();


            return this.successRes(plantList);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Get("/getDepartmentTypes")
    @Authenticated()
    @Status(200, { description: "Get department Types" })
    async getDepartmentTypes(
        @Req() request: Express.Request,
        @Res() response: Express.Response
    ) {
        try {
        const departmentTypes = await this.plantRegisterService.getDepartmentTypes();
        return this.successRes(departmentTypes);
        } catch ({ message }) {
        return this.handleError(message, 400);
        }
    }

    @Post("/filter")
    @Authenticated()
    @Status(200, { description: "Get filter" })
    async filter(
        @BodyParams("filter") search: {},
        @Req() request: Express.Request,
        @Res() response: Express.Response
    ) {
        try {
        const searchResults = await this.plantRegisterService.filter(search);
        return this.successRes(searchResults);
        } catch ({ message }) {
        return this.handleError(message, 400);
        }
    }

    @Post("/filterLabStatus")
    @Authenticated()
    @Status(200, { description: "Get filter" })
    async filterLabStatus(
        @BodyParams("filter") search: {},
        @Req() request: Express.Request,
        @Res() response: Express.Response
    ) {
        try {
            const searchResults = await this.plantRegisterService.filterLabStatus(search);
            return this.successRes(searchResults);
        } catch ({ message }) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Get("/list/:volumeId")
    @Authenticated()
    @Status(200, {description: "Get a list of passports"})
    async list(@Req() request: Express.Request,
               @Res() response: Express.Response,
               @PathParams("volumeId") volumeId: string) {
        try {
            const {query: {listing = null, status = null, form = null, labId = null} = {}} = request;
            let plantList: any = null;

            if (form === "Register 4") {
                plantList = await this.plantRegisterService.listWarehouseRecoverPlants(volumeId, listing, status, labId);
            } else {
                plantList = await this.plantRegisterService.list(volumeId, listing, status, labId);
            }

            return this.successRes(plantList);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }


    /**
     *
     * @param id
     * @param request
     * @param response
     */
    @Get("/:id")
    @Authenticated()
    @Status(200, {description: "Get a specific passport"})
    async get(
        @PathParams("id") id: string,
        @Req() request: Express.Request | any,
        @Res() response: Express.Response) {
        try {
            const {query: {form = null} = {}} = request;
            let dbRolesInfo: any = null;
            if (form === "Register 4") {
                dbRolesInfo = await this.plantRegisterService.getSpecificRecoverPlant(id);
            } else {
                dbRolesInfo = await this.plantRegisterService.getSpecificPlant(id);
            }

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Post("/create")
    @Upload({path: "plant-files", array: true})
    @Authenticated()
    @Status(200, {description: "Register a new plant"})
    async create(@Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {
                files = [],
                user: {_id: createdBy = null} = {},
                body = {}
            } = request;

            let {
                isRegCharacterAttachedFileUpload = false,
                isRegAgreementAttachFileUpload = false,
                reg_plant_alter_name = null,
                reg_plant_sci_name,
                plant_gene_name,
                plant_gene_name_selection = false,
                plant_category
            } = body;

            let reg_agreement_attach_file = null;
            if (isRegAgreementAttachFileUpload && files.length) {
                reg_agreement_attach_file = files[1] ? files[1].filename : files[0].filename;
            }

            if (plant_gene_name_selection) {
                const {_id: plantTypeId} = await this.plantTypeService.createPlantType({
                    status: AppConstants.STATUS.ACTIVE,
                    plantTypeName_en: plant_gene_name,
                    plantTypeName_th: plant_gene_name,
                    plantSpecie: reg_plant_sci_name,
                    plantCategory: plant_category
                });
                body.plant_gene_name = plantTypeId;
            }

            reg_plant_alter_name = reg_plant_alter_name && reg_plant_sci_name ? JSON.parse(reg_plant_alter_name) : [];
            const dbRolesInfo: any = await this.plantRegisterService.createPlant({
                ...body,
                createdBy,
                m3_status: "draft",
                m3register_plant_createdBy : createdBy ,
                m3register_plant_updatedBy : createdBy ,
                m3register_plant_updatedAt : Date.now() ,
                m3register_plant_createdAt : Date.now() ,
                reg_plant_alter_name,
                reg_character_attached_file: isRegCharacterAttachedFileUpload && files.length ? files[0].filename : body.reg_character_attached_file,
                reg_agreement_attach_file: reg_agreement_attach_file || body.reg_agreement_attach_file,
            });

            if (reg_plant_sci_name) {
                this.plantCharacterService.updateTags(reg_plant_alter_name, reg_plant_sci_name);
            }
            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Post("/submit-m8")
    @Authenticated()
    @Status(200, {description: "submit to module 8"})
    async submitM9(@Required() @BodyParams("plantIds") plant_ids: [],
                   @Req() request: Express.Request | any,
                   @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.plantRegisterService.submitIntoM8(plant_ids, request['user']._id);
            dbRolesInfo.forEach(elementisNewPassport => {
                elementisNewPassport.isNewPassport = false;

                const dbRoleseleInfo: any = this.passportRegisterService.updatePassport({
                    isNewPassport : false 
                }, elementisNewPassport.passport);
                
                this.successRes(dbRoleseleInfo);
            });       

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param reg_date
     * @param reg_seed_receive
     * @param reg_seed_sent_no
     * @param id
     * @param request
     * @param response
     */
    @Put("/:id")
    @Upload({path: "plant-files", array: true})
    @Authenticated()
    @Status(200, {description: "Update a existing passport"})
    async update(@PathParams("id") id: string,
                 @Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {
                files = [],
                user: {_id: updatedBy = null} = {},
                body = {}
            } = request;
            let {
                isRegCharacterAttachedFileUpload = false,
                isRegAgreementAttachFileUpload = false,
                reg_plant_alter_name,
                reg_plant_sci_name,
                plant_gene_name,
                plant_gene_name_selection = false,
                plant_category
            } = body;

            if (plant_gene_name_selection) {
                const where: any = {plantTypeName_en: plant_gene_name};
                const list = await this.plantTypeService.list(where)
                if(list[0]!= undefined && list[0]!= undefined){
                    body.plant_gene_name = list[0]._id;   
                }
                else{ 
                    const {_id: plantTypeId} = await this.plantTypeService.createPlantType({
                        status: AppConstants.STATUS.ACTIVE,
                        plantTypeName_en: plant_gene_name,
                        plantTypeName_th: plant_gene_name,
                        plantSpecie: reg_plant_sci_name,
                        plantCategory: plant_category
                    });
                    body.plant_gene_name = plantTypeId; 
                }         
            }

            reg_plant_alter_name = reg_plant_alter_name && reg_plant_sci_name ? JSON.parse(reg_plant_alter_name) : null;
            let reg_agreement_attach_file = null;
            if (isRegAgreementAttachFileUpload && files.length) {
                reg_agreement_attach_file = files[1] ? files[1].filename : files[0].filename;
            }

            const dbRolesInfo: any = await this.plantRegisterService.updatePlant({
                ...body,
                reg_character_attached_file: isRegCharacterAttachedFileUpload && files.length ? files[0].filename : null,
                reg_agreement_attach_file,
                reg_plant_alter_name,
                updatedAt: Date.now(),
                updatedBy
            }, id, true);

            if (reg_plant_sci_name) {
                this.plantCharacterService.updateTags(reg_plant_alter_name, reg_plant_sci_name);
            }
      
            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     * @param id
     */
    @Delete("/:id")
    @Authenticated()
    @Status(200, {description: "Delete a existing passport"})
    async delete(@Req() request: Express.Request | any,
                 @Res() response: Express.Response,
                 @PathParams("id") id: string) {
        try {
            const {user: {_id: deletedBy = null} = {}} = request;
            const dbRolesInfo: any = await this.plantRegisterService.updatePlant({
                deletedBy,
                deletedAt: Date.now()
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }
}
