import {
    Authenticated,
    BodyParams,
    Controller, Delete,
    Get,
    MergeParams,
    PathParams,
    Post,
    Put,
    Req,
    Required,
    Res,
    Status
} from "@tsed/common";
import * as Express from "express";
import {isNil} from "lodash";

import BaseController from "../Base/BaseCtrl";
import {CommonProvinceService} from "../../services/common-province/CommonProvinceService";


@Controller("/common-province")
@MergeParams(true)
export class CommonProvinceController extends BaseController {
    constructor(private commonProvinceService: CommonProvinceService) {
        super();
    }


    /**
     *
     * @param name
     * @param department_en
     * @param department_th
     * @param rolesInfo
     * @param request
     * @param response
     */
    @Get("/list")
    @Status(200, {description: "Get a list of province"})
    async list(@Req() request: Express.Request,
               @Res() response: Express.Response) {
        try {
            const {query: {hideDeletedEntries = false} = {}} = request;
            const where: any = {};
            if (hideDeletedEntries) {
                where.isDeleted = null;
            }

            const groupsInfo: any = await this.commonProvinceService.list(where);

            return this.successRes(groupsInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param name
     * @param department_en
     * @param department_th
     * @param rolesInfo
     * @param request
     * @param response
     */
    @Post("/search")
    @Status(200, {description: "Get a list of categories"})
    async search(
            @BodyParams("keyword") keyword: string,
            @Req() request: Express.Request,
            @Res() response: Express.Response) {
        try {
            
            const categoriesInfo: any = await this.commonProvinceService.search(keyword);

            return this.successRes(categoriesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param id
     * @param request
     * @param response
     */
    @Get("/:id")
    @Status(200, {description: "Get a specific province"})
    async get(
        @PathParams("id") id: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.commonProvinceService.getSpecificProvince(id);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param status
     * @param groupName_en
     * @param groupName_th
     * @param rolesInfo
     * @param request
     * @param response
     */
    @Post("/create")
    @Status(200, {description: "Create a new province"})
    async create(@Required() @BodyParams("status") status: string,
                 @Required() @BodyParams("provinceName_en") provinceName_en: string,
                 @Required() @BodyParams("provinceName_th") provinceName_th: object,
                 @Req() request: Express.Request,
                 @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.commonProvinceService.createProvince({
                status,
                provinceName_en,
                provinceName_th,
                // createdBy,
            });

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param status
     * @param groupName_en
     * @param groupName_th
     * @param id
     * @param request
     * @param response
     */
    @Put("/:id")
    @Status(200, {description: "Update a existing province"})
    async update(@Required() @BodyParams("status") status: string,
                 @Required() @BodyParams("provinceName_en") provinceName_en: object,
                 @Required() @BodyParams("provinceName_th") provinceName_th: object,
                 @PathParams("id") id: string,
                 @Req() request: Express.Request,
                 @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.commonProvinceService.updateProvince({
                status,
                provinceName_en,
                provinceName_th,
                // updatedBy:
                updatedAt: Date.now()
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Delete("/:id")
    @Authenticated()
    @Status(200, {description: "Delete a existing Plant Category"})
    async delete(@Req() request: Express.Request | any,
                 @Res() response: Express.Response,
                 @PathParams("id") id: string) {
        try {
            const {user: {_id: deletedBy = null} = {}} = request;
            const dbRolesInfo: any = await this.commonProvinceService.updateProvince({
                isDeleted: true
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }
}

