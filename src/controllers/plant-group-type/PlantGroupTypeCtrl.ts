import {
    Authenticated,
    BodyParams,
    Controller, Delete,
    Get,
    MergeParams,
    PathParams,
    Post,
    Put,
    Req,
    Required,
    Res,
    Status
} from "@tsed/common";
import * as Express from "express";
import {isNil} from "lodash";

import BaseController from "../Base/BaseCtrl";
import {PlantGroupTypeService} from "../../services/plant-group-type/PlantGroupTypeService";


@Controller("/plant-group-types")
@MergeParams(true)
export class PlantGroupTypeController extends BaseController {
    constructor(private plantGroupService: PlantGroupTypeService) {
        super();
    }


    /**
     *
     * @param name
     * @param department_en
     * @param department_th
     * @param rolesInfo
     * @param request
     * @param response
     */
    @Get("/list")
    @Status(200, {description: "Get a list of group types"})
    async list(@Req() request: Express.Request,
               @Res() response: Express.Response) {
        try {
            const {query: {hideDeletedEntries = false} = {}} = request;
            const where: any = {};
            if (hideDeletedEntries) {
                where.isDeleted = null;
            }
            const groupsInfo: any = await this.plantGroupService.list(where);

            return this.successRes(groupsInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param id
     * @param request
     * @param response
     */
    @Get("/:id")
    @Status(200, {description: "Get a specific group types"})
    async get(
        @PathParams("id") id: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.plantGroupService.getSpecificGroup(id);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param status
     * @param groupTypeName_en
     * @param groupTypeName_th
     * @param rolesInfo
     * @param request
     * @param response
     */
    @Post("/create")
    @Status(200, {description: "Create a new group types"})
    async create(@Required() @BodyParams("status") status: string,
                 @Required() @BodyParams("groupTypeName_en") groupTypeName_en: string,
                 @Required() @BodyParams("groupTypeName_th") groupTypeName_th: object,
                 @Req() request: Express.Request,
                 @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.plantGroupService.createGroup({
                status,
                groupTypeName_en,
                groupTypeName_th,
                // createdBy,
            });

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param status
     * @param groupTypeName_en
     * @param groupTypeName_th
     * @param id
     * @param request
     * @param response
     */
    @Put("/:id")
    @Status(200, {description: "Update a existing group types"})
    async update(@Required() @BodyParams("status") status: string,
                 @Required() @BodyParams("groupTypeName_en") groupTypeName_en: object,
                 @Required() @BodyParams("groupTypeName_th") groupTypeName_th: object,
                 @PathParams("id") id: string,
                 @Req() request: Express.Request,
                 @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.plantGroupService.updateGroup({
                status,
                groupTypeName_en,
                groupTypeName_th,
                // updatedBy:
                updatedAt: Date.now()
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }


    @Delete("/:id")
    @Authenticated()
    @Status(200, {description: "Delete a existing Plant Category"})
    async delete(@Req() request: Express.Request | any,
                 @Res() response: Express.Response,
                 @PathParams("id") id: string) {
        try {
            const {user: {_id: deletedBy = null} = {}} = request;
            const dbRolesInfo: any = await this.plantGroupService.updateGroup({
                isDeleted: true
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }
}

