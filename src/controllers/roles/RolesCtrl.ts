import {
    BodyParams,
    Controller, Get,
    MergeParams,
    Next,
    PathParams,
    Post,
    Put,
    Req,
    Required,
    Res,
    Status
} from "@tsed/common";
import * as Express from "express";
import {isNil} from "lodash";

import BaseController from "../Base/BaseCtrl";
import {RoleService} from "../../services/roles/RolesService";


@Controller("/roles")
@MergeParams(true)
export class RolesController extends BaseController {
    constructor(private roleService: RoleService) {
        super();
    }


    /**
     *
     * @param name
     * @param department_en
     * @param department_th
     * @param rolesInfo
     * @param request
     * @param response
     */
    @Get("/list")
    @Status(200, {description: "Get a list of roles"})
    async list(@Req() request: Express.Request,
               @Res() response: Express.Response) {
        try {
            let dbRolesInfo: any = await this.roleService.list();
            dbRolesInfo = await this.roleService.findTotal(dbRolesInfo);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }


    /**
     *
     * @param id
     * @param request
     * @param response
     */
    @Get("/:id")
    @Status(200, {description: "Get a specific role"})
    async get(
        @PathParams("id") id: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.roleService.getSpecificRole(id);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param roleName
     * @param userName
     * @param password
     * @param password
     * @param request
     * @param response
     * @param next
     */
    @Post("/create")
    @Status(200, {description: "Create a new role"})
    async create(@Required() @BodyParams("name") name: string,
                 @Required() @BodyParams("departmentName_en") department_en: string,
                 @Required() @BodyParams("departmentName_th") department_th: object,
                 @Required() @BodyParams("rolesInfo") rolesInfo: object,
                 @Req() request: Express.Request,
                 @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.roleService.createRole({
                name,
                department_en,
                department_th,
                rolesInfo,
                // createdBy,
            });

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param name
     * @param department_en
     * @param department_th
     * @param rolesInfo
     * @param id
     * @param request
     * @param response
     */
    @Put("/:id")
    @Status(200, {description: "Update a existing role"})
    async update(@Required() @BodyParams("name") name: string,
                 @Required() @BodyParams("departmentName_en") department_en: string,
                 @Required() @BodyParams("departmentName_th") department_th: object,
                 @Required() @BodyParams("rolesInfo") rolesInfo: object,
                 @PathParams("id") id: string,
                 @Req() request: Express.Request,
                 @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.roleService.updateRole({
                name,
                department_en,
                department_th,
                rolesInfo,
                // updatedBy:
                updatedAt: Date.now()
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }
}

