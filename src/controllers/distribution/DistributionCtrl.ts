import {
    Authenticated,
    BodyParams,
    Controller, Delete,
    Get,
    MergeParams,
    PathParams,
    Post,
    Put,
    Req,
    Required,
    Res,
    Status
} from "@tsed/common";
import * as Express from "express";
import {isNil} from "lodash";

import BaseController from "../Base/BaseCtrl";
import {PassportRegisterService} from "../../services/passport-register/PassportRegister";
import {Upload} from "../../middlewares/upload/UploadDecorator";
import {VolumeService} from "../../services/volume/VolumeService";
import {WareHouseService} from "../../services/ware-house/WareHouseService";
import {WithdrawService} from "../../services/withdraw/withdrawService";
import {DistributionService} from "../../services/distribution/distributionService";


@Controller("/distribution")
@MergeParams(true)
export class DistributionCtrl extends BaseController {
    constructor(private distributionService: DistributionService) {
        super();
    }

    /**
     *
     * @param request
     * @param response
     */
    @Get("/unassigned-list")
    @Authenticated()
    @Status(200, {description: "Get a list of passports"})
    async unassignedList(@Req() request: Express.Request | any,
                         @Res() response: Express.Response) {
        try {
            const {query: {status = "draft"} = {}} = request;
            const list: any = await this.distributionService.unassignedList();

            return this.successRes(list);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Get("/list")
    @Authenticated()
    @Status(200, {description: "Get a list of passports"})
    async list(@Req() request: Express.Request | any,
               @Res() response: Express.Response) {
        try {
            const {query: {module = ""} = {}} = request;
            const list: any = await this.distributionService.list(module);

            return this.successRes(list);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Get("/distributionplantlist")
    @Authenticated()
    @Status(200, {description: "Get a list of passports"})
    async distributionplantlist(@Req() request: Express.Request | any,
               @Res() response: Express.Response) {
        try {
            const list: any = await this.distributionService.distributionplantlist();

            return this.successRes(list);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Get("/requestlist")
    @Authenticated()
    @Status(200, {description: "Get a list of passports"})
    async requestlist(@Req() request: Express.Request | any,
               @Res() response: Express.Response) {
        try {
            const {query: {module = ""} = {}} = request;
            const list: any = await this.distributionService.requestlist(module);

            return this.successRes(list);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }
    /**
     *
     * @param request
     * @param response
     */
    @Post("/getByDepartment")
    @Authenticated()
    @Status(200, {description: "Get a list of attributes list"})
    async getByDepartment(
            @Required() @BodyParams("startDt") startDt: string,
            @Required() @BodyParams("endDt") endDt: string,
            @Req() request: Express.Request,
            @Res() response: Express.Response) {
        try {
        
            const withdraws = await this.distributionService.getByDepartment(null, {startDt: startDt, endDt: endDt});
            return this.successRes(withdraws);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Post("/getByPlantType")
    @Authenticated()
    @Status(200, {description: "Get a list of attributes list"})
    async getByPlantType(
            @Required() @BodyParams("startDt") startDt: string,
            @Required() @BodyParams("endDt") endDt: string,
            @Req() request: Express.Request,
            @Res() response: Express.Response) {
        try {
            
            const withdraws = await this.distributionService.getByPlantType(null, {startDt: startDt, endDt: endDt});
            return this.successRes(withdraws);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }
    
    /**
     *
     * @param id
     * @param request
     * @param response
     */
    @Get("/:id")
    @Authenticated()
    @Status(200, {description: "Get a specific passport"})
    async get(
        @PathParams("id") id: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.distributionService.getSpecificWithDraw(id);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param id
     * @param request
     * @param response
     */
    @Get("/plants/:id")
    @Authenticated()
    @Status(200, {description: "Get a specific passport"})
    async getPlant(
        @PathParams("id") id: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.distributionService.getSpecificWithDrawPlant(id);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Put("/plants/change-status")
    @Authenticated()
    @Status(200, {description: "Get a specific passport"})
    async updatePlantStatus(
        @Req() request: Express.Request | any,
        @Res() response: Express.Response) {
        try {
            const {plants = [], status: distributionStatus} = request.body;
            for await (const plant of plants) {
                await this.distributionService.updateSpecificPlant(plant, {distributionStatus});
            }

            return this.successRes();

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Put("/plants/complete/:id")
    @Authenticated()
    @Upload({path: "distribution-complete-files", array: true})
    @Status(200, {description: "Get a specific passport"})
    async markPlantComplete(
        @Req() request: Express.Request | any,
        @Res() response: Express.Response) {
        try {
            const {params: {id = ""} = {}, files = [], body: {fileLoc = [], plants = [], ...others} = {}} = request;
            const updatedParams = {};
            files.forEach(({filename}, index) => {
                if (fileLoc[index]) {
                    updatedParams[fileLoc[index]] = filename;
                }
            });
            await this.distributionService.updateWithDraw({...updatedParams, ...others}, id);
            for await (const plant of plants) {
                await this.distributionService.updateSpecificPlant(plant, {distributionStatus: "complete"});
                await this.distributionService.renewWarehousePlant(plant);
            }


            return this.successRes();

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Put("/plants/:id")
    @Authenticated()
    @Status(200, {description: "Get a specific passport"})
    async updatePlant(
        @PathParams("id") id: string,
        @Req() request: Express.Request | any,
        @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.distributionService.updateSpecificPlant(id, request.body);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }


    @Get("/plants/list/:id")
    @Authenticated()
    @Status(200, {description: "Get a specific passport"})
    async getPlants(
        @PathParams("id") id: string,
        @Req() request: Express.Request | any,
        @Res() response: Express.Response) {
        try {
            const {query: {status = ""} = {}} = request;
            const dbRolesInfo: any = await this.distributionService.getPlantsWithDrawList(id, status);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Post("/plants/list-status")
    @Authenticated()
    @Status(200, {description: "Get a specific passport"})
    async getPlantsStatus(
        @Req() request: Express.Request | any,
        @Res() response: Express.Response) {
        try {
            const status = request.body.status;
            const dbRolesInfo: any = await this.distributionService.getPlantsWithDrawListStatus(status);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param reg_date
     * @param reg_seed_receive
     * @param reg_seed_sent_no
     * @param request
     * @param response
     */
    @Post("/create")
    @Upload({path: "distribution-files"})
    @Authenticated()
    @Status(200, {description: "Create a new passport"})
    async create(
        @Req() request: Express.Request | any,
        @Res() response: Express.Response) {
        try {
            const {file: {filename: distribution_request_form_no_attached = undefined} = {}, user: {_id: createdBy = null} = {}, body = {}} = request;
            const dbRolesInfo: any = await this.distributionService.createWithDrawal({
                ...body,
                createdBy,
                distribution_request_form_no_attached
            });

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param reg_date
     * @param reg_seed_receive
     * @param reg_seed_sent_no
     * @param request
     * @param response
     */
    @Post("/:id")
    @Authenticated()
    @Status(200, {description: "Create a new passport"})
    async submit(
        @Req() request: Express.Request | any,
        @Res() response: Express.Response) {
        try {
            const {user: {_id: createdBy = null} = {}, body: {plantIds = ""} = {}, params: {id = ""} = {}} = request;
            const dbRolesInfo: any = await this.distributionService.submitM5(plantIds, id);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param reg_date
     * @param reg_seed_receive
     * @param reg_seed_sent_no
     * @param id
     * @param request
     * @param response
     */
    @Put("/:id")
    @Upload({path: "distribution-files"})
    @Authenticated()
    @Status(200, {description: "Update a existing passport"})
    async update(@PathParams("id") id: string,
                 @Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {file: {filename: serviceRequestLetterAttachment = undefined} = {}, user: {_id: createdBy = null} = {}, body = {}} = request;
            const dbRolesInfo: any = await this.distributionService.updateWithDraw({
                ...body,
                createdBy,
                serviceRequestLetterAttachment
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     * @param id
     */
    @Delete("/:id")
    @Authenticated()
    @Status(200, {description: "Delete a existing passport"})
    async delete(@Req() request: Express.Request | any,
                 @Res() response: Express.Response,
                 @PathParams("id") id: string) {
        try {
            const {user: {_id: deletedBy = null} = {}} = request;
            // const dbRolesInfo: any = await this.distributionService.updatePassport({
            //     deletedBy,
            //     deletedAt: Date.now()
            // }, id);

            // return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }
}

