import {
    Authenticated,
    BodyParams,
    Controller,
    Get,
    MergeParams,
    PathParams,
    Post,
    Put,
    Req,
    Required,
    Res,
    Status
} from "@tsed/common";
import * as Express from "express";
import { isNil, orderBy, pickBy, identity } from "lodash";

import BaseController from "../Base/BaseCtrl";
import { AttributeListService } from "../../services/attribute-list/AttributeListService";
import { LabService } from "../../services/lab/LabService";
import { WareHouseService } from "../../services/ware-house/WareHouseService";
import { Upload } from "../../middlewares/upload/UploadDecorator";
import { PlantCharacterService } from "../../services/plant-character/PlantCharacterService";
import { Utility } from "../../utils/constants";
import { PlantRegisterService } from "../../services/plant-register/PlantRegisterService";
import { ObjectId } from "bson";
import { start } from "repl";


@Controller("/whs")
@MergeParams(true)
export class WareHouseCtrl extends BaseController {
    constructor(private attributeListService: AttributeListService,
        private warehouseService: WareHouseService,
        private registerService: PlantRegisterService,
        private plantCharacterService: PlantCharacterService,
        private labService: LabService) {
        super();
    }


    /**
     *
     * @param request
     * @param response
     */
    @Get("/labs/list")
    @Authenticated()
    @Status(200, { description: "Get a list of attributes list" })
    async list(@Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const { query: { isAlert = null } = {} } = request;
            let labs: any = [];
            if (isAlert) {
                labs = await this.warehouseService.listAlertLabs();
            } else {
                labs = await this.warehouseService.listLabs();
            }

            labs = orderBy(labs, "createdAt", "desc");

            return this.successRes(labs);

        } catch ({ message }) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Get("/list")
    @Authenticated()
    @Status(200, { description: "Get a list of attributes list" })
    async listWhs(@Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const warehouse = await this.warehouseService.list(null, { wh_status: "complete", submitTo: "m2" });

            return this.successRes(warehouse);

        } catch ({ message }) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Post("/getIdByRegGeneId")
    @Status(200, { description: "Get ID by RegGeneId" })
    async getIdByRegGeneId(@Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const {id} = request.body;
            const warehouse = await this.warehouseService.getAll(null, { reg_gene_id: id, gs_no:null });

            return this.successRes(warehouse[0]);

        } catch ({ message }) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Get("/getAll")
    @Authenticated()
    @Status(200, { description: "Get a list of attributes list" })
    async listAllWhs(@Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const warehouse = await this.warehouseService.getAll(null, { wh_status: "complete", submitTo: "m2" });

            return this.successRes(warehouse);

        } catch ({ message }) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param name
     * @param department_en
     * @param department_th
     * @param rolesInfo
     * @param request
     * @param response
     */
    @Post("/gsNoSearch")
    @Status(200, { description: "Get a list of categories" })
    async gsNoSearch(
        @BodyParams("keyword") keyword: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {

            const categoriesInfo: any = await this.warehouseService.gsNoSearch(keyword);

            return this.successRes(categoriesInfo);

        } catch ({ message }) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param name
     * @param department_en
     * @param department_th
     * @param rolesInfo
     * @param request
     * @param response
     */
    @Post("/regNoSearch")
    @Status(200, { description: "Get a list of categories" })
    async regNoSearch(
        @BodyParams("keyword") keyword: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {

            const categoriesInfo: any = await this.warehouseService.regNoSearch(keyword);

            return this.successRes(categoriesInfo);

        } catch ({ message }) {
            return this.handleError(message, 400);
        }
    }

    @Post("/exportDoa")
    @Status(200, { description: "Advance Search for Frontend" })
    async exportDoa(@Req() request: Express.Request | any,
        @Res() response: Express.Response) {
        try {
            const { body = {} } = request;
            const result = await this.warehouseService.exportDoa(body);

            return this.successRes(result);

        } catch (error) {
            return this.handleError(error.message || error, 400);
        }
    }


    /**
     *
     * @param request
     * @param response
     */
    @Post("/filterList")
    @Authenticated()
    @Status(200, { description: "Get a list of attributes list" })
    async filterListWhs(
        @Required() @BodyParams("start") start: string,
        @Required() @BodyParams("length") length: string,
        @BodyParams("keyword") keyword: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {

            let res = await this.warehouseService.filterList(null, { wh_status: "complete", submitTo: "m2" }, { start: start, length: length, keyword: keyword });

            return this.successRes(res);

        } catch ({ message }) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Post("/alertFilterList")
    @Authenticated()
    @Status(200, { description: "Get a list of attributes list" })
    async alertFilterListWhs(
        @Required() @BodyParams("start") start: string,
        @Required() @BodyParams("length") length: string,
        @BodyParams("keyword") keyword: string,
        @BodyParams("alert") alert: boolean,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {

            let res = await this.warehouseService.alertFilterList(null, {}, { start: start, length: length, keyword: keyword, alert: alert });
            return this.successRes(res);

        } catch ({ message }) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Post("/alertFilterNotDraftList")
    @Authenticated()
    @Status(200, { description: "Get a list of attributes list" })
    async alertFilterDraftListWhs(
        @BodyParams("alert") alert: boolean,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {

            let res = await this.warehouseService.alertFilterNotDraftNumber(null, {}, { alert: alert });
            return this.successRes(res);

        } catch ({ message }) {
            return this.handleError(message, 400);
        }
    }
    
    /**
     *
     * @param request
     * @param response
     */
    @Post("/alertFilterDraftList")
    @Authenticated()
    @Status(200, { description: "Get a list of attributes list" })
    async alertFilterNotDraftListWhs(
        @BodyParams("alert") alert: boolean,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {

            let res = await this.warehouseService.alertFilterDraftNumber(null, {}, { alert: true });
            return this.successRes(res);

        } catch ({ message }) {
            return this.handleError(message, 400);
        }
    }

    @Post("/storeDate")
    @Authenticated()
    @Status(200, { description: "Get a list of attributes list" })
    async storeDate(
        @BodyParams("id") id: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {

            let res = await this.warehouseService.getStoreDate(id);
            return this.successRes(res);

        } catch ({ message }) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Post("/m2FilterList")
    @Authenticated()
    @Status(200, { description: "Get a list of attributes list" })
    async m2FilterListWhs(
        @Required() @BodyParams("start") start: string,
        @Required() @BodyParams("length") length: string,
        @BodyParams("keyword") keyword: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {

            let res = await this.warehouseService.m2FilterList(null, { wh_status: "complete", submitTo: "m2" }, { start: start, length: length, keyword: keyword });

            return this.successRes(res);

        } catch ({ message }) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Get("/listAll")
    @Authenticated()
    @Status(200, { description: "Get a list of attributes list" })
    async listWhsAll(@Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const warehouse = await this.warehouseService.list(null, { wh_status: "complete" });

            return this.successRes(warehouse);

        } catch ({ message }) {
            return this.handleError(message, 400);
        }
    }
    /**
     *
     * @param request
     * @param response
     */
    @Get("/labs/:id")
    @Authenticated()
    @Status(200, { description: "Get a list of attributes list" })
    async getSpecificLab(
        @PathParams("id") id: string,
        @Req() request: Express.Request | any,
        @Res() response: Express.Response) {
        try {
            const { query: { status = "draft", submitTo = null, isAlert = null } = {} } = request;
            let labs: any = null;
            if (id !== "all") {
                if (isAlert) {
                    labs = await this.warehouseService.getSpecificAlertLabPlants(id, status, submitTo);
                } else {
                    labs = await this.warehouseService.getSpecificLabPlants(id, status, submitTo);
                }
            } else {
                const [simpleLab, alertLab] = await Promise.all([this.warehouseService.getSpecificAllLabPlants(id, submitTo), this.warehouseService.getSpecificAllAlertLabPlants(id, submitTo)]);
                labs = [...simpleLab, ...alertLab];
            }

            return this.successRes(labs);

        } catch ({ message }) {
            return this.handleError(message, 400);
        }
    }
    @Post("/listAllAlertPLant")
    @Authenticated()
    @Status(200, { description: "Get a list of attributes list" })
    async listAllAlertPLant(
        @PathParams("id") id: string,
        @Req() request: Express.Request | any,
        @Res() response: Express.Response) {
        let data = await this.warehouseService.getAllWH();
        return this.successRes(data);
    }

    /**
     *
     * @param request
     * @param response
     */
    @Get("/:id")
    @Authenticated()
    @Status(200, { description: "Get a specific warehouse plant information" })
    async get(
        @PathParams("id") id: string,
        @Req() request: Express.Request | any,
        @Res() response: Express.Response) {
        try {
            const { query: { isAlert = null, withdraw = null } = {} } = request;
            let data: any = {};
            if (isAlert) {
                data = await this.warehouseService.getSpecificAlertWHPlant(id);
            } else if (withdraw) {
                data = await this.warehouseService.getSpecificWHPlantWithdraw(id);
            } else {
                data = await this.warehouseService.getSpecificWHPlant(id);
            }

            return this.successRes(data);

        } catch ({ message }) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Put("/module/:id")
    @Authenticated()
    @Status(200, { description: "Get a specific warehouse plant information" })
    async submitToM2(
        @PathParams("id") id: string,
        @Required @BodyParams("submitTo") submitTo: string,
        @BodyParams("symbol") symbol: string,
        @BodyParams("chkRoom") chkRoom: boolean,
        @BodyParams("gs_no") gs_no_plant: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            let gs_no = gs_no_plant;
            const other: any = {};
            if (submitTo === "m2" && !gs_no_plant) {
                const count = await this.warehouseService.getCount();
                gs_no = "DOA" + symbol + Utility.getFormattedNumber(count + 1);
            }
            if (submitTo === "m2") {

                const dbRolesInfo: any = await this.warehouseService.updatePlant(pickBy({
                    submitTo,
                    wh_status: "complete",
                    m4_status: chkRoom,
                    gs_no,
                    m9warehouse_plant_submittedAt: Date.now(),
                    m9warehouse_plant_submittedBy: request['user']._id,
                    m2doa_plant_createdAt: Date.now(),
                    m2doa_plant_creatdBy: request['user']._id,
                    m2doa_plant_updatedAt: Date.now(),
                    m2doa_plant_updatedBy: request['user']._id,
                    // isPlantCancel :true ,
                    ...other
                }, identity), id, true);

                if (dbRolesInfo.room5 != null) {
                    let d5 = dbRolesInfo.room5;
                    for (let j = 0; j < d5.length; j++) {
                        if (parseFloat(d5[j].notification_period.weight) > parseFloat(d5[j].wh_batch_weight) ||
                            parseFloat(d5[j].notification_period.percent) > parseFloat(d5[j].lab_test_growth_percent)
                        ) {
                            let dbM4: any = await this.warehouseService.updatePlant(pickBy({
                                recovery_status: 'draft',
                                selectedBatchRoom: 'Room 5 Batch' + d5[j]['batch_no'],
                                ...other
                            }, identity), id, true);
                        }
                    }
                }
                if (dbRolesInfo.room10 != null) {
                    let d5 = dbRolesInfo.room10;
                    for (let j = 0; j < d5.length; j++) {
                        if (parseFloat(d5[j].notification_period.weight) > parseFloat(d5[j].wh_batch_weight) ||
                            parseFloat(d5[j].notification_period.percent) > parseFloat(d5[j].lab_test_growth_percent)
                        ) {
                            let dbM4: any = await this.warehouseService.updatePlant(pickBy({
                                recovery_status: 'draft',
                                selectedBatchRoom: 'Room 10 Batch' + d5[j]['batch_no'],
                                ...other
                            }, identity), id, true);
                        }
                    }
                }
                return this.successRes(dbRolesInfo);

            }
            else if (submitTo === "m4") {
                const dbRolesInfo: any = await this.warehouseService.updatePlant(pickBy({
                    submitTo,
                    wh_status: "complete",
                    recovery_status: 'draft',
                    gs_no,
                    m9warehouse_plant_submittedAt: Date.now(),
                    m9warehouse_plant_submittedBy: request['user']._id,
                    m4recovery_plant_createdAt: Date.now(),
                    m4recovery_plant_updatedAt: Date.now(),
                    m4recovery_plant_creatdBy: request['user']._id,
                    m4recovery_plant_updatedBy: request['user']._id,
                    // isPlantCancel :true,
                    ...other
                }, identity), id, true);
                return this.successRes(dbRolesInfo);
            }
            else if (submitTo === "m9") {
                const dbRolesInfo: any = await this.warehouseService.updatePlant(pickBy({
                    submitTo,
                    wh_status: "complete",
                    gs_no,
                    isPlantCancel: true,
                    ...other
                }, identity), id, true);

                return this.successRes(dbRolesInfo);
            }
            else {
                const dbRolesInfo: any = await this.warehouseService.updatePlant(pickBy({
                    submitTo,
                    wh_status: "complete",
                    gs_no,
                    // isPlantCancel :true,
                    ...other
                }, identity), id, true);

                return this.successRes(dbRolesInfo);
            }
        } catch ({ message }) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Put("/:id")
    @Upload({ path: "plant-files", array: true })
    @Authenticated()
    @Status(200, { description: "Update a existing warehouse plant" })
    async update(@PathParams("id") id: string,
        @Req() request: Express.Request | any,
        @Res() response: Express.Response) {
        try {
            const {
                files = [],
                user: { _id = null } = {}, body = {}
            } = request;
            let {
                reg_plant_alter_name,
                reg_plant_sci_name,
                reg_gene_category
            } = body;

            const {
                isWhSampleRefSeedUpload = false,
                isWhSampleRefMolecularUpload = false,
                isDescriptorFileUpload = false
            } = body;

            reg_plant_alter_name = reg_plant_alter_name && reg_plant_sci_name ? JSON.parse(reg_plant_alter_name) : null;

            let wh_sample_ref_seed_file = null;
            if (isWhSampleRefSeedUpload && files.length) {
                wh_sample_ref_seed_file = files[1] ? files[1].filename : files[0].filename;
            }

            if (body.room5 && body.room5.length) {
                body.room5 = JSON.parse(body.room5);
                body.room5.forEach(room => {
                    if (room.batch_id == undefined || room.batch_id == '' || room.batch_id == null)
                        room.batch_id = ObjectId();
                });
            }
            if (body.room10 && body.room10.length) {
                body.room10 = JSON.parse(body.room10);
            }
            if (body.anteArray && body.anteArray.length) {
                body.anteArray = JSON.parse(body.anteArray);
            }
            if (body.room130 && body.room130.length) {
                body.room130 = JSON.parse(body.room130);
            }
            let datas = {
                ...body,
                wh_sample_ref_seed_file,
                reg_plant_alter_name,
                wh_sample_ref_molecular_file: isWhSampleRefMolecularUpload && files.length ? files[0].filename : null,
                description_file: isDescriptorFileUpload && files.length ? files[0].filename : body.description_file,
                updatedBy: _id,
                updatedAt: Date.now()
            }
            if (reg_plant_sci_name) {
                datas.reg_plant_sci_name = reg_plant_sci_name;
            }
            const dbRolesInfo: any = await this.warehouseService.updatePlant(datas, id, true);
            if (reg_plant_sci_name) {
                this.plantCharacterService.updateTags(reg_plant_alter_name, reg_plant_sci_name);
            }

            return this.successRes([]);
        } catch ({ message }) {
            return this.handleError(message, 400);
        }
    }
    /**	
     *	
     * @param request	
     * @param response	
     */
    @Post("/alert-lists")
    @Authenticated()
    @Status(200, { description: "Get list warehouse plant when alert" })
    async listAlert(
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const { query: { isAlert = null } = {} } = request;
            let data: any = {};
            if (isAlert) {
                data = await this.warehouseService.getAlertWHPlant();
            } else {
                data = await this.warehouseService.getWHPlant();
            }
            let d = [];
            for (let i = 0; i < data.length; i++) {

                if (data[i].room5 != null) {
                    let d5 = data[i].room5;
                    for (let j = 0; j < d5.length; j++) {
                        try {
                            if (parseFloat(d5[j].notification_period.weight) < parseFloat(d5[j].wh_batch_weight) &&
                                parseFloat(d5[j].notification_period.percent) < parseFloat(d5[j].lab_test_growth_percent)
                                && Utility.checkYear(parseFloat(d5[j].notification_period.year), d5[j].wh_batch_store_date)) {
                                d.push(data[i]);
                            }
                        } catch (error) {
                            console.log(error);
                        }

                    }
                }
                if (data[i].room10 != null) {
                    let d5 = data[i].room10;
                    for (let j = 0; j < d5.length; j++) {
                        try {
                            if (parseFloat(d5[j].notification_period.weight) < parseFloat(d5[j].wh_batch_weight) &&
                                parseFloat(d5[j].notification_period.percent) < parseFloat(d5[j].lab_test_growth_percent)
                                && Utility.checkYear(parseFloat(d5[j].notification_period.year), d5[j].wh_batch_store_date)) {
                                d.push(data[i]);
                            }
                        } catch (error) {
                            console.log(error);
                        }

                    }
                }
            }
            let da = d.filter((item, index, arr) => {
                const c = arr.map(item => item._id);
                return index === c.indexOf(item._id)
            })
            return this.successRes(da);
        } catch ({ message }) {
            return this.handleError(message, 400);
        }
    }
    /**	
     *	
     * @param request	
     * @param response	
     */
    @Post("/alert-lists-recovery")
    @Authenticated()
    @Status(200, { description: "Get list warehouse plant when alert" })
    async listRecovery(
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const { query: { isAlert = null } = {} } = request;
            let data: any = {};
            data = await this.warehouseService.getListWHM4();
            let d = [];
            let tempId = [];
            for (let i = 0; i < data.length; i++) {
                try {
                    if (tempId.indexOf(data[i].recovery_id['_id']) == -1) {
                        tempId.push(data[i].recovery_id['_id']);
                        d.push(data[i]);
                    }
                } catch (error) {

                }
            }
            return this.successRes(d);
        } catch ({ message }) {
            return this.handleError(message, 400);
        }
    }
    /**	
     *	
     * @param request	
     * @param response	
     */
    @Post("/alert-listsM4")
    @Authenticated()
    @Status(200, { description: "Get list warehouse plant when alert" })
    async listAlertM4(
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const { query: { isAlert = null } = {} } = request;
            let data = await this.warehouseService.getWHM4();
            return this.successRes(data);
        } catch ({ message }) {
            return this.handleError(message, 400);
        }
    }

    //    @Post("/alert-lists/:id")	
    //    @Authenticated()	
    //    @Status(200, {description: "Get list warehouse plant when alert"})	
    //    async listAlertId(	
    //        @PathParams("id") id: string,	
    //        @Req() request: Express.Request,	
    //        @Res() response: Express.Response) {	
    //            try {	
    //                const {query: {isAlert = null} = {}} = request;	
    //                let data: any = {};	
    //                //data = await this.transService.getSpecificId(id);	

    //                return this.successRes(data);	

    //            } catch ({message}) {	
    //                return this.handleError(message, 400);	
    //            }	
    //    }
}

