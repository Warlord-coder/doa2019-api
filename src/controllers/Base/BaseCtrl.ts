import {pickBy} from "lodash";
import {IResponse} from "../../interfaces/IResponse";
import AppConstants from "../../utils/constants";

export default class BaseController {
    constructor() {
    }

    /**
     *
     * @param response
     * @private
     */
    private _formatResponse = (response) => pickBy(response, data => !(data === null || data === undefined));

    /**
     *
     * @param message
     * @param statusCode
     */
    protected handleError = (message = null, statusCode = 400, data = null): IResponse => {
        message = message && message.includes("validation failed") ? message.split("validation failed: ")[1] : message;

        return this._formatResponse({
            success: false,
            statusCode,
            message,
            data
        });
    };

    /**
     *
     * @param data
     * @param status
     * @param message
     */
    protected successRes = (data = null, status = 200, message = null): IResponse => {
        return this._formatResponse({
            success: true,
            data,
            status,
            message,
        });
    };

    protected calenderSyncService(syncAccount) {
        switch (syncAccount) {
            case AppConstants.CALENDAR.SYNC.GOOGLE:
                return "googleService";
            case AppConstants.CALENDAR.SYNC.OFFICE365:
                return "outlookService";
        }
    }
}
