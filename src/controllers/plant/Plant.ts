import {
    Authenticated,
    BodyParams,
    Controller, Delete,
    Get,
    MergeParams,
    PathParams,
    Post,
    Put,
    Req,
    Required,
    Res,
    Status
} from "@tsed/common";
import * as Express from "express";
import {isNil} from "lodash";

import BaseController from "../Base/BaseCtrl";
import {PlantService} from "../../services/plant/PlantService";


@Controller("/plants")
@MergeParams(true)
export class CommonAumperController extends BaseController {
    constructor(private plantService: PlantService) {
        super();
    }


    /**
     *
     * @param request
     * @param response
     */
    @Get("/list")
    @Status(200, {description: "Get a list of province"})
    async list(@Req() request: Express.Request,
               @Res() response: Express.Response) {
        try {
            const {query: {hideDeletedEntries = false} = {}} = request;
            const where: any = {};
            if (hideDeletedEntries) {
                where.isDeleted = null;
            }

            const groupsInfo: any = await this.plantService.list(where);

            return this.successRes(groupsInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param id
     * @param request
     * @param response
     */
    @Get("/:id")
    @Status(200, {description: "Get a specific province"})
    async get(
        @PathParams("id") id: string,
        @Req() request: Express.Request,
        @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.plantService.getSpecificPlant(id);

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param plant_status
     * @param plantTypeName_sci_en
     * @param plantTypeName_sci_global_en
     * @param plantTypeName_sci_other_en
     * @param plantTypeName_sci_th
     * @param plantTypeName_sci_global_th
     * @param plantTypeName_sci_other_th
     * @param plant_category
     * @param plant_character
     * @param plant_group
     * @param plant_group_type
     * @param request
     * @param response
     */
    @Post("/create")
    @Status(200, {description: "Create a new province"})
    async create(@Required() @BodyParams("plant_status") plant_status: string,
                 @Required() @BodyParams("plantTypeName_sci_en") plantTypeName_sci_en: string,
                 @Required() @BodyParams("plantTypeName_sci_global_en") plantTypeName_sci_global_en: string,
                 @Required() @BodyParams("plantTypeName_sci_other_en") plantTypeName_sci_other_en: string,
                 @Required() @BodyParams("plantTypeName_sci_th") plantTypeName_sci_th: string,
                 @Required() @BodyParams("plantTypeName_sci_global_th") plantTypeName_sci_global_th: string,
                 @Required() @BodyParams("plantTypeName_sci_other_th") plantTypeName_sci_other_th: string,
                 @Required() @BodyParams("plant_category") plant_category: string,
                 @Required() @BodyParams("plant_character") plant_character: string,
                 @Required() @BodyParams("plant_group") plant_group: string,
                 @Required() @BodyParams("plant_group_type") plant_group_type: string,
                 @Req() request: Express.Request,
                 @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.plantService.createPlant({
                plant_status,
                plantTypeName_sci_en,
                plantTypeName_sci_global_en,
                plantTypeName_sci_th,
                plantTypeName_sci_global_th,
                plantTypeName_sci_other_th,
                plant_category,
                plant_character,
                plantTypeName_sci_other_en,
                plant_group_type,
                plant_group,
                // createdBy,
            });

            return this.successRes(dbRolesInfo);

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param plant_status
     * @param plantTypeName_sci_en
     * @param plantTypeName_sci_global_en
     * @param plantTypeName_sci_other_en
     * @param plantTypeName_sci_th
     * @param plantTypeName_sci_global_th
     * @param plantTypeName_sci_other_th
     * @param plant_category
     * @param plant_character
     * @param plant_group
     * @param plant_group_type
     * @param id
     * @param request
     * @param response
     */
    @Put("/:id")
    @Status(200, {description: "Update a existing province"})
    async update(@Required() @BodyParams("plant_status") plant_status: string,
                 @Required() @BodyParams("plantTypeName_sci_en") plantTypeName_sci_en: string,
                 @Required() @BodyParams("plantTypeName_sci_global_en") plantTypeName_sci_global_en: string,
                 @Required() @BodyParams("plantTypeName_sci_other_en") plantTypeName_sci_other_en: string,
                 @Required() @BodyParams("plantTypeName_sci_th") plantTypeName_sci_th: string,
                 @Required() @BodyParams("plantTypeName_sci_global_th") plantTypeName_sci_global_th: string,
                 @Required() @BodyParams("plantTypeName_sci_other_th") plantTypeName_sci_other_th: string,
                 @Required() @BodyParams("plant_category") plant_category: string,
                 @Required() @BodyParams("plant_character") plant_character: string,
                 @Required() @BodyParams("plant_group") plant_group: string,
                 @Required() @BodyParams("plant_group_type") plant_group_type: string,
                 @PathParams("id") id: string,
                 @Req() request: Express.Request,
                 @Res() response: Express.Response) {
        try {
            const dbRolesInfo: any = await this.plantService.updatePlant({
                plant_status,
                plantTypeName_sci_en,
                plantTypeName_sci_global_en,
                plantTypeName_sci_th,
                plantTypeName_sci_global_th,
                plantTypeName_sci_other_en,
                plantTypeName_sci_other_th,
                plant_category,
                plant_character,
                plant_group_type,
                plant_group,
                // updatedBy:
                updatedAt: Date.now()
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Delete("/:id")
    @Authenticated()
    @Status(200, {description: "Delete a existing Plant Category"})
    async delete(@Req() request: Express.Request | any,
                 @Res() response: Express.Response,
                 @PathParams("id") id: string) {
        try {
            const {user: {_id: deletedBy = null} = {}} = request;
            const dbRolesInfo: any = await this.plantService.updatePlant({
                isDeleted: true
            }, id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }
}

