import {Authenticated, Controller, Get, MergeParams, Req, Res, Status} from "@tsed/common";
import {isNil} from "lodash";

import BaseController from "../Base/BaseCtrl";
import {ReportService} from "../../services/report/ReportService";
import * as Express from "express";


@Controller("/reports")
@MergeParams(true)
export class ReportCtrl extends BaseController {
    constructor(private reportService: ReportService) {
        super();
    }


    /**
     *
     * @param request
     * @param response
     */
    @Get("/mol3-register-report")
    // @Authenticated()
    @Status(200, {description: "Get a list of recovery"})
    async recoverPlants(@Req() request: Express.Request,
                        @Res() response: Express.Response) {
        try {
            const {query: {startAt = null, endAt = null} = {}} = request;
            const data: any = await this.reportService.getMol3RegisterReport(startAt, endAt);

            return this.successRes(data);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param request
     * @param response
     */
    @Get("/mol8-l-status-report")
    // @Authenticated()
    @Status(200, {description: "Get a list of recovery"})
    async mol8LStatus(@Req() request: Express.Request,
                      @Res() response: Express.Response) {
        try {
            const data: any = await this.reportService.getMol8LStatusReport();

            return this.successRes(data);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }
}

