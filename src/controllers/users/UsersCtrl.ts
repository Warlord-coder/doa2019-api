import {
    Authenticated,
    BodyParams,
    Controller,
    Get,
    MergeParams,
    Next,
    PathParams,
    Post,
    Delete,
    Put,
    Req,
    Required,
    Res,
    Status
} from "@tsed/common";
import * as Express from "express";
import * as passport from "passport";
import {isNil} from "lodash";
import * as multer from "multer";

import BaseController from "../Base/BaseCtrl";
import {UserService} from "../../services/users/UsersService";
import {IResponse} from "../../interfaces/IResponse";
import AppConstants from "../../utils/constants";

import PassportService from "../../services/passport/PassportService";
import MailerService from "../../services/mailer/MailerService";
import {User} from "../../models/users/User";
import {Upload} from "../../middlewares/upload/UploadDecorator";

const upload = multer();

@Controller("/users")
@MergeParams(true)
export class UserController extends BaseController {
    constructor(private userService: UserService, private passportService: PassportService) {
        super();
    }

    private _passportSetup = () => {
        PassportService.setup(this.passportService);
    };

    /**
     *
     * @param email
     * @param password
     * @param request
     * @param response
     * @param next
     */
    @Post("/register")
    @Authenticated()
    @Upload("single")
    @Status(200, {description: "Create User from admin management"})
    async create(@Required() @BodyParams("email") email: string,
                 @Required() @BodyParams("firstName") firstName: string | any,
                 @Required() @BodyParams("lastName") lastName: string | any,
                 @Required() @BodyParams("gender") gender: string,
                 @Required() @BodyParams("username") userName: string,
                 @Required() @BodyParams("password") password: string,
                 @Required() @BodyParams("role") role: string,
                 @Required() @BodyParams("status") status: string,
                 @Req() request: Express.Request | any,
                 @Res() response: Express.Response) {
        try {
            const {file: {filename: profileImg = undefined} = {}} = request;
            const user: any = await this.userService.createUser({
                email,
                name: {
                    firstName,
                    lastName
                },
                gender,
                profileImg,
                userName,
                password,
                status,
                role,
                // createdBy:
            });

            return this.successRes(user.display());

        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param email
     * @param password
     * @param request
     * @param response
     * @param next
     */
    @Post("/login")
    @Status(200, {description: "User logged in and get token with user information"})
    async login(@Required() @BodyParams("email") email: string,
                @Required() @BodyParams("password") password: string,
                @Req() request: Express.Request,
                @Res() response: Express.Response,
                @Next() next: Express.NextFunction) {
        this._passportSetup();

        return passport.authenticate("local", async (err, user: any, info: string) => {
            try {
                const error = err || info;
                if (error || !user) {
                    const {message} = error;
                    throw new Error(message || error || AppConstants.ERROR_MESSAGES.SOMETHING_WENT_WRONG);
                }
                const {authToken, lastLogin} = user;

                return response.json({
                    success: true,
                    status: 200,
                    data: {
                        authToken,
                        tokenExpiry: AppConstants.TOKEN_EXPIRY, ...user.display(),
                        lastLogin
                    }
                });
            } catch ({message}) {
                return response.status(401).json({success: false, status: 401, message});
            }
        })(request, response, null);
    }

    /**
     *
     * @param request
     */
    @Get("/me")
    @Authenticated()
    @Status(200, {description: "Get Current Logged In User information"})
    public async currentUserInfo(@Req() request: Express.Request | any): Promise<IResponse> {
        try {
            const {user = {}} = request;
            const {id = ""} = user;
            const userInfo = await this.userService.findUser(id);

            return this.successRes(userInfo.display(), 200);
        } catch ({message}) {
            return this.handleError(message, 401);
        }
    }

    /**
     *
     * @param request
     */
    @Get("/list")
    @Authenticated()
    @Status(200, {description: "List all users information"})
    public async list(@Req() request: Express.Request | any): Promise<IResponse> {
        try {
            const users = await this.userService.findAllUsers();

            return this.successRes(users, 200);
        } catch ({message}) {
            return this.handleError(message, 401);
        }
    }

    /**
     *
     * @param request
     */
    @Get("/:id")
    @Authenticated()
    @Status(200, {description: "Get Current Logged In User information"})
    public async getSpecific(@Req() request: Express.Request | any,
                             @PathParams("id") id: string): Promise<IResponse> {
        try {
            const userInfo = await this.userService.findUser(id);

            return this.successRes(userInfo.display(), 200);
        } catch ({message}) {
            return this.handleError(message, 401);
        }
    }

    /**
     *
     * @param request
     */
    @Put("/:id")
    @Authenticated()
    @Upload("single")
    @Status(200, {description: "Get Current Logged In User information"})
    public async updateSpecific(@Req() request: Express.Request | any,
                                @Required() @BodyParams("email") email: string,
                                @Required() @BodyParams("firstName") firstName: string | any,
                                @Required() @BodyParams("lastName") lastName: string | any,
                                @Required() @BodyParams("gender") gender: string,
                                @Required() @BodyParams("username") userName: string,
                                @Required() @BodyParams("role") role: string,
                                @Required() @BodyParams("status") status: string,
                                @PathParams("id") id: string): Promise<IResponse> {
        try {
            const {file: {filename: profileImg = undefined} = {}} = request;
            const userInfo = await this.userService.findAndUpdate(id, {
                email,
                name: {
                    firstName,
                    lastName
                },
                gender,
                profileImg,
                userName,
                role,
                status
            });

            return this.successRes(userInfo.display(), 200);
        } catch ({message}) {
            return this.handleError(message, 401);
        }
    }

    /**
     *
     * @param email
     */
    @Post("/forgot-password")
    @Status(200, {description: "Sent forgot password reset link to user email"})
    public async forgotPassword(@Required() @BodyParams("email") email: string): Promise<IResponse> {
        try {
            const {resetPasswordToken, fullName} = await this.userService.setForgotPasswordLink(email);
            MailerService.getInstance().resetPassword(email, {resetPasswordToken, fullName});

            return this.successRes(null, 201, AppConstants.SUCCESS_MESSAGES.RESET_PASSWORD);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    /**
     *
     * @param verificationToken
     */
    @Get("/verify-account-token/:token")
    @Status(200, {description: "Success or fail response depend on forgot verification token validation and expiry"})
    public async verifyAccountVerificationToken(@Required() @PathParams("token") verificationToken: string): Promise<IResponse> {
        try {
            const user = await this.userService.verifyForgotPasswordToken(verificationToken);

            return this.successRes(user.display(), 200);
        } catch ({message, data}) {
            return this.handleError(message, 400, data);
        }
    }

    /**
     *
     * @param password
     * @param resetToken
     */
    @Put("/change-password/:resetToken")
    @Status(200, {description: "Change password reset successfully"})
    async changePassword(@Required() @BodyParams("newPassword") password: string,
                         @PathParams("resetToken") resetToken: string): Promise<IResponse> {
        try {
            const user = await this.userService.verifyForgotPasswordToken(resetToken);
            password = await User.encrypt(password);
            await this.userService.updateUser(user, {
                password,
                resetPasswordToken: undefined,
                resetPasswordExpiry: undefined
            }, false);

            return this.successRes(null, 200, AppConstants.SUCCESS_MESSAGES.PASSWORD_RESET);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }

    @Delete("/:id")
    @Authenticated()
    @Status(200, {description: "Delete a existing Plant Category"})
    async delete(@Req() request: Express.Request | any,
                 @Res() response: Express.Response,
                 @PathParams("id") id: string) {
        try {
            const {user: {_id: deletedBy = null} = {}} = request;
            const dbRolesInfo: any = await this.userService.delete(id);

            return this.successRes(dbRolesInfo);
        } catch ({message}) {
            return this.handleError(message, 400);
        }
    }
}

