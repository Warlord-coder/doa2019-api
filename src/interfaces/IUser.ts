export interface IUser {
    _id?: string;
    name: IName;
    email: string;
    gender: string;
    role?: string;
    profileImg?: string;
    userName?: string;
    password?: string;
    status?: string;
    authToken?: string;
    tokenExpiry?: string;
    verifyAccountToken?: string;
    verifyAccountTokenExpiry?: number;
    resetPasswordExpiry?: number;
    resetPasswordToken?: string;
    color?: string;
    lastLogin?: string;
}

export interface IName {
    first: string;
    last: string;
}

export interface IAccounts {
    account: string;
    status: string;
}
