export interface IResponse {
    success: Boolean;
    errorCode?: Number;
    message?: String;
    data?: any;
}
